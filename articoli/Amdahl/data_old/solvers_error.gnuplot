set terminal epslatex size 9.0cm,6.35cm
set output fileout

set zeroaxis

# Uniform
set title t

set xr [0.0:max] 
#set yr [0.0:1.0] 

plot filein using 1:2 t 'ordinary least square' with lines, \
     filein using 1:3 t 'total least square' with lines, \
     filein using 1:4 t 'symplex' with lines
