# open foam
#install.packages('reshape')
#install.packages('xtable')
#install.packages('tikzDevice')
library(reshape)
require(MASS)
library(lattice)
library(xtable)
require(tikzDevice)
library(rpart)

doFiles <- T
# set singleUsingThreading to T if you want to use hyper in the training set (to show wrong setting)
# set it to F is you want to use the same settings as the test set (real experiment)
singleUsingThreading <- F


out.folder <-  "/Users/davidemorelli/Projects/papers/Amdahl/data"
#out.folder <-  "/Users/davidemorelli/Projects/energy-model-paper/data"
#out.folder <- getwd()
names <- c("cavity", "pitzDaily", "squareBumpFine", "mixerVesselAMI2D") 

graph.width <- 6.0

rmse <- function(sim, obs)
    {
        sqrt( mean( (sim - obs)^2) )
    }

if (singleUsingThreading)
    {
        singleValueFile <- sprintf("%s/singleValuesHyperthreading.csv", out.folder)
    } else {
        singleValueFile <- sprintf("%s/singleValues.csv", out.folder)
    }

OpenFoamData <- read.csv(file=singleValueFile, header=TRUE,sep=";", colClasses=c("character", rep("numeric",14)), row.names = NULL)

E.w <-  OpenFoamData$E/OpenFoamData$E
a1.w <-  OpenFoamData$a1/OpenFoamData$E
a2.w <-  OpenFoamData$a2/OpenFoamData$E
a3.w <-  OpenFoamData$a3/OpenFoamData$E
a4log.w <-  OpenFoamData$a4log/OpenFoamData$E
a4real.w <-  OpenFoamData$a4real/OpenFoamData$E
acomm.w <-  OpenFoamData$acomm/OpenFoamData$E

# pivot tables used later
pivot.Tw <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "Twall")
pivot.Tj <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "Tjob")
pivot.Tw.data <- pivot.Tw[,c(2:5)]
pivot.Tj.data <- pivot.Tj[,c(2:5)]

pivot.energy <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "E")
pivot.time <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "Twall")
pivot.energy.data <- pivot.energy[,c(2:5)]
pivot.time.data <- pivot.time[,c(2:5)]

#par(mfrow=c(1,1))
#plot(pivot.time$pitzDaily, type="l")
#plot(pivot.time$cavity, type="l")

# find Pi and Pc using linear regression
ls.PiPc <- rlm(OpenFoamData$E ~ 0 + OpenFoamData$a1 + OpenFoamData$a3, psi=psi.bisquare)
summary(ls.PiPc)
r.squared <- function(ls.model) {
    y.mean <- mean(ls.model$fitted.values)
    y.mean.vec <- rep(y.mean, length(ls.model$fitted.values))
    y.mean.diff <- ls.model$fitted.values - y.mean.vec
    SStot <- sum(y.mean.diff * y.mean.diff)
    SSres <- sum(ls.model$residuals * ls.model$residuals)
    1 - SSres/SStot
}

r.squared(ls.PiPc)


Pi.PiPc <- ls.PiPc$coefficients[1]
Pc.PiPc <- ls.PiPc$coefficients[2]

# plot quality of fitting of finding Pi Pc Pm using LBM
if (doFiles)
    {
        tikz(sprintf("%s/extracted_Pi_Pc_by_LBM.tex",out.folder),width=graph.width,height=4)
    }

ncores <-  c(1:36)
par(mfrow=c(2,2))
n <- names[1]
errors <- c()
allcores <- c()
allpred <- c()
for(n in names)
    {
        cores <- c()
        cores.pred <- c()
        for (i in ncores)
            {
                d1 <- read.csv(sprintf("%s/single/%s.%i",out.folder,n, i), sep = " ")
                v1.l <- length(d1[,2])
                v1.selection <- c(1:v1.l) > (v1.l * 0.4) & c(1:v1.l) < (v1.l * 0.6)
                #v1.selection <- 1:v1.l
                v1 <- mean(d1[v1.selection,2])
                #print(sprintf("W1=%f W2=%f", 220*v1, OpenFoamData$E[OpenFoamData$Program == n & OpenFoamData$n == i]/ OpenFoamData$Twall[OpenFoamData$Program == n & OpenFoamData$n == i]))
                cores <-  rbind(cores, 220 * v1)
                #cores <- rbind(cores, OpenFoamData$E[OpenFoamData$Program == n & OpenFoamData$n == i]/ OpenFoamData$Twall[OpenFoamData$Program == n & OpenFoamData$n == i])
                allcores <- rbind(allcores, 220 * v1)
                
                m <-  OpenFoamData$m[OpenFoamData$Program == n & OpenFoamData$n == i]
                realcore <-  OpenFoamData$realcore[OpenFoamData$Program == n & OpenFoamData$n == i]
                logcore <-  OpenFoamData$logcore[OpenFoamData$Program == n & OpenFoamData$n == i]
                pred <-  Pi.PiPc + Pc.PiPc*i
                #stopifnot(a1 == 1 && a3 == (i-1))
                cores.pred <-  rbind(cores.pred, pred)
                allpred <- rbind(allpred, pred)
                errors <- rbind(errors, pred - 220 * v1)
            }
        plot(cores, main=sprintf("%s", n), xlab="Cores", ylab="Power", pch=3, ylim = c(0, max(max(cores), max(cores.pred))))
        points(cores.pred, t="l")
    }

if (doFiles)
    {
        dev.off()
    }
    

f.PiPc <- var.test(allpred, allcores)
f.PiPc$estimate
f.PiPc$p.value
t.test(allpred, allcores)
t.test(allpred, allcores, paired = T)




                                        # find Pi, Pm and Pc using linear regression
ls <- rlm(OpenFoamData$E ~ 0 + OpenFoamData$a1 + OpenFoamData$a2 + OpenFoamData$a3, psi=psi.bisquare)
summary(ls)
plot(OpenFoamData$E, resid(ls))
Pi <- ls$coefficients[1]
Pm <- ls$coefficients[2]
Pc <- ls$coefficients[3]



r.squared(ls)

        # plot quality of fitting of finding Pi Pc Pm using LBM
if (doFiles)
    {
        tikz(sprintf("%s/extracted_Pi_Pm_Pc_by_LBM.tex",out.folder),width=graph.width,height=4)
    }
        
ncores <-  c(1:36)
par(mfrow=c(2,2))
n <- names[1]
errors <- c()
allcores <- c()
allpred <- c()
for(n in names)
    {
        cores <- c()
        cores.pred <- c()
        for (i in ncores)
            {
                d1 <- read.csv(sprintf("%s/single/%s.%i",getwd(),n, i), sep = " ")
                v1.l <- length(d1[,2])
                v1.selection <- c(1:v1.l) > (v1.l * 0.4) & c(1:v1.l) < (v1.l * 0.6)
                v1 <- mean(d1[v1.selection,2])
                cores <-  rbind(cores, 220 *  v1)
                allcores <- rbind(allcores, 220 * v1)
                m <-  OpenFoamData$m[OpenFoamData$Program == n & OpenFoamData$n == i]
                realcore <-  OpenFoamData$realcore[OpenFoamData$Program == n & OpenFoamData$n == i]
                logcore <-  OpenFoamData$logcore[OpenFoamData$Program == n & OpenFoamData$n == i]
                pred <-  Pi + m*Pm + Pc*i
                cores.pred <-  rbind(cores.pred, pred)
                allpred <- rbind(allpred, pred)
                errors <- rbind(errors, pred - 220 * v1)
            }
        plot(cores, main=sprintf("%s", n), xlab="Cores", ylab="Power", pch=3, ylim = c(0, max(cores)))
        points(cores.pred, t="l")
    }

if (doFiles)
    {
        dev.off()
    }
weighted.sum.squared.errors <- sum(errors * errors / rep(var(allcores), length(errors)))
degrees.of.freedom <- length(allcores) - 1
mean.square.weighted.deviation <- weighted.sum.squared.errors / degrees.of.freedom

f <- var.test(allcores, allpred)
f$p.value
f$estimate
t.test(allpred, allcores)
t.test(allpred, allcores, paired = T)
t.test(allpred, allcores, paired = T, mu=9, conf.level = 0.95)
allpred-allcores
plot(allpred-allcores)
plot(allcores, allpred)
mean(allpred)
sd(allpred)
mean(allcores)
sd(allcores)

#################### Verify ####################

empty.0 = read.csv(sprintf("%s/empty.0", getwd()), sep = " ")
empty.0.E = 220 * mean(empty.0[,2])
empty.1 = read.csv(sprintf("%s/empty.1", getwd()), sep = " ")
empty.1.E = 220 * mean(empty.1[,2])
Pi.ref = empty.0.E
Pm.ref = empty.1.E - empty.0.E
# diff between 0 and 1 cores
cores <- c()
for (n in names)
    {
        d2 <- read.csv(sprintf("%s/single/%s.%i",getwd(), n, 1), sep = " ")
        cores <- rbind(cores, 220 * mean(d2[,2]) - empty.1.E)
    }
Pc.ref <- mean(cores)


                                        #print table of predicted Pi Pm Pc vs reference values
                                        #start with Pi + Pc only
extracted.values <- c(Pi.PiPc, Pc.PiPc)
reference.values <- c(Pi.ref, Pc.ref)
relative.error <- c(Pi.PiPc/Pi.ref - 1, Pc.PiPc/Pc.ref - 1)
df <-  data.frame(extracted.values, reference.values, relative.error)
colnames(df) <- c("Estimated", "Reference", "Relative error")
rownames(df) <- c("\\Pinfr", "\\Pc")
xtab <- xtable(df)
caption(xtab) <- "Estimated \\Pinfr and \\Pc"
label(xtab) <- "PiPc"
print(xtab, type="latex", sanitize.text.function = function(x){x}, file=sprintf("%s/PiPc.tex", out.folder))



#then add Pm


extracted.values <- c(Pi, Pm, Pc)
reference.values <- c(Pi.ref, Pm.ref, Pc.ref)
relative.error <- c(Pi/Pi.ref - 1, Pm/Pm.ref - 1, Pc/Pc.ref - 1)
df <-  data.frame(extracted.values, reference.values, relative.error)
colnames(df) <- c("Estimated", "Reference", "Relative error")
rownames(df) <- c("\\Pinfr", "\\Pm", "\\Pc")
xtab <- xtable(df)
label(xtab) <- "PiPmPc"
caption(xtab) <- "Estimated \\Pinfr, \\Pm and \\Pc"
print(xtab, type="latex", sanitize.text.function = function(x){x}, file=sprintf("%s/PiPmPc.tex", out.folder))





#table.coefficients <- data.frame(fitted = ls$coefficients, fitted.normalised = ls.w$coefficients, experiment = ls.data$coefficients)
#plot(machines, y, t="p", ylim = c(0, max(y)), xlab = "machines", ylab = "Instant Power")
#abline(coef=ls.data$coefficients, col="red")
#abline(ls.data$coefficients[1], ls.data$coefficients[2], col="red")
#abline(ls$coefficients[1], ls$coefficients[2],  col="blue")
#abline(ls.w$coefficients[1], ls.w$coefficients[2],  col="green")

# use this to predict using Pi, Pm and Pc fitted from idle measures



############################## multi task : using measured Tw and Tj ##############################

doFiles <- T
#set socketAndHyper to T if you want to see the error training with hyper and testing without
socketAndHyper <- T 

combo.1 <- c(0,0,0,0,1,1,1,2,2,3)
combo.2 <- c(0,1,2,3,1,2,3,2,3,3)

errors <- c()
table.p1 <- c()
table.p2 <- c()
table.n1 <- c()
table.n2 <- c()
table.measured <- c()
table.predicted <- c()
table.relative.error <- c()
table.relative.accuracy <- c()
table.Tw.measured <- c()
table.Tw.predicted <- c()

m <- read.csv2(sprintf("%s/singleValuesParallel.csv", getwd()), colClasses=c("numeric"), dec=".")


if (socketAndHyper)
    {
        # use Pi, Pm and Pc from  dataset with hyper threading
        #
        Pi.multi = Pi + Pm
        Pc.multi = Pc
    } else {
        a1 <- m$Twall
        a2 <- (2 * m$Twall + (m$n1 - 1) * m$Tjob1 + (m$n2 - 1) * m$Tjob2)
        ls.coeff.multi <- lm(m$E ~ 0 + a1 + a2)
        Pi.multi <-  ls.coeff.multi$coefficients[1]
        Pc.multi <-  ls.coeff.multi$coefficients[2]
    }

for(n1 in c(1,3,6))
    {
        for (n2 in c(1,3,6))
            {
                for (combo in c(1:length(combo.1)))
                    {
                        p1 <- combo.1[combo]
                        p2 <- combo.2[combo]

                        idx <- c(1:length(m$E))[ m$Program1 == p1 & m$Progra2 == p2 &  m$n1 == n1 & m$n2 == n2 ]
                        Tw <- m$Twall[idx]
                        Tj1 <- m$Tjob1[idx]  
                        Tj2 <- m$Tjob2[idx]
                        E.measured <- m$E[idx]
                        E.predicted <- (Pi.multi + Pc.multi + Pc.multi) * Tw + Pc.multi *((n1 - 1) * Tj1 + (n2 - 1) * Tj2) 
                        E.relative.error <-  E.predicted/E.measured - 1
                        errors <- c(errors, E.relative.error)
                        table.p1 <- c(table.p1, p1)
                        table.p2 <- c(table.p2, p2)
                        table.n1 <- c(table.n1, n1)
                        table.n2 <- c(table.n2, n2)
                        table.relative.error <-  c(table.relative.error, E.relative.error)
                        table.measured <- c(table.measured, E.measured)
                        table.predicted <- c(table.predicted, E.predicted)
                        table.relative.accuracy <- c(table.relative.accuracy, E.predicted/E.measured)
                        table.Tw.measured <- c(table.Tw.measured, t)
                        table.Tw.predicted <- c(table.Tw.predicted, Tw)
                    }
            }
    }

experiments <- data.frame(table.p1, table.n1, table.p2, table.n2, table.measured, table.predicted, table.relative.accuracy, table.relative.error)

if (doFiles)
    {
        tikz(sprintf("%s/details_multi_knownT.tex",out.folder),width=graph.width,height=2.5)
    }

par(mfrow=c(1,2))

plot(density(experiments[,7]), main="Energy prediction\nrelative accuracy", sub="All programs", xlab = "Relative accuracy")
abline(v=1, lty=3)



get.density <- function(i) density(experiments[experiments$table.p1 == i | experiments$table.p2 == i,7])

get.x <- function(i) get.density(i)$x
get.y <- function(i) get.density(i)$y
x.min <- min(unlist(lapply(0:3, get.x)))
x.max <- max(unlist(lapply(0:3, get.x)))
y.min <- min(unlist(lapply(0:3, get.y)))
y.max <- max(unlist(lapply(0:3, get.y)))
plot(get.density(0), main="Energy prediction\nrelative accuracy", sub="Each program", xlim = c(x.min,x.max), ylim = c(y.min,y.max), xlab = "Relative accuracy")
abline(v=1, lty=3)
points(get.density(1), t="l", col="blue")
focus <-  2
points(get.density(2), t="l", col="red")
focus <-  3
points(get.density(3), t="l", col="green")
legend.y <- y.max
legend.x <- x.min
legend(legend.x, legend.y, c(names[1], names[2], names[3], names[4]), lty=c(1,1,1), lwd=c(1,1, 1),col=c("black","blue","red","green"), cex=.5)

# RMSE
rmse(experiments[,6], experiments[,5])
# relative error
mean(experiments[,7])
sd(experiments[,7])

# http://user.physics.unc.edu/~deardorf/uncertainty/definitions.html

absolute.percentage.errors <- abs((table.predicted - table.measured)/table.measured)
mean.absolute.errors <- mean(abs(table.predicted - table.measured))




if (doFiles)
    {
        dev.off()
    }


measure.n <- 3
df <- data.frame(measure = character(measure.n), mean = numeric(measure.n), median = numeric(measure.n), percentile01 = numeric(measure.n), percentile10 = numeric(measure.n), percentile50 = numeric(measure.n), percentile90 = numeric(measure.n), percentile99 = numeric(measure.n), stringsAsFactors = F)

quantiles <- c(0.01,.1,.5,.9,.99)

addTableRow <- function(d,n,i) {
    d.quantiles <- quantile(d, quantiles)
    df$measure[i] <- n
    df$mean[i] <- mean(d)
    df$median[i] <- median(d)
    df$percentile01[i] <- d.quantiles[1]
    df$percentile10[i] <- d.quantiles[2]
    df$percentile50[i] <- d.quantiles[3]
    df$percentile90[i] <- d.quantiles[4]
    df$percentile99[i] <- d.quantiles[5]
    df
}


df <- addTableRow(table.relative.accuracy, "Relative accuracy", 1)
df <- addTableRow(table.relative.error, "Relative errors", 2)
df <- addTableRow(absolute.percentage.errors, "Absolute percentage errors", 3)

colnames(df) <- c("Measure", "Mean", "Median", "1 perc", "10 perc", "50 perc", "90 perc", "99 perc")
xtab <- xtable(df)
label(xtab) <- "accuracy_measures"
caption(xtab) <- "Accuracy measures"
digits(xtab) <- 4
print(xtab, type = "latex", file=sprintf("%s/accuracy_measures.tex", out.folder), include.rownames = F)
#print(xtab, include.rownames = F, type = "latex")



table.absolute.errors <- table.measured - table.predicted
plot(table.measured, table.predicted)
abline(0,1)





#################### regression trees ####################

alberello <- rpart(OpenFoamData$E ~  OpenFoamData$a1 + OpenFoamData$a2 + OpenFoamData$a3, method="anova")
printcp(alberello)
plotcp(alberello)
summary(alberello)
rsq.rpart(alberello)
plot(alberello, uniform=T)



install.packages("randomForest")
library(randomForest)
foresta <- randomForest(OpenFoamData$E ~  OpenFoamData$a1 + OpenFoamData$a2 + OpenFoamData$a3)
print(foresta)
importance(foresta)


#################### scheduling ####################


m <- read.csv2(sprintf("%s/singleValuesParallel.csv", getwd()), colClasses=c("numeric"), dec=".")

OpenFoamData
n1 <- 1
np1 <- 12
n2 <- 1
np2 <- 12
name1 <- names[n1+1]
name2 <- names[n2+1]

E1 <-  OpenFoamData$E[OpenFoamData$Program == name1 & OpenFoamData$n == np1]
E2 <-  OpenFoamData$E[OpenFoamData$Program == name2 & OpenFoamData$n == np2]
E12 <-  m$E[m$n1 == np1/2 & m$Program1 == n1 & m$n2 == np2/2 & m$Progra2 == n2]
(E1+E2)/E12

plot(OpenFoamData$n[OpenFoamData$Program == name1], OpenFoamData$E[OpenFoamData$Program == name1])


################ just plot completion times #########

if (doFiles)
{
  tikz(sprintf("%s/completion_times.tex",out.folder),width=graph.width,height=3)
}


#datatable = pivot.energy
datatable = pivot.time
plot(datatable$n, datatable$cavity, type = "l", col = "black", ylim = c(0, max(datatable)), xlab = "cores", ylab = "completion time (s)", main = "Programs completion times")
points(datatable$n, datatable$pitzDaily, type = "l", col = "blue")
points(datatable$n, datatable$squareBumpFine, type = "l", col = "red")
points(datatable$n, datatable$mixerVesselAMI2D, type = "l", col = "green")
legend.y <- max(datatable)
legend.x <- 26
legend(legend.x, legend.y, c(names[1], names[2], names[3], names[4]), lty=c(1,1,1), lwd=c(1,1, 1),col=c("black","blue","red","green"), cex=.5)
abline(v=13, lty = 2)
abline(v=25, lty = 2)

if (doFiles)
{
  dev.off()
}



################ just plot completion times #########

if (doFiles)
{
  tikz(sprintf("%s/speedups.tex",out.folder),width=graph.width,height=3)
}


#datatable = pivot.energy
datatable = pivot.energy
plot(datatable$n[-1], datatable$cavity[-1]/datatable$cavity[2], type = "l", col = "black", ylim = c(0, max(datatable/datatable$cavity[2])), xlab = "cores", ylab = "completion time (s)", main = "Programs completion times")
points(datatable$n[-1], datatable$pitzDaily[-1]/datatable$pitzDaily[2], type = "l", col = "blue")
points(datatable$n[-1], datatable$squareBumpFine[-1]/datatable$squareBumpFine[2], type = "l", col = "red")
points(datatable$n[-1], datatable$mixerVesselAMI2D[-1]/datatable$mixerVesselAMI2D[2], type = "l", col = "green")
legend.y <- max(datatable)
legend.x <- 26
legend(legend.x, legend.y, c(names[1], names[2], names[3], names[4]), lty=c(1,1,1), lwd=c(1,1, 1),col=c("black","blue","red","green"), cex=.5)
abline(v=13, lty = 2)
abline(v=25, lty = 2)

if (doFiles)
{
  dev.off()
}


################## show power prediction ##################

i = 30


progIndex = 4

if (doFiles)
{
  tikz(sprintf("%s/instant_power_%i.tex",out.folder, progIndex),width=graph.width,height=6)
}

par(mfrow=c(3,4), oma = c(0,0,2,0))
program = names[progIndex]
for(ncores in seq(3,36,3))
{
  #ncores = 12
  nmachines = (ncores - 1) %/% 12 + 1
  
  #OpenFoamData$Program == program && OpenFoamData$n = ncores && OpenFoamData$m == nmachines
  lev1 = Pi + nmachines * Pm + Pc
  lev2 = Pi + nmachines * Pm + Pc * ncores
  power.data = read.csv(sprintf("%s/single/%s.%i",out.folder,program, ncores), sep = " ")
  ypower = as.numeric(sapply(power.data[2], function(x){x}))*220
  xtime = c(1:length(ypower))/10
  plot(xtime, ypower, type="l", main = sprintf("%i cores", ncores), xlab = "Time (s)", ylab = "Power (W)")
  abline(h=lev1, col = "green")
  abline(h=lev1+9, lty = 2, col = "green")
  abline(h=lev1-9, lty = 2, col = "green")
  abline(h=lev2, col = "red")
  abline(h=lev2+9, lty = 2, col = "red")
  abline(h=lev2-9, lty = 2, col = "red")
}
mtext(sprintf("Instant power consumption of %s", program), outer = TRUE)
if (doFiles)
{
  dev.off()
}





