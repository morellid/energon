set terminal epslatex size 15.0cm,5cm
set output 'speedup.tex'

set multiplot layout 1,2

set boxwidth 0.5
set style fill solid

set key font ",8"


set title "Energy speedup"
set xlabel "Cores"
set ylabel "Serial to parallel energy ratio"
plot "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' pitzDaily.dat" w lines lt rgb "#00FF00" t "pitzDaily", \
     "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' mixerVesselAMI2D.dat" w lines lt rgb "#FF0000" t "mixerVesselAMI2D", "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' squareBumpFine.dat" w lines lt rgb "#0000FF" t "squareBump"



set title "Time speedup"
set xlabel "Cores"
set ylabel "Serial to parallel time ratio"
plot "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' pitzDaily.dat" w lines lt rgb "#00FF00" t "pitzDaily", \
     "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' mixerVesselAMI2D.dat" w lines lt rgb "#FF0000" t "mixerVesselAMI2D", "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' squareBumpFine.dat" w lines lt rgb "#0000FF" t "squareBump"
