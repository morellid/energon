set terminal epslatex size 15.0cm,5cm
set output 'measures.tex'

set multiplot layout 1,2

set boxwidth 0.5
set style fill solid

set key font ",8"


set title "Energy consumption"
set xlabel "Cores"
set ylabel "Energy (J)"
plot 'pitzDaily.dat' u 2:3 w lines lt rgb "#00FF00" t "pitzDaily", \
     'mixerVesselAMI2D.dat' u 2:3 w lines lt rgb "#FF0000" t "mixerVesselAMI2D", 'squareBumpFine.dat' u 2:3 w lines lt rgb "#0000FF" t "squareBump"



set title "Completion time"
set xlabel "Cores"
set ylabel "Time (s)"
plot 'pitzDaily.dat' u 2:4 w lines lt rgb "#00FF00" t "pitzDaily", \
     'mixerVesselAMI2D.dat' u 2:4 w lines lt rgb "#FF0000" t "mixerVesselAMI2D", 'squareBumpFine.dat' u 2:4 w lines  lt rgb "#0000FF"t "squareBump"
