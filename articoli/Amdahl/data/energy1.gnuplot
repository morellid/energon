set terminal epslatex size 15.0cm,10cm
set output 'energy1.tex'

set multiplot layout 2,2

set boxwidth 0.5
set style fill solid

set key font ",8"

coresPerMachine = 8
T1 = 1
Tserial = 0.0
Tjob(x,p) =T1 *  p + T1*(1-p)/(x)
Twall(x,p) = Tserial + Tjob(x,p)
Pinfr = 1
Pm = 1
m(x) = ceil(x/coresPerMachine)
Pc = 0.1
E(x,p) =  Twall(x,p) * (Pinfr + m(x) * Pm + Pc) + (x-1) * Pc * Tjob(x,p)

set xrange [1:128]
set xlabel "Cores"

set logscale y

set title "Time using plain Amdahl"
set ylabel "Time"

set yrange [0.005:1.2]

plot Twall(x,0) w lines lt rgb "#00FF00" t "p=0", \
	Twall(x,0.01) w lines lt rgb "#44AA00" t "p=0.01", \
	Twall(x,0.1) w lines lt rgb "#AA4400" t "p=0.1", \
	Twall(x,1) w lines lt rgb "#FF0000" t "p=1"

set title "Energy using plain Amdahl"
set ylabel "Energy"
set autoscale y
plot E(x,0) w lines lt rgb "#00FF00" t "p=0", \
	E(x,0.01) w lines lt rgb "#44AA00" t "p=0.01", \
	E(x,0.1) w lines lt rgb "#AA4400" t "p=0.1", \
	E(x,1) w lines lt rgb "#FF0000" t "p=1"




coresPerMachine = 8
T1 = 1
Tserial = 0.0
Tjob(x,p) =T1 *  p + T1*(1-p)/(x) + 0.001*x
Twall(x,p) = Tserial + Tjob(x,p)
Pinfr = 1
Pm = 1
m(x) = ceil(x/coresPerMachine)
Pc = 0.1
E(x,p) =  Twall(x,p) * (Pinfr + m(x) * Pm + Pc) + (x-1) * Pc * Tjob(x,p)

set xrange [1:128]
set xlabel "Cores"

set logscale y

set title "Time using corrected Amdahl"
set ylabel "Time"

set yrange [0.05:1.2]

plot Twall(x,0) w lines lt rgb "#00FF00" t "p=0", \
	Twall(x,0.01) w lines lt rgb "#44AA00" t "p=0.01", \
	Twall(x,0.1) w lines lt rgb "#AA4400" t "p=0.1", \
	Twall(x,1) w lines lt rgb "#FF0000" t "p=1"

set title "Energy using corrected Amdahl"
set ylabel "Energy"
set autoscale y
plot E(x,0) w lines lt rgb "#00FF00" t "p=0", \
	E(x,0.01) w lines lt rgb "#44AA00" t "p=0.01", \
	E(x,0.1) w lines lt rgb "#AA4400" t "p=0.1", \
	E(x,1) w lines lt rgb "#FF0000" t "p=1"

