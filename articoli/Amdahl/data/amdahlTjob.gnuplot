set terminal epslatex size 15.0cm,10cm
set output 'amdahl'.program.'.tex'
set key inside top right
set xlabel 'Cores'
set ylabel 'Time'
set title program.' TJob prediction using Amdahl'
plot  "amdahlTjob".i.".dat" using 1:2 title 'Measured' with lines, "amdahlTjob".i.".dat" using 1:3 title 'Modeled with Amdahl' with lines
