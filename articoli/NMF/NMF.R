

# Install
# install.packages("NMF") # Load
library(NMF)
library(reshape)



out.folder <-  "/Users/davidemorelli/Projects/energy-model-paper/data"
names <- c("cavity", "pitzDaily", "squareBumpFine", "mixerVesselAMI2D") 

graph.width <- 6.0

rmse <- function(sim, obs)
{
  sqrt( mean( (sim - obs)^2) )
}

singleValueFile <- sprintf("%s/singleValues.csv", out.folder)

OpenFoamData <- read.csv(file=singleValueFile, header=TRUE,sep=";", colClasses=c("character", rep("numeric",14)), row.names = NULL)

# pivot tables used later
pivot.Tw <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "Twall")
pivot.Tj <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "Tjob")
pivot.Tw.data <- pivot.Tw[,c(2:5)]
pivot.Tj.data <- pivot.Tj[,c(2:5)]

pivot.energy <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "E")
pivot.time <- cast(OpenFoamData, n ~  Program, function(x) x[1], value = "Twall")
pivot.energy.data <- pivot.energy[,c(2:5)]
pivot.time.data <- pivot.time[,c(2:5)]


#plot(pivot.energy)
#plot(pivot.energy$cavity)

cputime.cavity = pivot.Tj$cavity * (pivot.Tj$n - 1) + pivot.Tw$cavity 
cputime = pivot.Tj * (pivot.Tj$n - 1) + pivot.Tw 
cputime$n = pivot.Tj$n
cputime[-1]

X = data.frame(cavity = pivot.time$cavity, pitzDaily = pivot.time$pitzDaily, squareBumb = pivot.time$squareBumpFine, mixerVesselAMI2D = pivot.time$mixerVesselAMI2D)
plot(X)
plot(X$cavity)

install.packages("fastICA")
library(fastICA)

par(mfrow = c(2,1))
res = nmf(cputime[-1], 3, seed = "ica")
basismap(res)
coefmap(res)

?nmf
?nmfSeed

plot(cputime[-1])

par(mfrow = c(1,1))
plot(cputime[2]$cavity, type = "l", col = "black")
points(cputime[3]$mixerVesselAMI2D, type = "l", col = "red")
points(cputime[4]$pitzDaily, type = "l", col = "blue")
points(cputime[5]$squareBumpFine, type = "l", col = "green")

par(mfrow = c(2,1))
n <- 50; counts <- c(5, 5, 8);
V <- syntheticNMF(n, counts)
# perform a 3-rank NMF using the default algorithm
res <- nmf(V, 3)
basismap(res)
coefmap(res)

?nmf
