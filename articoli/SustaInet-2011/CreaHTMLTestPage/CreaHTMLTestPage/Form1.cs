﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CreaHTMLTestPage
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private string digitFormat = "D4";

        //private int maxID = 8 + 4 + 2 + 1;
        private int maxID = 1024 + 512 + 256 + 128 + 64 + 32 + 16 + 8 + 4 + 2 + 1;
        enum jskit
        {
            jquery,
            dojo,
            prototype
        }

        private jskit currentkit = jskit.prototype;

        private void button1_Click(object sender, EventArgs e)
        {
            StringBuilder html = new StringBuilder();
            html.AppendLine(@"
<!doctype html>
<html>
  <head>
    <title>TEST JS</title>");
            switch (currentkit)
            {
                case jskit.jquery:
                    html.AppendFormat(@"
    <script type=""text/javascript"" src=""http://code.jquery.com/jquery-1.4.4.min.js""></script>");
                    break;
                case jskit.dojo:
                    html.AppendLine(@"
    <script src=""http://ajax.googleapis.com/ajax/libs/dojo/1.5/dojo/dojo.xd.js""></script>");
                    break;
                case jskit.prototype:
                    html.AppendLine(@"
    <script src=""https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js""></script>");
                    break;
                default:
                    break;
            }
            html.AppendLine(@"
<STYLE TYPE=""text/css"">
  .BigFontsize { font-size: x-large;}
  .DifferentFont {font-family:""Times New Roman"", Times, serif;}
  .Bold { font-weight: bold;}
  .RedBackground {background-color:red;}
  .GreenForeground {color: green;}
  .WithBorder { border-style:solid; border-width:5px;}
  .ZeroPadding {padding: 0px;}
  .Invisible {visibility:hidden;}
</STYLE>  </head>
  <body>");
            AppendChild(html, 0);
            html.AppendFormat("\r\n<br/>Total DIVs={0}", counter);

            html.Append(@"
<script type=""text/javascript"">
var currTest = 0;
var nTests = 500;
function execTest()
{
    var n = 'tests.f' + currTest + '()';
    eval(n);
    currTest = (currTest+1) % 500;
}
var tests = {
");
            for (int i = 0; i < 500; i++)
            {
                if (i > 0) html.Append(",");
                switch (currentkit)
                {
                    case jskit.jquery:
                        html.AppendLine(string.Format("f{0}: {1}", i, MakeTestJQuery()));
                        break;
                    case jskit.dojo:
                        html.AppendLine(string.Format("f{0}: {1}", i, MakeTestDojo()));
                        break;
                    case jskit.prototype:
                        html.AppendLine(string.Format("f{0}: {1}", i, MakeTestPrototype()));
                        break;
                    default:
                        break;
                }
            }
                html.AppendLine(@"
};
    setInterval(""execTest()"",40);
    </script>");

            html.AppendLine(@"
  </body>
</html>");
            File.WriteAllText(string.Format("TestJS{0}.HTML", currentkit.ToString()), html.ToString());

        }

        Random r = new Random(0);
        int counter = 0;
        string[] classes = { "DifferentFont", "Bold", "Invisible", "BigFontsize", "RedBackground", "GreenForeground", "WithBorder", "ZeroPadding" };

        private string MakeTestJQuery()
        {
            string selector = "";
            string action = "";
            int randSelector = r.Next(2);
            switch (randSelector)
            {
                case 0:
                    // cerco un name a caso
                    int randID = r.Next(maxID);
                    selector = string.Format("[name=\"{0}\"]", IDtoName(randID));
                    break;
                case 1:
                    // cerco una classe a caso
                    selector = string.Format(".{0}", classes[r.Next(classes.Length)]);
                    break;
                default:
                    break;
            }
            int randAction = r.Next(1);
            switch (randAction)
            {
                case 0:
                    // cambia classe
                    //string newtext = RandomText();
                    string randomClass = classes[r.Next(classes.Length)];
                    action = string.Format("toggleClass('{0}')", randomClass);
                    break;
                default:
                    break;
            }
            return MakeJSFunctionJQuery(selector, action);
        }

        private string MakeJSFunctionJQuery(string selector, string action)
        {
            return string.Format(@"function() {{ $('{0}').{1}; }}", selector, action);
        }

        private string MakeTestDojo()
        {
            string selector = "";
            string action = "";
            int randSelector = r.Next(2);
            switch (randSelector)
            {
                case 0:
                    // cerco un name a caso
                    int randID = r.Next(maxID);
                    selector = string.Format("[name={0}]", IDtoName(randID));
                    break;
                case 1:
                    // cerco una classe a caso
                    selector = string.Format(".{0}", classes[r.Next(classes.Length)]);
                    break;
                default:
                    break;
            }
            int randAction = r.Next(1);
            switch (randAction)
            {
                case 0:
                    // cambia classe
                    //string newtext = RandomText();
                    string randomClass = classes[r.Next(classes.Length)];
                    action = string.Format(".toggleClass('{0}')", randomClass);
                    break;
                default:
                    break;
            }
            return MakeJSFunctionDojo(selector, action);
        }

        private string MakeJSFunctionDojo(string selector, string action)
        {
            return string.Format(@"function() {{ dojo.query(""{0}""){1} }}", selector, action);
        }

        private string MakeTestPrototype()
        {
            string selector = "";
            string action = "";
            int randSelector = r.Next(2);
            switch (randSelector)
            {
                case 0:
                    // cerco un name a caso
                    int randID = r.Next(maxID);
                    selector = string.Format("[name=\"{0}\"]", IDtoName(randID));
                    break;
                case 1:
                    // cerco una classe a caso
                    selector = string.Format(".{0}", classes[r.Next(classes.Length)]);
                    break;
                default:
                    break;
            }
            int randAction = r.Next(1);
            switch (randAction)
            {
                case 0:
                    // cambia classe
                    //string newtext = RandomText();
                    string randomClass = classes[r.Next(classes.Length)];
                    action = string.Format("toggleClassName('{0}')", randomClass);
                    break;
                default:
                    break;
            }
            return MakeJSFunctionPrototype(selector, action);
        }

        private string MakeJSFunctionPrototype(string selector, string action)
        {
            return string.Format(@"function() {{ $$('{0}').each(function(s){{ s.{1} }}); }}", selector, action);
        }


        private string IDtoName(int id)
        {
            return string.Format("node{0}", id.ToString(digitFormat));
        }
        private string randomClass()
        {
            StringBuilder sb = new StringBuilder();
            int howMany = r.Next(classes.Length);
            List<string> classesBucket = new List<string>(classes);
            for (int i = 0; i < howMany; i++)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                int chosenIndex = r.Next(classesBucket.Count);
                sb.Append(classesBucket[chosenIndex]);
                classesBucket.RemoveAt(chosenIndex);
            }
            return sb.ToString();
        }

        private string RandomText()
        {
            List<string> words = new List<string>("Lorem ipsum dolor sit amet, consectetur adipiscing elit.".Split(new char[] { ' ' }));
            int nwords = r.Next(words.Count);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < nwords; i++)
            {
                int nextrand = r.Next(words.Count);
                sb.Append(words[nextrand]);
                words.RemoveAt(nextrand);
            }
            return sb.ToString();
        }



        private void AppendChild(StringBuilder sb, int level)
        {
            string name = IDtoName(counter++);
            sb.AppendFormat("\r\n<div id=\"{0}\" name=\"{0}\" class=\"{1}\">", name, ""/*randomClass()*/);
            sb.AppendLine("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");

            if (level < 10)
            {
                AppendChild(sb, level + 1);
                AppendChild(sb, level + 1);
            }
            sb.AppendLine(@"</div>");
        }
    }
}
