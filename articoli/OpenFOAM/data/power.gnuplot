set terminal epslatex size 15.0cm,15cm
set output 'power.tex'

set multiplot layout 4,1
#set multiplot

set boxwidth 0.5
set style fill solid
set key out vert 
set key top right

set linestyle 1 lt 1 lc 1
set linestyle 2 lt 2 lc 2
set linestyle 3 lt 3 lc 3

set ytics 250

case = "cavity"

set title "cavity"
set xlabel "Time (s)"
set ylabel "Power (W)"
plot "<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".24" u 1:2 w lines lt rgb "#0000FF" t "24 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".48" u 1:2 w lines lt rgb "#00FF00" t "48 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".72" u 1:2 w lines lt rgb "#FF0000" t "72 cores"

case = "pitzDaily"

set title case
set xlabel "Time (s)"
set ylabel "Power (W)"
plot "<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".24" u 1:2 w lines lt rgb "#0000FF" t "24 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".48" u 1:2 w lines lt rgb "#00FF00" t "48 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".72" u 1:2 w lines lt rgb "#FF0000" t "72 cores"

case = "squareBumpFine"

set title "squareBump"
set xlabel "Time (s)"
set ylabel "Power (W)"
plot "<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".24" u 1:2 w lines lt rgb "#0000FF" t "24 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".48" u 1:2 w lines lt rgb "#00FF00" t "48 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".72" u 1:2 w lines lt rgb "#FF0000" t "72 cores"

case = "mixerVesselAMI2D"

set title case
set xlabel "Time (s)"
set ylabel "Power (W)"
plot "<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".24" u 1:2 w lines lt rgb "#0000FF" t "24 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".48" u 1:2 w lines lt rgb "#00FF00" t "48 cores", \
	"<awk '{if(NR==1){shift=$1} print (($1 - shift)/10000000.0), ($2*220.0) }' ".case.".72" u 1:2 w lines lt rgb "#FF0000" t "72 cores"

unset multiplot
