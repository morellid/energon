set terminal epslatex size 13.0cm,7cm
set output 'speedup.tex'

set multiplot layout 1,2

set boxwidth 0.5
set style fill solid
set key samplen 1

set key font ",8"

set xtics (1,24,48,72)

set title "Energy speedup"
set xlabel "Cores" offset 1
set ylabel "Serial to parallel energy ratio" offset 1
plot "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' cavity.dat" w lines lt rgb "#0000FF" t "cavity", \
     "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' pitz.dat" w lines lt rgb "#00FF00" t "pitzDaily", \
     "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' mixer.dat" w lines lt rgb "#FF0000" t "mixerVesselAMI2D", "<awk '{if(NR==1){serial=$3} print $2,(serial/$3) }' square.dat" w lines lt rgb "#FFAA00" t "squareBump"



set title "Time speedup"
set xlabel "Cores" offset 1
set ylabel "Serial to parallel time ratio" offset 1
plot "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' cavity.dat" w lines lt rgb "#0000FF" t "cavity", \
     "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' pitz.dat" w lines lt rgb "#00FF00" t "pitzDaily", \
     "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' mixer.dat" w lines lt rgb "#FF0000" t "mixerVesselAMI2D", "<awk '{if(NR==1){serial=$4} print $2,(serial/$4) }' square.dat" w lines lt rgb "#FFAA00" t "squareBump"
