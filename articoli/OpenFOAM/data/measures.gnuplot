set terminal epslatex size 13.0cm,7cm
set output 'measures.tex'

set multiplot layout 1,2

set boxwidth 0.5
set style fill solid
set key samplen 1

set key font ",8"

set xtics (1,24,48,72)

set title "Energy consumption"
set xlabel "Cores" offset 1
set ylabel "Energy (J)" offset 1
plot 'cavity.dat' u 2:3 w lines lt rgb "#0000FF" t "cavity", \
     'pitz.dat' u 2:3 w lines lt rgb "#00FF00" t "pitzDaily", \
     'mixer.dat' u 2:3 w lines lt rgb "#FF0000" t "mixerVesselAMI2D", 'square.dat' u 2:3 w lines lt rgb "#FFAA00" t "squareBump"



set title "Completion time"
set xlabel "Cores" offset 1
set ylabel "Time (s)" offset 1
plot 'cavity.dat' u 2:4 w lines lt rgb "#0000FF" t "cavity", \
     'pitz.dat' u 2:4 w lines lt rgb "#00FF00" t "pitzDaily", \
     'mixer.dat' u 2:4 w lines lt rgb "#FF0000" t "mixerVesselAMI2D", 'square.dat' u 2:4 w lines  lt rgb "#FFAA00"t "squareBump"
