%This is a template for producing OASIcs articles.
%See oasics-manual.pdf for further information.

\documentclass{llncs}%

\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amssymb}
%\usepackage{amsthm}
\usepackage{amsmath}
%\usepackage{setspace}


\graphicspath{ {./data/} }

\bibliographystyle{plain}

\begin{document}

\title{Accurate blind predictions of OpenFOAM energy consumption using the LBM prediction model}
%\subtitle{Do you have a subtitle?\\ If so, write it here}

%\titlerunning{LBM prediction model}        % if too long for running head

\author{Davide Morelli \inst{1} \and Antonio Cisternino \inst{1} }

%\authorrunning{Short form of author list} % if too long for running head

\institute{Computer Science Department, University of Pisa \\
              Largo Bruno Pontecorvo 3, 56127 Pisa \\ 
              \email{davide.morelli@unipi.it} \\
              \email{cisterni@di.unipi.it} \\
              }


\maketitle



\begin{abstract}


The ability to predict the energy consumption of an HPC task, varying the number of assigned nodes, can lead to the ability to assign the correct number of nodes to tasks, saving large amount of energy. 

In this paper we present LBM, a model capable of predicting the resource usage (applicable to different resources, such as completion time and energy consumption) of programs, following a black box approach, where only passive measures of the running program are used to build the prediction model, without requiring its source code, or static analysis of the binary. LBM builds the predicting model using other programs as benchmarks. 
We tested LBM predicting the energy consumption of \emph{pitzDaily}, a case of the OpenFOAM CFD suite, using a very low number of benchmarks (3), obtaining extremely precise predictions.



\end{abstract}


\section{Introduction}

The efficiency of the HPC scheduler can benefit in many ways from the ability to predict the amount of resources a job will require to complete. In HPC completion time has traditionally been the most important resource to save, but in the last years we are witnessing a rising interest in reducing energy consumption. 

Because of communication time, the number of nodes that will minimize the completion time is not necessarily the highest, i.e. we will have a minimum. When we try to minimize energy consumption we could have a different minimum than the one found when minimizing completion time. A scheduler should be able to predict the correct number of nodes that will minimize the consumption of the resource of interest, usually it will have a limited number of measures at its disposal (as the number of compute nodes varies).

We propose a linear model that leverages on measures taken on a few benchmarks running on a variable number of nodes to predict the resource consumption of a target program, as the number of compute nodes changes.


As shown in \cite{hoste_performance_2006,phansalkar_measuring_2005,sharkawi_performance_2009,sankaran_energy_2013} a resource can be used to predict other resources (i.e. performance counters can be used to predict energy consumption). Our effort is to unify existing approaches dedicated to different resources (in particular, but not limiting to, completion time and energy), creating a more abstract model that can to characterize programs behavior in a more general sense, predicting different resources from the ones used to build a prediction model. Energy characterization and prediction could then take advantage of the vast literature in performance prediction of software.

% why blind predictions

Prediction models usually require instrumentation of the system, and involve simulation and other complex computationally intensive tasks \cite{annavaram_fuzzy_2004,lau_strong_2005}. Resource usage prediction should be performed without knowledge of the program's source code, as in most of the real world scenarios source code is not known. The resource usage prediction model should be able to rely only on data that can be measured running the programs, implementing a \emph{blind} approach. Characterization and prediction should be performed relying only on the informations about running programs that are usually available to the operative system, without the need of additional hardware or manipulations of the binary. For this reason we decided to test our linear prediction model only using information easily available in an HPC cluster (completion time and instant power through the PDU).

% unified model

The resource prediction model should also be as abstract as possible, avoiding relying on a specific micro architecture or resource kind (as in \cite{neugebauer_energy_2001}), as the model would become obsolete before it could become widely used. For a program's descriptive and predictive model to be useful, it should be portable on different hardware from the one where the model was built. 

% competitors

The models that achieve a low prediction error are very narrow, focusing on a particular micro architecture \cite{tiwari_power_1994,brooks_wattch:_2000,steinke_accurate_2001,snavely_modeling_2001,russell_software_1998}, resource (i.e. completion time, energy consumption), even programming language \cite{tiwari_power_1994,arnold_online_2002}, they also usually model execution down to the single instruction level of detail \cite{sinha_jouletrack_2001,steinke_accurate_2001,russell_software_1998}, hiding the interaction between instructions that changes from architecture to architecture, making it difficult to abstract the results to a different architecture. 

% the solution

We developed a simple linear model that leverages on resource usage measures to predict the usage of other resources, the model was designed to be as simple as possible, capable of predicting and describing resource consumption of both hardware and software, not focusing on particular architectures or resources. The model is black-box, it does not require the source code of the program being measured. Because it relies on measures of benchmarks (non linear regressors) it can capture non linear phenomena even if based on linear regression.

% model validation using SPEC CPU 2006

We validated our model predicting the energy consumed by a cases of the OpenFOAM suite, using a very limited set of measures of other 3 cases used as benchmarks, running on 1, 2 or 3 compute nodes (24, 48 or 72 processors).


\section{The LBM model}

Benchmarking is currently more an art than a science where the performance of a system $S$ is measured against a particular test
$T$ in order to characterize the performance of S with respect to $T$. If $T$ captures a particular feature of a program $P$ we may infer that if $T$ performs well on $S$ then $P$ will follow the same pattern. Moreover, by varying either $S$ or $T$ it is possible to compare different systems against a benchmark or different benchmark against a given system. The quality of a benchmark $T$ is given by the implicit power of capturing a predictive aspect, though what prediction means is often hinted without formal specification.

As witnessed by \cite{rivoire_models_2008}, benchmarking is largely driven by industry and practitioners rather than a well defined theory, this is due to the fact that it is difficult to relate a particular program $P$ with a particular test $T$.

We propose LBM (Linear Benchmarking Model), a model designed to describe the relation between a set of benchmarks and a program.

LBM is a generalization  of \cite{tiwari_power_1994,hoste_performance_2006,snavely_modeling_2001,sharkawi_performance_2009}, designed to be resource agnostic, it can be used to characterize both hardware and software as shown in \cite{morelli_compositional_2012}, and can predict the completion time as well as the energy consumption, the allocated memory, the number of cache misses, etc. The contribution of LBM lies in its capability of dealing with measures of different resources, without requiring access to source code or binary instrumentation nor simulation. LBM is also a simple and straightforward model, that allows intuitive interpretations of the surrogate built with it, using linear combinations of predictors.

Resource consumption of programs is known to be a non linear phenomenon, therefore using linear regression could seem a wrong approach. LBM uses other programs as regressors (non linear), hence combining them linearly we still will be able to describe the non linear behavior of the target program.

\subsection{Definitions}

In this section we provide the definition for terms used in the rest of the paper for presenting the case study.

%\newtheorem*{definition}{Definition}

\begin{definition}[program]
A program is a particular and defined sequence of instructions.  
\end{definition}
The same \emph{program} can be run on different micro architectures, even if will generate different low level sequence of processor instructions, it will still be considered the same \emph{program}. When called to process different input sizes, because the sequence of high level instructions will considerably change, it will be considered a different program. The \emph{target program} is the program whose resource consumption we are interested in predicting.

\begin{definition}[computational environment]
A computational environment is any computational system that can execute programs. 
\end{definition}
Examples of \emph{computational environments} are embedded computers, smart phones, PCs with different micro-architectures, clusters. We consider part of the \emph{computational environment} the hardware as well as the operative system and all the software running on the machine at the same time as the \emph{program} being measured.


\begin{definition}[resource]
A resource is a finite asset of the computational environment that is used by programs to run.
\end{definition}

The energy used by a computer, or the time used to complete a \emph{program} are examples of \emph{resources}. The same resource on different \emph{computational environments} are considered different \emph{resources}: e.g. completion time on computer A and completion time on computer B are different \emph{resources}.

Not every \emph{resource} can be used in our model, it needs to provide measures that have the following properties: non negativity ($\forall x$ $\mu(x) \geq 0 $), null empty set ($\mu(\varnothing)=0$) and countable additivity ($\mu(\cup x_i) = \sum \mu(x_i)$). 

Examples of valid resources are processor time, completion time, memory allocations, energy. Examples of invalid resources are \% processor time (it may decrease), active memory (memory could be deallocated), power (instant power could decrease). Usually invalid resources can be made valid combining them with time. 

An interesting example is power that is not a valid \emph{resource}. Let's consider a program $X$ that is composed of 2 \emph{programs} $A$ and $B$ executed sequentially: 
$
X = \{ A ; B \}
$

if $P_a$ the average power during the execution of $A$ is higher than $P_b$ the average power during the execution of $B$, then $P_x$ the average power during the execution of $X$ will be $P_b \leq P_x \leq P_a $. This violates the required countable additivity property (the power used by a part of a \emph{program} is higher than the power used by the whole \emph{program}). On the other hand, because both average power and completion time are always positive quantities, countable additivity holds for energy: $E_x = E_a + E_b = P_a T_a + P_b T_b $, $E_x \geq E_a$, $E_x \geq E_b$ (where $T_a$ and $T_b$ are completion times for $A$ and $B$).

\begin{definition}[target resource]
The target resource is the resource that we want to predict for the target program.
\end{definition}

\begin{definition}[target program]
The target program is the program that we want to model in terms of the benchmarks, whose target resource we are interested in predicting.
\end{definition}

\begin{definition}[measure]
A measure is a positive real number that describes the quantity of resource used by a certain program to run on a certain computational environment.
\end{definition}

A \emph{measure} always refers to both a \emph{program} and a \emph{resource} (therefore a \emph{computational environment}), i.e. a \emph{measure} quantifies the usage of a particular resource on a particular \emph{computational environment} by a \emph{program}.

\begin{definition}[target measure]
The target measure is the measure of the target resource and the target program that we want to predict.
\end{definition}


\begin{definition}[computational pattern]
A computational pattern is an ideal program the exhibits a peculiar resource consumption.
\end{definition}
Examples of \emph{computational patterns} are: a \emph{program} made in its entirety by floating point operations; or a \emph{program} that triggers a cache miss at every instruction. \emph{Computational patterns} are orthogonal to each other. \emph{Computational patterns} are usually ideal \emph{programs}, real \emph{programs} can not consist only of a single \emph{computational pattern}. At most synthetic \emph{benchmarks} can approximate particular \emph{computational patterns}. Some \emph{computational pattern} could be reasonably be guessed (in some case even designed), but in general they are unknown, and may arise when new micro-architectures are created: a novel micro-architecture could expose a peculiar resource usage when used by a certain sequence of instructions.

The model assumes that all \emph{programs} can ideally be decomposed in sequences of \emph{computational patterns}.
The \emph{computational patterns} form a basis of the resource consumption space (because they are orthogonal with respect to resource consumption). Any program, including both the \emph{benchmark} and the \emph{target program}, can be written as a linear combination of the \emph{computational patterns}. If every \emph{computational pattern} used by the \emph{target program} is contained at least in one of the \emph{benchmarks}, and if the \emph{benchmarks} are not linearly dependent, we can operate a change of basis and express the \emph{target program} as a linear combination of the \emph{benchmarks}. If the \emph{target program} contains \emph{computational patterns} that are not contained in any \emph{benchmark}, then the change of basis will lose information.


\begin{definition}[benchmark]
A benchmark is a program used to predict the measure of the target resource and the target program. 
\end{definition}

\begin{definition}[surrogate]
A surrogate is a linear combination of benchmarks used as a model to predict the resource consumption of the target program. 
\end{definition}

The surrogate does not depend on the \emph{target resource} or \emph{computational environment}.

\begin{definition}[solver]
A solver is an algorithm that, given a set of measures of the benchmarks and the target program, creates a surrogate for it. 
\end{definition}

In this work we use linear regression as the solver.



\subsection{Model definition}

Given a set of benchmarks and resources $\mathbf{A}$, LBM defines the relation between a program $p_t$ and $\mathbf{A}$. Every benchmark is characterized by a set of measurements, each relating with one of the model's resources. A combination of benchmarks and resources defines an LBM model.

More precisely measures, resources, benchmarks, target program, target resource and target measure are organized as follows:

\begin{itemize}
\item $\mathbf{A}$ is a matrix that contains the measures of the resources used by the benchmarks, this matrix does not contain the target resource. Each row of $\mathbf{A}$ contains the measures relative to a resource, each column contains the measures relative to a benchmark. 
\item $\mathbf{b}$ is a vector that contains the measures of the resources used by the target program. This vector contains measures relative to the same resources used to build $\mathbf{A}$: $\mathbf{b_i}$ is the measure of the target program usage of the same resource measured by the values contained in the $i^{th}$ row $\mathbf{A}$.
\item $\mathbf{c}$ is the vector that contains measures of the resource that we want to predict used by the benchmarks. This vector contains measures relative to the same benchmarks used to build $\mathbf{A}$: $\mathbf{c_i}$ is the measure of the target resource used by the benchmark whose values are contained in the $i^{th}$ column of $\mathbf{A}$.
\item $\mathbf{x}$ is the surrogate of the target program with respect to the benchmarks.
\item $\mu_t$ is the target measure.
\end{itemize}

$\mathbf{A}$, $\mathbf{b}$, $\mathbf{c}$, $\mathbf{x}$ and $\mu_t$ are linked by the following equations:

\begin{equation}
\mathbf{Ax}=\mathbf{b}
\label{find_surrogate}
\end{equation}

\begin{equation}
\mathbf{cx}=\mu_t
\label{prediction}
\end{equation}

LBM models can be used to predict target resources: given a set of measures of resources for a set of benchmarks and a target program $p_t$, we can express $p_t$ as a linear combination of the benchmarks: using equation \ref{find_surrogate} we can find $x$. We can add a resource (called target resource) for which we have measures for the benchmarks but not for the target program, LBM can predict the measure of the target resource for the target program (called target measure), using equation \ref{prediction}.  

Consider the following example: we have 3 different subset of nodes of a uniform cluster $S_1$, $S_2$ and $S_3$, we have measured the energy consumption of a CPU bound program $b_1$ and a communication bound program $b_2$ on all the subsets, we have also measured a program $p_t$ on $S_1$ and $S_2$, and we want to predict the energy consumption of $p_t$ on $S_3$. Energy consumption on $S_1$, $S_2$ and $S_3$ are resources, with $S_3$ being the target resource; $p_t$ is the target program, the measure of the energy consumption of $p_t$ on $S_3$ is the target measure. The measures ($\mu$) of $b_1$ and $b_2$ on $S_1$ and $S_2$ will form $A$:
\[
\mathbf{A} = 
\begin{pmatrix}
\mu(b_1,S_1) & \mu(b_2,S_1) \\
\mu(b_1,S_2) & \mu(b_2,S_2) \\
\end{pmatrix}
\]

The measures we have for the target program $p_t$ will form vector $\mathbf{b}$, with the same resources and in the same order as in $\mathbf{A}$:
\[
\mathbf{b} = 
\begin{pmatrix}
\mu(p,S_1) & \mu(p,S_2) \\
\end{pmatrix}
\]

The measures for the target resource for the benchmarks ($b_1$ and $b_2$) will form vector $\mathbf{c}$, with the same programs and in the same order as in $\mathbf{A}$:
\[
\mathbf{c} = 
\begin{pmatrix}
\mu(b_1,S_3) & \mu(b_2,S_3) \\
\end{pmatrix}
\]

$\mu_t$ is the target measure: the energy consumption of the target program $p_t$ on $S_3$. 

The surrogate $x$ not only is a tool to predict the target measure $\mu_t$, also provides information about the target program, with respect of the benchmarks. In this example it will estimate the composition of the target program $p_t$ in terms of \emph{CPU bound} vs \emph{communication bound}.

\subsection{On the normalization of $\mathbf{A}$}

la regressione lineare assume distribuzione bla bla sugli errori, invece la nostra e'  relativa, normalizzando la riportiamo uguale bla bla

\subsection{Limits of LBM}

LBM is an extremely generic model that can be applied to different hardware architectures and different types of programs, without instrumentation or source code access. 

We believe that LBM can handle measures coming from heterogeneous micro-architectures, actually a desirable scenario because the same program running on different architectures will contain different CPU level instructions, but it will still be composed of the same \emph{computational patterns} (therefore the \emph{surrogate} does not change when we change \emph{computational environment}), and the different low level behavior will allow a more precise characterization of the surrogate. However the experimental setting presented in this paper is limited to a single architecture (all the compute nodes are uniform), therefore we can not claim yet that the model can successfully operate with heterogeneous architectures.

LBM treats the same program running on different input sizes as different programs. This assumption simplifies the formulation of the model. We have an extension of the model that handles multiple input sizes for the same program as the same program, but for sake of brevity we do not introduce it in this paper.

The \emph{benchmarks} constitute the knowledge base that the \emph{solver} has when it creates the \emph{surrogate}, if the \emph{benchmarks} do not contain most of the \emph{computational patterns} contained in the \emph{target program}, the \emph{surrogate} will not be able to capture the important factors that model the behavior of the \emph{target program}, e.g. if our \emph{target program} is memory bound, and no \emph{benchmark} makes large use of memory, it's very unlikely that linear regression will be able to create a good fit, and the predictions made with LBM will have a large error.
To be able to successfully express the \emph{target program} as a linear combination of the \emph{benchmarks}, $\mathbf{b}$ needs to be in the column span of $\mathbf{A}$. To avoid numeric errors in the linear regression, or even $\mathbf{A}$ not being full rank, the \emph{benchmarks} should not have a similar behavior, i.e. columns of $\mathbf{A}$ should have low correlation. The \emph{benchmarks} set should therefore be variegate, containing several different algorithms, that exhibit very different behavior.

\section{Predicting OpenFOAM energy consumption and completion time}

OpenFOAM is an open source Computational Fluid Dynamics (CFD) and structural analysis tool, widely used in HPC clusters.

To demonstrate the potential of our approach we limited the resources to completion time and energy (we could have used performance resources) and a limited number of benchmarks as predictors.

We tested our model measuring the completion time and energy consumption of 4 cases of the tutorials included in the OpenFOAM CFD suite, running on an enclosure in the IT Center data center at the University of Pisa, with 4 compute nodes, each node equipped with 12 Intel(R) Xeon(R) X5670 CPUs (2.93GHz), hyper threading enabled. We prepared a simple ammeter using a cheap Phidgets \cite{_phidgets_????} component, one compute node was running Windows HPC server 2008 with the measurement framework we wrote \cite{_energon_????-1} to control the experiment and measure the energy consumed by the enclosure. The remaining 3 compute nodes were installed with CentOS, Kernel 2.6.32, we installed OpenFOAM from the RHEL RPM package available on \cite{_openfoam_????}. We modified 4 of the tutorials as follows:
\begin{enumerate}
\item case \emph{cavity} with the \emph{icoFoam} solver, augmenting the mesh density 900 times, 100 iterations
\item case \emph{pitzDaily} with the \emph{adjointShapeOptimizationFoam} solver, augmenting the mesh density 400 times, 10 iterations
\item case \emph{squareBump} with the \emph{shallowWaterFoam} solver, augmenting the mesh density 6400 times, 90 iterations
\item case \emph{mixerVesselAMI2D} with the \emph{pimpleDyMFoam} solver, augmenting the mesh density 1000 times, 10 iterations
\end{enumerate}

We measured the completion time (elapsed time from the start of the job to its completion on all nodes) and energy consumed (as the product of average instant power, as measured by the ammeter at PDU level, and completion time) by the 4 cases running on 1 (24 cores), 2 (48 cores) and 3 nodes (72 cores).

To test the prediction accuracy of LBM we measured \emph{cavity}, \emph{mixerVesselAMI2D}, \emph{squareBump} on 1, 2 and 3 nodes; then we measured \emph{pitzDaily} on running on 1 and on 3 nodes and tried to predict its energy consumption running on 2 nodes. We did the following test:

\begin{enumerate}
\item matrix $\mathbf{A}$ is made by the measures of the completion time and energy consumption of programs \emph{cavity}, \emph{mixerVesselAMI2D}, and \emph{squareBump} running on 24 and 72 cores
\item vector $\mathbf{c}$ is made by the measures of the energy consumption of programs \emph{cavity}, \emph{mixerVesselAMI2D}, and \emph{squareBump} running on 48 cores
\item vector $\mathbf{b}$ is made by the measures of the completion time and the energy consumption of \emph{pitzDaily} running on 24 and 72 cores
\item our target measure is the energy consumption of \emph{pitzDaily} on 48 cores 
\end{enumerate}

The motivation to try to predict the energetic performance of a case with an intermediate number of cores is that the number of cores that will minimize the energy consumption is not likely to be an extreme value, as can be seen in table \ref{measures_prepare}: \emph{cavity} and \emph{squareBump} have a minimum for the intermediate number of cores, \emph{pitzDaily} has the minimum for the lower number of cores.

\subsection{Prediction results}

The measures taken to prepare the prediction are reported in table \ref{measures_prepare}, we measured the cases used as benchmarks running on 24, 48 and 72 cores, and measured the target program on 24 and 72 cores (and tried to predict the performance for 48 cores). We also measured each case running in serial.

\begin{table*}
\caption{Measures taken to prepare the prediction} 
\label{measures_prepare}
\begin{center}
\begin{tabular}{ l | c || c | c | c | c }
Case & Cores & Energy consumed & Completion time & Average Power  \\ 
\hline

cavity & 1 & 795835.58J & 3011.73s & 264.24W \\
cavity & 24 & 269919.72J & 706.42s & 382.09W \\
cavity & 48 & 251975.76J & 418.78s & 601.68W \\
cavity & 72 & 660199.18J & 773.38s & 853.64W \\
mixerVesselAMI2D & 1 & 160411.67J & 613.95s & 261.27W \\
mixerVesselAMI2D & 24 & 157951.17J & 525.41s & 300.61W \\
mixerVesselAMI2D & 48 & 238653.37J & 530.57s & 449.80W \\
mixerVesselAMI2D & 72 & 520949.26J & 695.95s & 748.54W \\
squareBump & 1 & 368907.72J & 1391.43s & 265.12W \\
squareBump & 24 & 214286.62J & 614.60s & 348.65W \\
squareBump & 48 & 190538.20J & 375.23s & 507.78W \\
squareBump & 72 & 203057.01J & 308.66s & 657.86W \\
\hline
pitzDaily & 1 & 540806.31J & 2047.81s & 264.08W \\
pitzDaily & 24 & 201251.53J & 661.82s & 304.08W \\
%pitzDaily & 48 & 308195.18J & 675.70s & 456.11W \\
pitzDaily & 72 & 530366.40J & 818.13s & 648.25W \\
\end{tabular}
\end{center}
\end{table*}

To prepare the prediction we build $\mathbf{A}$, $\mathbf{b}$, $\mathbf{c}$ and $\mathbf{c}$ using completion time and energy consumption (power is not a valid resource):

\[
\mathbf{A} = 
\begin{pmatrix}
269919.72 & 157951.17  & 214286.62 \\
660199.18 & 520949.26 & 203057.01 \\
706.42 & 525.41 & 614.60 \\
773.38 & 695.95 & 308.66 \\
\end{pmatrix}
\]

\[
\mathbf{c} = 
\begin{pmatrix}
251975.76 & 238653.37 & 190538.20 \\
 \end{pmatrix}
\]

\[
\mathbf{b} = 
\begin{pmatrix}
201251.53 \\
530366.40 \\
661.82  \\
818.13  \\
\end{pmatrix}
\]

LBM finds the following surrogate for \emph{pitzDaily}:
\[
\mathbf{x} = 
\begin{pmatrix}
-0.40  \\ 
1.35  \\
0.45 \\
\end{pmatrix}
\]

The surrogate $\mathbf{x}$ tells us that, given the information we had when the model was built, \emph{pitzDaily} can be expressed as linear combination of \emph{cavity}, \emph{mixerVesselAMI2D} and \emph{squareBump}, where \emph{mixerVesselAMI2D} is dominant with respect to the other programs. %Figure \ref{fig:power} shows that the behavior of \emph{pitzDaily} and \emph{mixerVesselAMI2D} are indeed similar with respect to completion time and overall energy consumption, as the degree of parallelism changes.

The prediction of the energy consumption of \emph{pitzDaily} on 48 cores is calculated multiplying 
$\mathbf{c}$ by $\mathbf{x}$ which gives 307586.91J.
The measured energy consumption of \emph{pitzDaily} on 48 cores is actually 308195.18J, LBM predicted the energy consumption of the target program running on 48 cores with a -0.19\% error.

Similarly we predicted the completion time (setting completion time as our target resource when creating $\mathbf{c}$): LBM predicted 719.73s, the actual value is 675.70s, with a -6.51\% prediction error. 

The prediction provided by LBM is more accurate than the obvious interpolation that can be calculated by the energy consumption on 24 and 72 nodes (365808J); similarly, the prediction regarding completion time provided by LBM is better than the simple interpolation (739,5s), proving that LBM was able to extract useful information from the \emph{benchmarks}.


\begin{figure*}
\begin{center}
\scriptsize{\input{data/measures}}
\caption{Resource consumption of programs, changing degree of parallelism}
\label{fig:measures}
\end{center}
\end{figure*}


\begin{figure*}
\begin{center}
\scriptsize{\input{data/speedup}}
\caption{Time and energy speedups, changing degree of parallelism}
\label{fig:speedup}
\end{center}
\end{figure*}


%\begin{figure*}
%\begin{center}
%\scriptsize{\input{data/power}}
%\caption{Power over time for all cases, all degrees of parallelism}
%\label{fig:power}
%\end{center}
%\end{figure*}


Figure \ref{fig:measures} shows the measures of all the experiments used in this paper. We can see how different programs (with certain input) exhibit different behavior as the degree of parallelism grows, now always the degree of parallelism that optimizes the completion time also optimizes the energy consumption, i.e. \emph{squareBump} consumes the less energy with 48 cores, but the completion time is the lowest with 72 cores. Also a small increase in completion time can lead to a considerable increase in energy consumption, i.e. \emph{cavity}, \emph{pitzDaily} and \emph{mixerVesselAMI2D}. LBM can be useful to estimate the optimal degree of parallelism that will minimize the usage of the resource we want to save.

Figure \ref{fig:speedup} shows the time and energy speedup of the programs, where energy speedup is the ratio between the energy consumption of the serial run and the parallel run. \emph{pitzDaily} has its best energy speedup running on 24 cores, LBM predicted an energy speedup for 48 cores of 1.7582 and the real speedup was 1.7547, with an error as low as 0.19\%.

\section{TODO}

measure simple MPI benchmarks similar to the one Andrea wanted to measure, apply FSCL paper approach?

\section{Conclusions}

This paper presents LBM, a model that can predict resources (the resource consumption of programs) using a black box approach, using a different set of resources (not necessarily of the same kind of the one being predicted) and a set of benchmarks (programs) used to build a surrogate of the target program as a linear combination of benchmarks. This model can be applied to different kind of resources, including energy consumption, completion time, memory, etc. 

To the best of our knowledge there is no other black box predictive model achieves the same precision, allowing the usage of measures of different resources.

We tested the model using 4 cases from the OpenFOAM suite, predicting the completion time and energy consumption of one program, using the other 3 as predictors, with a low error.  
LBM can also be used to characterize the behavior of a program, only using measures of the resources, and it is computationally inexpensive (once the surrogate has been built).

LBM could be used in an HPC scheduler (where the source code of the tasks is seldom available) to minimize the completion time or energy consumption allocating the proper amount of nodes for the task. It would be sufficient to measure the \emph{benchmarks} on different possible sets of compute nodes (that are usually allocated to jobs in predefined sets to avoid cluster fragmentation) during the cluster setup, this measure has to be taken only once, and can be used for all the prediction models. Once the \emph{target program} will have been measured on some configurations, LBM will be able to predict the completion time and energy consumption on the other configurations. The scheduler will then be able to minimize the resource consumption for subsequent run of the same algorithm (running on similar inputs).


\section*{Acknowledgments}\label{sec:Acknowledgments}

Authors would like to thank Distretto Ligure Tecnologie Marittime for the support provided throughout the preparation of this paper.



%%
%% Bibliography
%%

%% Either use bibtex (recommended), but commented out in this sample

\bibliography{Energon_bibliography}

%% .. or use bibitems explicitely



\end{document}
