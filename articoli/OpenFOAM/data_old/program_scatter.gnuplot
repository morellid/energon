set terminal epslatex size 8.0cm,5.00cm
f = program."_".t."_".modelsize."_".basissize."_scatter"
titolo = "Scatter plot of ".program[5:strlen(program)].", $m$=".modelsize.", $n$=".basissize
print titolo
print f
set output f.".tex"
bin(x, s) = s*int(x/s)
set zeroaxis
set title titolo
set key top right
set style fill solid
set xlabel "predicted value"
set ylabel "measured value"
set logscale x
set logscale y

plot [:][:] f.".dat" u 1:2 t "" w points, \
	x with lines