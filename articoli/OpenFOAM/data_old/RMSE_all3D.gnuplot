set terminal epslatex size 9.0cm,6.35cm
set isosamples 100
set samples 100
set view 45,120

unset key

set style data lines
set parametric

f = "RMSE_".t."_All_summary"
titolo = "RMSE changing model size and number of predictors"
print titolo
print f
set output f.".tex"

#set title titolo
set xlabel "\\rotatebox{0}{$m$}"
set ylabel "\\rotatebox{0}{$n$}"
set zlabel "\\rotatebox{0}{RMSE}"

#set zrange [0.0:2.0]
set xrange [30:350]
set yrange [3:27]

# set logscale x
# set logscale y

set hidden3d
#set dgrid3d 32 25

set grid

splot f.".dat" using 1:2:5 title 'RMSE'