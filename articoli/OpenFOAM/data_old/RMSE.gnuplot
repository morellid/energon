set terminal epslatex
set output fileout
bin(x, s) = s*int(x/s)

set zeroaxis

# Uniform
set title "dummy title"
set key top right
set boxwidth 0.05
plot [-1.0:1.0][-0.4:1.4] filein u 1:(0.25*rand(0)-.35) t '', \
     "" u (bin($1,0.05)):(20/300.) s f t 'frequency' w boxes
