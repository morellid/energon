set terminal epslatex size 8.0cm,5.00cm
f = program."_".t."_".modelsize."_".basissize
titolo = "Relative error of ".program[5:strlen(program)].", $m$=".modelsize.", $n$=".basissize
print titolo
print f
set output f.".tex"
bin(x, s) = s*int(x/s)
set zeroaxis
set title titolo
set key top right
set style fill solid
set xlabel "relative error"

stats f.".dat" using 1 
range = STATS_max - STATS_min

binsize = range / 100.0

set boxwidth binsize
plot [:][:] f.".dat" u 1:(1.0*rand(0)-1.0) t '', \
     "" u (bin($1,binsize)):(20/300.) s f t 'frequency' w boxes fs solid 0.25

 #     ""  smooth kdensity t 'frequency'
