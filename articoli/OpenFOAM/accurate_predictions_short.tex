%This is a template for producing OASIcs articles.
%See oasics-manual.pdf for further information.

\documentclass{sig-alternate-10pt}
  %for A4 paper format use option "a4paper", for US-letter use option "letterpaper"
  %for british hyphenation rules use option "UKenglish", for american hyphenation rules use option "USenglish"
\usepackage{authblk}
\usepackage{microtype}%if unwanted, comment out or use option "draft"
\usepackage{graphicx}
\usepackage{tikz}
\graphicspath{ {./data/} }
%\graphicspath{{./graphics/}}%helpful if your graphic files are in another directory

\bibliographystyle{plain}% the recommended bibstyle

% Author macros %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Micro-architecture independent, accurate blind predictions of the SPEC CPU 2006 suite completion time}

\author{Davide Morelli\qquad Antonio Cisternino\\\{morelli|cisterni\}@di.unipi.it\\Computer Science Department, University of Pisa}


\begin{document}

\maketitle

\begin{abstract}



\end{abstract}


\section{Introduction}

% why resource prediction

Programs resource usage prediction is a sensitive task in many cases: 
a better informed process scheduler could allocate the right amount of resources to processes, optimizing the completion time and resource usage pressure; 
an HPC job scheduler capable of predicting the memory usage of the jobs in the queue and their completion time could allocate the right amount of nodes to jobs, with the appropriate resources, avoiding situations like allocating nodes with large memory for a CPU bound task (wasting resources for a task that could have been run on a node with less memory) or allocating nodes with too little memory, resulting in thrashing the nodes, wasting hours if not days of work;
a cloud manager capable of predicting the resource usage pressure could migrate virtual machines to the appropriate hardware just before the resource usage spike, and consolidating them on a single physical machine when the resource usage pressure is low, saving money while preserving the SLA.

Energy is one of the resources used by computers to perform a tasks. As shown in \cite{hoste_performance_2006,phansalkar_measuring_2005,sharkawi_performance_2009} a resource can be used to predict a different resource (i.e. performance counters can be used to predict energy consumption). Our effort is to unify existing approaches dedicated to different resources (in particular, but not limiting to, completion time), creating a more abstract model that can to characterize programs behaviour in a more general sense, predicting different resources from the ones used to build a prediction model. Energy characterization and prediction could then take advantage of the vast literature in performance prediction of software.

% why blind predictions

Resource usage prediction should be performed without knowledge of the program's source code, as in most of the real world scenarios source code is not known. The resource usage prediction model should be able to rely only on data that can be measured running the programs, implementing a \emph{blind} approach. Characterization and prediction should be performed relying only on the informations about running programs that are usually available to the operative system, without the need ot additional hardware or manipulations of the binary.

% unified model

The resource prediction model should also be as abstract as possible, avoiding relying on a specific micro architrecture or resource kind \cite{neugebauer_energy_2001}, as the model would be obsolete before it could become widely used. For a program's descriptive and predictive model to be useful, it should be portable on different hardware from the one where the model was built. 

% competitors

The models that achieve a low prediction error are very narrow, focusing on a particular micro architecture \cite{tiwari_power_1994,brooks_wattch:_2000,steinke_accurate_2001,snavely_modeling_2001,russell_software_1998}, resource (i.e. completion time, energy consumption), even programming language \cite{tiwari_power_1994,arnold_online_2002}, they also usually model execution down to the single instruction level of detail \cite{sinha_jouletrack_2001,steinke_accurate_2001,russell_software_1998}, hiding the interaction between instructions that changes from architecture to architecture, making it difficult to abstract the results to a different architecture.

% the solution

We developed a simple linear model that leverages on resource usage measures to predict the usage of other resources, the model was designed to be as simple as possible, capable of predicting and describing resource consumption of both hardware and software, not focusing on particular architectures or resources. The model is black-box, it does not require the source code of the program being measured.

% model validation using SPEC CPU 2006

We validated our model predicting the completion time of the SPEC CPU 2006 programs using a subset of the same suite as the surrogates.

% \begin{itemize}
% \item \cite{saavedra_analysis_1996}: linear composition of abstract operations, SPEC CPU completion time prediction average error 0.57\%, root mean square 15.07\%, idea of a machine independent model to characterize machines and program, and estimation.
% \item \cite{sharkawi_performance_2009}: 7.2\% (5.4\% std dev)average error, 10.5\% (8.2\% std dev) when changing micro architecture.
% \item \cite{eeckhout_workload_2002} \cite{hoste_performance_2006}: TODO usa perf counters, PCA, somiglianza tra programmi in CPU SPEC
% \item 
% : prevede SPEC FP con errore 7\% std dev 5\% (in SWAPP 11\% err con 2\% std dev),  comb lineare di surrogati, stessa intuizione ma procedura molto piu' articolata, genetic algo
% \item \cite{steinke_accurate_2001} instruction level, processor specific
% \item \cite{tiwari_power_1994} 94, embedded, instruction level modeling
% \item \cite{snavely_modeling_2001} comb lineare di "basic blocks", non astratto rispetto alla risorsa, convoluzione con modello i/o. 23\% errore su multi processors
% \item \cite{brooks_wattch:_2000} wattch, power dissipation at architecture level, 10\% accuracy, cycle level performance simulator
% \item \cite{phansalkar_measuring_2005} programs similarity, SPEC CPU, use perf counters
% \end{itemize}

\section{The model}

Our model is a generalization  of \cite{tiwari_power_1994,hoste_performance_2006,snavely_modeling_2001,sharkawi_performance_2009}. We designed the  model to be resource agnostic, it can be used to characterize both hardware and software \cite{morelli_compositional_2012}, and can predict the completion time as well as the energy consumption, the allocated memory, the number of cache misses, etc.. 

In this work we will apply the model to the SPEC CPU 2006 suite completion time as an example of resource, using the aboundant database of results available in the SPEC website \cite{_spec_2014}.

\subsection{Definitions}

\newtheorem{definition}{Definition}

\begin{definition}
A program is a particular and defined high level sequence of instructions.  
\end{definition}
The same program can be run on different micro architectures, even if will generate different low level sequence of processor instructions, it will still be considered the same program. When called to process different input sizes, because the sequence of high level instructions will considerably change, it will be considered a different program. The target program is the program whose resource consumption we are interested in predicting.

\begin{definition}
A computational environment is any computational system that can execute programs. 
\end{definition}
Examples of computational environments are embedded computers, smartphones, PCs with different micro-architectures, clusters. We consider part of the computational environment the hardware as well as the operative system and all the software running on the machine at the same time as the program being measured.

\begin{definition}
A resource is an asset of the computational environment, used to execute the program. 
\end{definition}
Not every resource can be used in our model, it needs to provide measures that have the following properties: non negativity, null empty set and countable additivity [TODO: cita o spiega]. Examples of valid resources are processor time, completion time, cache misses, Joule. Examples of invalid resorources are \% processor time (it may decrease), active memory (memory could be deallocated), Watt (instant power could decrease). Usually invalid resources can be made valid combining them with time (e.g. $W$ is invalid, but $J=W T$ is valid). The same resource on two different computational environment is considered as a different resource (e.g. completion time on computer $A$ and completion time on computer $B$)

\begin{definition}
A measure or resource consumption is a positive real number that describes the quantity of resource used by a certain program to run on a certain computational environment.   
\end{definition}

\begin{definition}
A surrogate is a program used to predict the resource consumption of the target program. 
\end{definition}

\begin{definition}
A predictor is a linear combination of surrogates used as a model to predict the resource consumption of the target program. 
\end{definition}

\begin{definition}
A computational pattern is an ideal program the exhibits a peculiar resource consumption.
\end{definition}
 E.g. a program made in its entirety by floating point operations, or a program that triggers a cache miss at every instruction. Computational patterns are orthogonal to each other. Computational patterns are usually ideal programs, a program can not consist only of a single computational pattern. At most synthetic benchmarks can approximate particular computational patterns. Some computational pattern could be reasonably be guessed (in some case even designed), but in general they are unknown, and may arise when new micro-architectures are created: a novel micro-architecture could expose a peculiar resource usage when used by a certain sequence of instructions.

The model assumes that all programs can ideally be decomposed in sequences of computational patterns.
The computational patterns form a basis of the resource consumption space (because they are orthogonal with respect to resource consumption). Any program, including both the surrogates and the target program, can be written as a linear combination of the computational patterns. If every computational pattern used by the target program is contained at least in one of the surrogates, and if the surrogates are not linearly dependent, we can operate a change of basis and express the target program as a linear combination of the surrogates. If the target program contains computational patterns that are not contained in any surrogate, then the change of basis will lose information.

\subsection{Finding the predictor}

The model tries to express the target program as a linear combination of surrogates, called predictor. 
When they share a large number of computational patterns related to the resources being measured, the surrogates will be able to predict the consumption of those resources with high precision, when there are computational patterns that are used by the target program but not by any of the surrogates a larger error will arise.

The solver finds the vector $\mathbf{x}$ such that 
\begin{math}
\mathbf{Ax=b}
\end{math}
where $\mathbf{A}$ is the matrix containing the measures of the resource consumption of the surrogates, keeping only the resources for which we have measures for the target program as well, one column for each surrogate, one row for each resource, and $\mathbf{b}$ is the vector containing the known measures of the target program.

Several solvers can be used to find $\mathbf{x}$, in this work we present the results using Ordinary Least Squares, we have also tried using the symplex (adding the constraint that $\mathbf{b}$ must only contain positive elements) with similar prediction performances; and Total Least Squares, that was slightly less accurate.


\subsection{Applying the model}

Once the prediction model $\mathbf{x}$ has been build we can use it to predict the resource consumption of the target program when we know the resource consumption of the surrogates
\begin{math}
\mathbf{A'x=b'}
\end{math}
where $\mathbf{A'}$ is the matrix containing the measures of the resource consumption of the surrogates, keeping only the resources that we want to predict for the target program, and $\mathbf{b'}$ will contain the predicted resource usage.

\subsection{Testing the solvers}

Different solvers can be used to find $\mathbf{x}$ once we know $\mathbf{A}$ and $\mathbf{b}$, so far we have tested the following solvers:
\begin{itemize}
\item Ordinary Linear Regression, simply solving $\mathbf{Ax=b}$. 
\item Total Linear Regression, solving $\mathbf{Ax=b}$. This method is potentialy interesting because we know that $\mathbf{b}$ contains error.
\item Symplex. In this case we assume $\mathbf{x}$ to only contain positive elements, the complete procedure is described in \cite{morelli_compositional_2012}
\end{itemize}

To test the theoretical accuracy of our approach, with different solvers, and in presence of data with error, we did the following simulation:
we created a matrix $A$ with $m=10$ columns and $n=10$ rows, with random values between 0 and 1,
we then created a vector $x$ with 10 rows, with random values between 0 and 1, $x$ is the \emph{true} linear decomposition of an imaginary target program we are testing. We calculated the vector $b$ as $Ax=b$.
Then we added noise to both $A$ and $b$, to simulate real data (thus containing error), obtaining $A'$ and $b'$. We removed random columns of $A'$ and the corresponding elements of $b'$ to simulate the ignorance of some computational pattern, and used each solver to find $x'$. This should give us an idea of what solution the solvers would give us ($x'$) trying to guess the real linear decomposition ($x$) in presence of data with error and with a limited set of predictors, not necessarely covering all the computational patterns ($A'$ and $b'$).

At this point we create a new matrix $\overline{A}$ with the same number of columns as $A'$ and 100 rows (10 times bigger), with random values between 0 and 1, calculated the vector $\overline{b}$ as $\overline{A}x=\overline{b}$ and added noise to both $\overline{A}$ and $\overline{b}$, obtaining $\overline{A'}$ and $\overline{b'}$.
We calculated the residual (error) vector $\epsilon= \overline{A'}x'-\overline{b'}$ and the model error (the difference between the output of the solver and the real linear decomposition)  $\epsilon' = x' - x $ (skipping the elements of $x$ non present in $x'$, repeating each simulation 100 times to ensure statistical validity.

Here we report the average value of the RMSE of the relative prediciton errors $RMSE=\sqrt{\frac{\sum (\frac{\epsilon_i}{b'_i})^2}{m}} $, that will give us an idea of the resource consumption prediciton error we can expect if the bias error is gaussian and our model true (target programs can be expressed as linear composition of predictors). We also report the average value of the total model error as $\sqrt{\frac{\sum (\epsilon'_i)^2}{m}}$ (the difference between the real linear decomposition and the linear decomposition that the solver found).

We tested our solvers with an error ranging from 0.0 to 1.0, incrementing by 0.01. We show here the results and a zoom on the [0.0, 0.1].

In the range of interest they are all approximately equal, with Ordinary Least Square providing the best results, Symplex the worst. As the error grows Total Least Square shows very large error. Ordinary Least Square is therefore the best choice.

  % \input{data/solvers_error_all} 

  \input{data/tex/solvers_error_zoom} 


  % \input{data/solvers_solution_error_all} 

  \input{data/tex/solvers_solution_error_zoom} 



\section{Predicting CPU SPEC 2006 completion time}

We tested our model using the SPEC CPU2006 data publicly available on the SPEC website [TODO: cita], using the completion time on each report as a different computational resource, chosing a subset of the suite as the surrogates and the rest of the suite as the target programs. 

We decided to use the SPEC CPU suite because it has been proven to be a representative workload for real workd applications [TODO: cita], ithas been used to prove the performance of several other performance predictors [TODO: CITA], and all the data needed to replicate the experiment is available on the SPEC website \cite{_spec_2014}, allowing repeatability of the results presented in this work

\subsection{The test setting}

We downloaded the data from the SPEC website, we kept only the results that had both SPEC INT and SPEC FP, creating one row for each machine and one column for each program of the suite. The resulting matrix contianed 730 rows and 29 columns. The software and scripts used to download and aggregate the data, to create the models, to make predictions and to check the prediciton errors is available on \cite{_energon_????}.

The primary objective of our experiment was to test the prediction accuracy of our prediction model using the data from CPU SPEC 2006. In each experiment we proceeded as follows:

\begin{enumerate}
\item we chose the number $m$ of resources used to build the model. This is the number of rows of $\mathbf{A}$.
\item we chose the number $n$ of surrogates. This is the number of columns of $\mathbf{A}$
\item we created a matrix $\mathbf{M}$ with all the measures we have, including all the results of SPEC CPU 2006 and all the programs. 
\item we randomly select $m$ results from the CPU SPEC 2006 database. This represents the knowledge we have when we build the model. We will use the rest of the results of the CPU SPEC 2006 database to test the model. We created a matrix $\mathbf{M}$ with all the results of SPEC CPI 2006, all the programs and all the computational environments. We then created $\mathbf{M_{model}}$ filtering only the selected $m$ rows of $\mathbf{M}$ and $\mathbf{M_{test}}$ with all the other rows.
\item we normalized each row of $\mathbf{M_{model}}$ (to make sure each computational environment had the same weigth in the model)
\item we created a correlation matrix of the column of $\mathbf{M_{model}}$. For each column of the correlation matrix, we calculated the sum of its element, and chose the $n$ lower columns. Each column corresponds to a program of the suite, that we will use as our surrogates. 
\item we created $\mathbf{A}$ keeping only the columns of $\mathbf{M_{model}}$ relative to the surrogates
\item we used every remaining program in the SPEC CPU 2006 suite ($29 - n$) as a target program, each with its $\mathbf{b}$ vector (the column of $\mathbf{M_{model}}$ relative to that program).
\item we found $\mathbf{x}$ of every target program, using Ordinary Least Squares
\item the predicted resource consumption of every target program is $ \mathbf{p} = \mathbf{A'x} $, the actual resource consumption is $ \mathbf{b'}$ (the column of $\mathbf{M_{test}}$ relative to the target program). For each row $i$ of $\mathbf{M_{test}}$ we computed the relative prediction error $ e_i = \frac{\mathbf{p}_i - \mathbf{b'}_i}{\mathbf{b'}_i}$. For each target program we reported the average relateive prediction error and root mean square error (RMSE).
\end{enumerate}

\subsection{Bias error}

Performance measures will have bias error, as \cite{mytkowicz_producing_2009} shown in 2009, because of unexpected phenomena. Our dataset contains several cases in which we have more than one report from a machine (repeated experiments). We extracted the reports that have been run on machines with the same components and analysed the difference in the measures to estimate the bias error.

\begin{tabular}{ l r }
program & RMSE \\
483.xalancbmk & 0.0153023905331217 \\
473.astar & 0.0112602528197657 \\
471.omnetpp & 0.0218376332577717 \\
464.h264ref & 0.0102051259658287 \\
462.libquantum & 0.043549038171124 \\
458.sjeng & 0.00908320198786252 \\
456.hmmer & 0.0176220101625248 \\
445.gobmk & 0.00948446176732873 \\
429.mcf & 0.0157164267654412 \\
403.gcc & 0.0116133665992006 \\
401.bzip2 & 0.0124363933254114 \\
400.perlbench & 0.0143623365779309 \\
\hline
482.sphinx3 & 0.0228012964353596 \\
481.wrf & 0.0260893280747269 \\
470.lbm & 0.0738958421402926 \\
465.tonto & 0.023039738105416 \\
459.GemsFDTD & 0.0634323235625337 \\
454.calculix & 0.0120414647510306 \\
453.povray & 0.0103201661018488 \\
450.soplex & 0.0163557902983576 \\
447.dealII & 0.0140522896501951 \\
444.namd & 0.00992816254325202 \\
437.leslie3d & 0.0596947873459814 \\
436.cactusADM & 0.0858244473761345 \\
435.gromacs & 0.0357911323617833 \\
434.zeusmp & 0.0379265127384284 \\
433.milc & 0.0214655231378676 \\
416.gamess & 0.0133756232652227 \\
410.bwaves & 0.0764408529704244 \\
\end{tabular}

The program with the lowest bias error is 458.sjeng (0.00908320198786252), the program with the highest error is 436.cactusADM (0.0858244473761345), the total bias error (RMSE) of the CPU SPEC is equal to 0.03531.

\subsection{Expected prediction results}

We have seen that the bias error in our dataset is between 0.01 and 0.09, with an average of 0.03. In that range of error in data, Ordinary Least Square solver has an expected prediction error that ranges between 0.00 (0.01 standard deviation) and 0.06 (0.05 standard deviation), we expect to obtain predictions with an error at least that large. The closer we get to that lower bound, the more likely our model is to be valid. 

\subsection{Prediction results}

\begin{figure}
\scriptsize{\input{data/tex/RMSE_vector_space_All_summary}}
\caption{RMSE changing number of surrogates ($n$), for several model sizes ($m$), 100 iterations each}
\label{fig:RMSE_vector_space_All_summary}
\end{figure}


TODO fix picture
\begin{figure}
\input{data/tex/RMSE_vector_space_40_summary}
\caption{Average relative error and RMSE, for model size=30. 100 iterations}
\label{fig:RMSE_vector_space_30_summary}
\end{figure}

\ref{fig:RMSE_vector_space_All_summary} shows the RMSE changing the number of surrogates ($n$) and the number of resources used to build the model ($m$). Every point of this graph is the average RMSE of $(29-n)(730-m)$ predictions (a particular case of $n$ and $m$, i.e. for $n=9$ and $m=30$ we have 20 target programs and 700 resources for each target program to predict), each case iterated 100 times to ensure statistical validity. The minimum number of surrogates is 3, maximum is 28. Model size increases by 10, starting from 30 and ending with 350. The total number of predictions that were made to produce this graph is $\sum_{i=3}^{28} \sum_{j=1}^{350/10} 100(29-i)(730-10j) = 476307000$.

Models built with less than 8 surrogates always have a large error (RMSE $> 0.2$), even if we use a large model size. In particular using 1 surrogate RMSE is always between 0.9 and 0.8 (0.89 with $m=10$, 0.79 for $m=350$, with decreasing values as we increase $m$), with 2 surrogates RMSE is between 0.78 and 0.65, and so on up to 5 surrogates with RMSE between 0.50 and 0.23. This is expected, because the reduced number of surrogates makes it unlikely to build models with enough information to capture the computational patterns that may be present in the target program. 

Models built with a very small number of resources (less than 30) usually are unstable and have a large error (RMSE $> 0.2$). Also this is expected, because CPU SPEC 2006 has results on many different micro architectures, and with such a small number of micro architectures used to build a model is unlikely to have a representative sample in $\mathbf{A}$, needed to have a chance to see the peculiar resource consumption of computational patterns.

Unexpectedly a relatively small number (compared to the size of the test set) of resources are needed to be able to predict the consumption of a very large number of resources with a relatively small error: with as low as 30 resources and 10 surrogates the RMSE is only 0.16 on 700 resources and 18 target programs.

As we keep adding resources to $\mathbf{A}$ (and surrogates) the models become more and more accurate: with 100 resources and 10 surrogates the average relative error is 0.00 and the RMSE is 0.12, withh 100 resources and 20 surrogates the average relative error is 0.00 and the RMSE is 0.10, with 200 resources and 10 surrogates RMSE is 0.11, with 200 resources and 20 surrogates the RMSE is 0.09. As expected the largest model size and number of surrogates achieves the lowest RMSE: for $m=350$ and $n=28$ RMSE is 0.069 (predicting 1 program on 380 machines). 

The lowest RMSE is in line with our expectations (with an average error of 0.03, the bias error we measured, ordinary least square has a prediction error of 0.06). This results indicates that with optimal conditions (large number of resources and predictors to build a model) this method is capable of explaining the underlying structure of programs, and predict accurately their resource consumption. 

This result also validates the idea that programs can be thought as made of computational patterns.


TODO: compare to other papers, keeping in mind that we predict on unkown machines


\subsection{Details of the case model size 30 with 9 surrogates} 

In this section we will analyse the case of model size $m=30$, with $n=9$ surrogates, because the number of resources is small enough to be practical in real world use (instead of the completion time on 30 machines we could use performance counters on a smaller number of machines, taking care of using different micro-acrhitectures), but the predictions are precise enough to be useful in several tasks (such as scheduling): the average relative error is 0.01 and the RMSE is 0.17.

This case is interesting because is a compromise between accuracy, number of surrogates and number of resources used to build the model.

TODO
% The surrogates were: \input{data/tex/basis_vector_space_40_10}.

TODO: graph of RMSE of single target programs in this particular case, with 100 iterations

For most of the target programs (all except milc) a prediction error of 0 is the most frequent outcome, and the most frequent error is near 0 (very high density between -0.1 and 0.1), exponentially less frequent as we move away from 0. Milc has the worse performance, often over estimating the resource consumption (around 0.25). Gromacs also has an unusual density far from 0 (at -0.2). In such cases there must be some computational pattern that was not discovered by the model, probaby not present in the surrogates, or in the resources used to build the model.

\foreach \f in {482_sphinx3,
465_tonto,
459_GemsFDTD,
454_calculix,
453_povray,
450_soplex,
447_dealII,
444_namd,
435_gromacs,
433_milc,
416_gamess,
483_xalancbmk,
473_astar,
471_omnetpp,
464_h264ref,
458_sjeng,
429_mcf,
403_gcc,
401_bzip2,
400_perlbench} {

%  \input{data/tex/\f_vector_space_40_10} 
TODO

%  \input{data/tex/\f_vector_space_40_10_scatter} 

}

\subsection{Further observations}

TODO: descrivi lo spazio dei predittori, matrice di correlazione, commenta capacita' descrittiva dell' hardware conoscendo il comportamento dei predittori (1 pagina)

TODO: descrivi i modelli creati, rapporta la composizione con i paper in letteratura per mostrare che i modelli descrivono la natura dei programmi (1 pagina)

\cite{phansalkar_measuring_2005}


\section{Conclusion and further work}

The model presented in this work is able to predict the resource consumption of programs using a black box approach, using a different set of resources (not necessarely of the same kind of the one being predicted) and a set of surrogates (programs) used to build a linear decomposition of the target program as a linear combination of surrogates. This model can be applied to different kind of resources, including energy consumption, completion time, memory, etc. We tested the model using the data from the SPEC CPU 2006 suite, using a subset of the suite as surrogates and predicting the completion time of the remaining programs, with an increasing accuracy as we use more data to build the model. Using only 9 surrogates and 30 (our of the 730) computer systems present in the SPEC CPU 2006 database we achieved 0.01 average relative error and 0.17 RMSE. The model has been extensively tested with this data (6930000 predictions), making it a reliable measure of accuracy. 
This model can also be used to characterize the behaviour of a program, only using measures of its resource usage. 

This model could be used in an HPC scheduler (where the source code of the tasks is seldom available) to better allocate the nodes (the right number of nodes, with the right amount of memory); or to predict the resources needed by a task in a cloud, to consolidate the virtual machines while keeping the required SLA; or in an operative system scheduler, because once the model has been built, the prediction is computationally not expensive.

We will apply the same prediction model to other benchmarks, such as SPEC VIRT and to SPEC POWER. We will also apply different solvers to find the decomposition of the target program into a linear combination of the surrogates, such as shrinkage, or probabilistic approaches.

%%
%% Bibliography
%%

%% Either use bibtex (recommended), but commented out in this sample

\bibliography{Energon_bibliography}

%% .. or use bibitems explicitely



\end{document}
