	%This is a template for producing OASIcs articles.
%See oasics-manual.pdf for further information.

\documentclass[a4paper,UKenglish]{oasics}
  %for A4 paper format use option "a4paper", for US-letter use option "letterpaper"
  %for british hyphenation rules use option "UKenglish", for american hyphenation rules use option "USenglish"

\usepackage{verbatim} % for multiline comments
\usepackage{amsmath} % for math

\usepackage{microtype}%if unwanted, comment out or use option "draft"

%\graphicspath{{./graphics/}}%helpful if your graphic files are in another directory

\bibliographystyle{plain}% the recommended bibstyle

% Author macros %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{A compositional model to characterize software and hardware from their resource usage}
\titlerunning{Characterizing HW and SW from their resource usage} %optional, in case that the title is too long; the running title should fit into the top page column

\author[1]{Davide Morelli}
\author[1]{Antonio Cisternino}
\affil[1]{Computer Science Department, University of Pisa\\
  Largo B. Pontecorvo 3, Italy\\
  \texttt{(morelli|cisterni)@di.unipi.it}}
\authorrunning{D. Morelli and A. Cisternino}%optional. First: Use abbreviated first/middle names. Second (only in severe cases): Use first author plus 'et. al.'

\Copyright[nc-nd]%choose "nd" or "nc-nd"
          {Davide Morelli}

\subjclass{D.2.8 Metrics, D.4.8 Performance, B.8.2 Performance Analysis and Design Aids} % mandatory: Please choose ACM 1998 classifications from http://www.acm.org/about/class/ccs98-html . E.g., cite as "F.1.1 Models of Computation". 
\keywords{Performance, Metrics, Energy consumption}% mandatory: Please provide 1-5 keywords
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Editor-only macros (do not touch as author)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\serieslogo{}%please provide filename (without suffix)
\volumeinfo%(easychair interface)
  {Billy Editor, Bill Editors}% editors
  {2}% number of editors: 1, 2, ....
  {Conference/workshop/symposium title on which this volume is based on}% event
  {1}% volume
  {1}% issue
  {1}% starting page number
\EventShortName{}
\DOI{10.4230/OASIcs.xxx.yyy.p}% to be completed by the volume editor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\unit}[1]{\ensuremath{\, \mathrm{#1}}}

\newcommand{\program}[1]{#1}
\newcommand{\system}[1]{#1}
\newcommand{\attribute}[1]{#1}
\newcommand{\programset}{\mathcal{P}}
\newcommand{\programsetIdeal}{\mathcal{P}^{\text{id}}}
\newcommand{\completionTime}[2]{t_{\system{#1}, \program{#2}}}
\newcommand{\energyConsumption}[2]{J_{\system{#1}, \program{#2}}}
\newcommand{\measurementVectorLong}[2]{\vec{\mathbf{\mu}}_{\attribute{#2}(\system{#1})}}
\newcommand{\measurementVector}[1]{\vec{\mathbf{\mu}}_{\system{#1}}}
\newcommand{\measurementVectorShort}{\vec{\mathbf{\mu}}}
\newcommand{\measurementVerticalVector}[2]{\uparrow\mathbf{\mu}_{\system{#1}}(#2)}
\newcommand{\measurementVerticalVectorAll}[1]{\underline{\uparrow\mathbf{\mu}_{(#1)}}}
\newcommand{\measures}[1]{\mathbf{\mu}_{\program{#1}}}
\newcommand{\measurementScalar}[2]{\mu_{\system{#1}}(\program{#2})}
\newcommand{\measurementScalarLong}[3]{\mu_{\attribute{#2}(\system{#1})}(\program{#3})}
\newcommand{\measurementMatrix}[1]{\mathbf{M}_{\system{#1}}}
\newcommand{\measurementMatrixShort}{\mathbf{M}}
\newcommand{\measurementMatrixAll}{\underline{\mathbf{M}}}
\newcommand{\splitupLong}[3]{\vec{\mathbf{s}}_{\substack{\system{#1}\\#3(\program{#2})}}}
\newcommand{\splitup}[2]{\vec{\mathbf{s}}_{#2(\program{#1} ) }}
\newcommand{\splitupShort}[1]{\mathbf{s}_{\program{#1}}}
\newcommand{\splitupComponent}[3]{s_{#2(\program{#1}, #3 ) }}
\newcommand{\instPower}[2]{P_{\system{#1}, \program{#2}}}
\newcommand{\instConstPower}[1]{P_{\system{#1}}}
\newcommand{\betaMatrix}[3]{B_{\frac{\system{#1}}{\system{#2}}, #3}}
\newcommand{\betaScalar}[4]{\beta_{\frac{\system{#1}}{\system{#2}}, #3(\program{#4})}}
\newcommand{\similarityProgram}[2]{\mathrm{sim}_{\program{#1}, \program{#2}}}
\newcommand{\similaritySystem}[3]{\mathrm{sim}_{#3(\system{#1}, \system{#2})}}
\newcommand{\measurementSpace}{\mathcal{M}}
\newcommand{\measurementSpaceLong}[1]{\mathcal{M}_{#1}}
\newcommand{\measurementSpaceAll}{\underline{\mathcal{M}}}
\newcommand{\splitupSpace}{\mathcal{S}}
\newcommand{\splitupSpaceLong}[1]{\mathcal{S}_{#1}}
\newcommand{\complexity}[2]{\mathbf{c}_{\attribute{#2}(\program{#1})}}
\newcommand{\complexityShort}[1]{\mathbf{c}_{\program{#1}}}
\newcommand{\complexityLong}[3]{\mathbf{c}_{\attribute{#2}(\program{#1})}(#3)}
\newcommand{\fittingFunction}[2]{\text{fit}_{#1_{#2}}}
\newcommand{\fittingFunctionLong}[3]{\text{fit}_{#3_{#2}({#1})}}
\newcommand{\uniformSet}{\mathbb{U}}
\newcommand{\uniformSetLong}[1]{\mathbb{U}_{#1}}
\newcommand{\notUniformSet}{\overline{\mathbb{U}}}
\newcommand{\notUniformSetLong}[1]{\overline{\mathbb{U}}_{#1}}

\begin{document}

\maketitle

\begin{abstract}
% pain
Since the introduction of laptops and mobile devices, there has been a strong research focus towards the energy efficiency of hardware. Many papers, both from academia and industrial research labs, focus on methods and ideas to lower power consumption in order to lengthen the battery life of portable device components. Much less effort has been spent on defining the responsibility of software in the overall computational system’s energy consumption.

Some attempts have been made to describe the energy behaviour of software, but none of them abstract from the physical machine where the measurements were taken. In our opinion this is a strong drawback because results can not be generalized. We propose a measuring method and a set of algebraic tools that can be applied to resource usage measurements. These tools are expressive and show insights on how the hardware consumes energy (or other resources), but are equally able to describe how efficiently the software exploits hardware characteristics. The method is based on the idea of decomposing arbitrary programs into linear combinations of benchmarks of a test-bed without the need to analyse a program’s source code by employing a black box approach, measuring only its resource usage.

\end{abstract}

\section{Introduction}
% why energy consumption -> models in literature -> general model for resource usage

% why energy consumption 
Energy consumption is a global concern; as the Environmental Protection Agency of the U.S. stated in a report \cite{EPA2007} dated 2007, the energy consumed by data centres and servers alone can account for 1.5\% of the global energy use, and is doubling every five years. 

The adoption rate of portable devices raised the attention towards energy efficiency of hardware components (in order to lengthen battery life) and network protocols. Much less effort has been spent on defining the responsibility of software in the overall computational system's energy consumption, none of them abstract from the physical machine where the measures are taken. In our opinion this is a strong drawback because results can not be generalized. 

We propose a measuring method and a set of algebraic tools that can be applied to resource usage measurements (energy consumption and completion time being just two instances of resource usage). These tools are expressive and show insights on how the hardware consumes energy (or other resources). They are also able to describe how efficiently the software exploits the hardware characteristics.

%Computational systems are complex systems
Typically, measurement techniques proposed in the literature aim to break down the energy consumption to atomic components, associating an average cost to every instruction \cite{Tiwari1994, Steinke01anaccurate, Flinn1999, Brooks2000, Vijaykrishnan00energydrivenintegrated}. We believe that a different approach should be used, as modern computational systems tend towards complex systems. Hardware started developing parallelism when the CPU frequency reached its cap, and software systems are becoming increasingly complex. The result of this process is that the resource usage of a single instruction is hardly predictable and depends on the execution context. For example, the time required to load a location depends on both the program's memory access pattern, the hardware characteristics, and the memory used by the other processes running on the same computational system, leading to almost \textit{non deterministic} results. For this reasons we have chosen to follow a black box approach, measuring the resource usage of the software running on a computational system as a whole, following the approach proposed in \cite{RivoirePhD2008}, instead of trying to profile the software \cite{Flinn1999}, simulating the execution of algorithms on modified virtual machines or power level performance simulators \cite{Brooks2000,Vijaykrishnan00energydrivenintegrated}. This is because these approaches either require source code access or are only feasible for simple architectures, where a cycle accurate simulator is possible and for relatively small programs. A black box approach measures the software as a whole without trying to break down the energy consumption of single instructions. This approach is the most simple to implement, does not need modifications to the operative system and works with a simple ammeter with the simplest possible approach: the current is measured in AC from the wall outlet \cite{Sinha01jouletrack,Vijaykrishnan00energydrivenintegrated,IGCC2010}.

Metrics for software similarity are very interesting because they allow us to predict the behaviour of programs using measurements of similar programs, and allow their characterization. Yamamoto proposes a metric of similarity based on source code analysis in \cite{Yamamoto2005}. For the scope of our research, we are interested in methods that do not require access to the source code because we want to be able to characterize software as a black box.

% decomposition of components
In a 1992 paper \cite{AnalysisBenchmarkCharacteristicsBenchmarkPerformancePrediction} we found an approach that was particularly inspiring for our work: a model was proposed to characterize both hardware and software. The overall completion time of a program was modelled as a linear decomposition of abstract operations. Our model is very similar to their with respect to the linear composition model. Nevertheless, there are many important differences:
\begin{itemize}
\item they use abstract operations as computational patterns (e.g. \textit{add}, \textit{store}, \textit{divide}, etc.); we allow more articulate entities, long combinations of instructions
\item the program analysis is performed by means of static analysis and instrumentation of the source code; we don't require the source code to analyse the program
\item their model focuses on completion time solely; our model is capable of describing the usage of every measurable resource, we are most interested in completion time and energy consumption, but could be applied to any other metric (i.e. memory usage, CPU time, etc.)
\end{itemize}
\cite{Vijaykrishnan00energydrivenintegrated} also proposed an energy characterization model for both hardware and software. 

% performance counters
In \cite{FindingRepresentativeWorkloadsComputerSystemDesign}, the use of performance counters leverages the characterization of software, a technique that is becoming a de facto standard  \cite{OnlinePowerPerformanceAdaptationMultithreadedProgramsUsingHardwareEventBasedPrediction, Phansalkar, WorkloadDesignSelectingRepresentativeProgramInputPairs, Duesterwald2003}. Benchmarks are analyzed, PCA is used to reduce the solution space and clustering techniques are used to identify families of programs and to find a representative workload for a certain task.

% environment
Two other key concepts are the idea that the environment where the program is run must be taken into account \cite{Chang2003} and the need to find a model capable of offering results resilient with change of hardware: Sherwood \cite{Sherwood2002} characterized software with a model consistent with the change of architecture; he proposed a high level approach (not at instruction level), but he did not focus on energy consumption. 


\section{A compositional model}

% computational patterns: sequence of instructions that expose a peculiar resource usage: cache miss, disk access, network, FPU operations

Programs are composed of instructions that once executed affect the resource consumption of the system. There are many different kinds of computational resources a program can consume (CPU time, memory, network, etc.). We define \textit{computational pattern} a sequence of instructions that expose a peculiar resource usage, that is subject to change as we change the computational system where the pattern is executed on, e.g. on a processor family FPU operation may consume more energy to complete with respect to a different class of processors.

% assumption: programs are made composing computational patterns
In our model we assume that actual programs can be seen as composed of computational patterns, i.e. a matrix multiplication algorithm will read data from memory (showing a peculiar memory read pattern with cache hit and miss), perform FPU then write the result back to memory.

A computational pattern will have a different resource usage on each computational system, e.g. the same memory pattern of data read could rise to a much lower number of cache miss if a processor is capable of predicting the pattern and prefetching data. The composition of a program from the point of view of the computational patterns does not change when the program is run on a different computational system, but the resource usage behaviour will change because the computational pattern the program is composed of will have a different resource usage profile.

Therefore, we chose a set of synthetic benchmarks as our \textit{test-bed}, where every benchmark is intended to capture a particular computational pattern that we expect to find in different quantities in every program we intend to analyse, with zero being a legitimate quantity.

% we can't write pure computational patterns, we will use small synthetic benchmarks that expose a certain behaviour
% tentativo: We can't write a benchmark that perfectly captures a computational pattern, because every program to run needs some basic resources (some CPU, some memory, etc.), e.g. we can't write a program that \textit{only} does cache miss, it needs to have at least some cache hit and CPU usage. 

% we see computational systems as complex enviroments, we can't isolate the resource usage of the program we are measuring from the operative system. a computational system is hardware+software (o.s. and other software running on the machine), the same machine with a different software stack is a different environment and may have different resource usage


\section{Linear algebra model}

% the measurement matrix
We define the \textit{measurement matrix} a computational system \system{S} as a $ \mathbb{R}^{m \times n} $ matrix where $ n $ is the number of benchmarks in our test-bed and $ m $ is the number of attributes (resource usage) we measure for each program. Each one of these matrices holds the knowledge we have about a particular computational system. The $i^{th}$ column of $\measurementMatrixShort$ shows the resource usage of the $i^{th}$ benchmark of our test-bed. The $j^{th}$ cell of that column holds the measurement of the resource usage of the $j^{th}$ attribute (e.g. CPU time, cache hit, etc.).

When we want to decompose a program $\program{p}$ using the benchmarks of our test-bed as the building block we measure the resources usage of $\program{p}$ running on $ \system{S} $ and we build a vector $ \measures{p} $ with those measures. The $j^{th}$ element of $ \measures{p} $ holds the measurement of the resource usage of the $j^{th}$ attribute; $ \measures{p} $ is like a column of $\measurementMatrixShort$ but is composed of measurements of a program that is not part of the test-bed.
Now consider the following linear system:

\begin{equation}\label{splitupdef}
\measurementMatrixShort \cdot \splitupShort{p} = \measures{p}
\end{equation}

% the splitup vector

We call $ \splitupShort{p} $ the \textit{split-up} of $\program{p}$, it holds a decomposition of $ \program{p} $ using the benchmarks of our test-bed as building blocks.

% TODO: example with real data?

% algebraic properties:
% vectors in the matrix space
Standard vector algebra can be used to analyse and interpret measures, splitups and programs. We can analyse vectors using vector norm:
\begin{equation}\label{Norm_x}
\Vert v \Vert =\sqrt{\sum_{i=1}^{n} (v_i)^2}
\end{equation}
and vector similarity:
\begin{equation}
cos(\theta)=\frac{v_1 \cdot v_2}{ \Vert v_1\Vert  \Vert v_2 \Vert }
\end{equation}

\subsection{Measurements space}
% columns of M
The columns of M can be seen as vectors in an $m$ dimensional vector space that we call the \textit{measurements space}. The position of the $i^{th}$ vector shows the resource usage of the $i^{th}$ benchmark of the test-bed. More generally speaking, the $ \measures{p} $ vector shows the resource usage of the program $\program{p}$ in a particular system $ \system{S} $.

The norm can be used to get an insight on the overall resource usage of a benchmark, i.e. more resource demanding benchmarks will have a higher norm than less resource demanding ones.

Vector similarity will tell us how similar two programs are: if the angle between the vectors is small it means that they may use more or less resources in absolute terms (have different norms), but their resource usage behaviour is similar.

\subsection{Splitup space}
% splitup
Splitups can be seen as vectors in a $n$ dimensional vector space that we call the \textit{splitup space}. The position of a vector in this space shows the composition of the program using the benchmarks as building blocks.

When comparing the splitup vectors of two programs we can say that the one with a higher norm is the more resources demanding. If the vector similarity is very close to 1 but the norms are different we are probably looking at the same program running with different input sizes, e.g. if $p_1$ and $p_2$ are the same sorting algorithm with $p_1$ running on half the array size of $p_2$ we'll probably see $\Vert \splitupShort{p_2} \Vert = 2 \Vert \splitupShort{p_1} \Vert$ and $ \frac{\splitupShort{p_1} \cdot \splitupShort{p_2}}{ \Vert \splitupShort{p_1}  \Vert \Vert \splitupShort{p_2} \Vert } = 1 $

% uniform programs
When a program is run on different input sizes the balance of the computational patterns used may vary, e.g. the cache hit ratio could grow logarithmically while the FPU usage may grow linearly. Analysing how the splitup of a program changes with the input size is highly informative of the program structure. When the splitup does not change with the input size we call the program \textit{uniform}; and if it changes, we call it \textit{non uniform}.

%same program same splitup
If the test-bed is well formed the splitup of a program has to be the same when we run it on different computational systems. If it is different it means that this program is capturing a resource usage behaviour not captured by any benchmark in the test-bed, in other words this program contains an unknown computational pattern, therefore it should be added to our test-bed.

\subsection{Benckmark space}
% rows of M (nth row of M1 vs nth row of M2)
$ \measurementMatrix{S} $ is the $ \measurementMatrixShort $ of a system $ \system{S} $. Its rows can be seen as vectors in a $n^{th} $ dimensional space that we call the \textit{benchmark space}. 
We can see how resource usage changes when we change computational system from $ \system{S_a} $ to $ \system{S_b} $  analysing the position of the $i^{th} $ row of $ \measurementMatrix{S_a} $ (where $i$ is the index of the resource we are interested in) against the position of the $i^{th} $ row of $ \measurementMatrix{S_b} $ in the benchmark space.

E.g. when the norm of the energy consumption vector of $\system{S_1}$ is higher than the norm of the energy consumption vector of $\system{S_2}$ it means that 
$\system{S_1}$ is (generally speaking) less energy efficient than $\system{S_2}$. If the angle between their completion time vectors is small it means that $\system{S_1}$ and $\system{S_2}$ have a similar architecture and probably one is just more efficient than the other (i.e. a newer machine). If the angle is large it means that the systems have a different architecture that makes some of the benchmarks in the test-bed more efficient than others; the direction of the difference between the vectors tells us how $\system{S_1}$ is different from $\system{S_2}$.

\section{Real data}

% errors in measuring
Measuring resource attribute will usually involve error, i.e. the accuracy of the measuring tool, sampling frequency, etc. 
Equation \ref{splitupdef} could be not solvable and has to be rewritten in order to minimise a norm of the error vector:
\begin{equation}
\label{error}
\epsilon = \vert \measurementMatrixShort \cdot \splitupShort{p} - \measures{p} \vert
\end{equation}

If we use a Manhattan norm instead of an Euclidean norm, this is a linear programming problem that can be solved using the simplex algorithm. 

% convex cone
We want all the elements of $\splitupShort{p} $ to be non negative numbers, because each of them expresses an estimation of the number of iterations of the respective benchmark, as present in $\program{p}$. The benchmark space is therefore not a vector space but a convex cone. This limitation does not change the approach needed to find the splitup, since we just need to add a few conditions to the simplex.

% the test-bed
Being the benchmark space a convex cone, the number of vectors (the benchmark in the test-bed) that form a basis is not generally known, but the process of selection of the benchmarks in the test-bed can be incremental and automatic: if the splitup of a program falls within the convex cone it means that it can be expressed as linear decomposition of known computational patterns, if it falls outside the cone it means that it should be added to the test-bed, widening the range of programs that can be expressed algebraically.

% level of detail
We can choose the level of detail we want to get with the decomposition of programs. I.e., we might want to have a computational pattern for every major memory read pattern or just a general one. In the former case we would be able to discriminate how the program uses memory, but we would need a lot of experimental data to solve the system. In the latter, we would need few experimental data but might only see a raw estimate of the program's behaviour. The number of rows of the measurement matrix needs to be larger than the number of columns, which means that we need to measure at least as many resources as the number of the benchmarks in the test-bed. This could be difficult if we want to have a large test-bed, in which case we could create a new measurement matrix with more rows just merging measurement matrices of multiple systems.

\section{Experimental data}

As an example we present data of a preliminary test:  we measured the completion time and the energy consumption of a small set of programs running on a desktop computer equipped with a CoreDuo processor with 2MB L2 cache and 1 GB RAM (from now on referred to as \textit{S}). We prepared two synthetic benchmarks: \textit{cpu} is a simple \textit{add} assembler instruction executed $10^6$ times; \textit{mem} is a program that sums a fixed number of random locations from a large array. We used \textit{cpu} and \textit{mem} as our test-bed and measured mergesort (from now on referred to as \textit{p}) sorting arrays of different sizes (1M, 2M, 4M, 8M, 16M, 32M). 

\begin{tabular}{l*{8}{c}}
 & cpu     & mem      & p(1M)  & p(2M)   & p(4M)  & p(8M)   & p(16M)   & p(32M)\\
\hline
time      & 2.14 s  & 7.26 s    & 0.22 s & 0.33 s  & 0.67 s & 1.39 s   & 2.85 s    & 5.79 s\\
energy  & 81.46 J & 304.00 J & 8.60 J & 13.20 J & 27.49  & 58.29 J & 121.79 J & 254.82 J\\
\end{tabular}

$ \measurementMatrix{S} $ is composed of the first two columns of the above table and the measurement vectors for mergesort at various input sizes are:
\[ 
\begin{array}{ccc}
\measures{p(1M)}= \left( \begin{array}{c}
0.22 \unit{s} \\
8.60 \unit{J} \end{array} \right) & 
\measures{p(2M)}= \left( \begin{array}{c}
0.33 \unit{s} \\
13.20 \unit{J} \end{array} \right) & 
\measures{p(4M)}= \left( \begin{array}{c}
0.67 \unit{s} \\
27.49 \unit{J} \end{array} \right) \\
\measures{p(8M)}= \left( \begin{array}{c}
1.39 \unit{s} \\
58.29 \unit{J} \end{array} \right) &
\measures{p(16M)}= \left( \begin{array}{c}
2.85 \unit{s} \\
121.79 \unit{J} \end{array} \right) &
\measures{p(32M)}= \left( \begin{array}{c}
5.79 \unit{s} \\
254.82 \unit{J} \end{array} \right) 
\end{array}
\]

The resulting splitup vectors (calculated minimizing formula \ref{error} using the simplex algorithm) are:
\[  \begin{array}{ccc}
\splitupShort{p(1M)}= \left( \begin{array}{c}
0.075118  \\
0.008161 \end{array} \right) &
\splitupShort{p(2M)}= \left( \begin{array}{c}
0.075862  \\
0.023093 \end{array} \right) &
\splitupShort{p(4M)}= \left( \begin{array}{c}
0.069347  \\
0.071845 \end{array} \right) \\
\splitupShort{p(8M)}= \left( \begin{array}{c}
0  \\
0.248125 \end{array} \right) &
\splitupShort{p(16M)}= \left( \begin{array}{c}
0 \\
0.528191 \end{array} \right) &
\splitupShort{p(32M)}= \left( \begin{array}{c}
0 \\
0.780954 \end{array} \right)
\end{array} \] 


The splitup vectors show how quickly mergesort gets dominated by memory usage as the input size grows. This is expected because as the array grows it will not fit into cache and a lot of cache miss will occur, therefore most of the time and energy will be spent accessing memory.

\begin{comment}
# python (openopt + glpk) code to find splitups

# 1M
from FuncDesigner import *
from openopt import LP
e1, e2, abs1, abs2 = oovars(4)
f1 = 2.14*e1 + 7.26*e2 - 0.22
f2 = 81.46*e1 + 304.00*e2 - 8.60
obj = abs1+ abs2
constraints = [e1>0, e2>0, abs1>f1, abs1>-f1, 
abs2>f2, abs2>-f2]
startPoint = {e2:0, e1:0, abs1:0, abs2:0}
p = LP(obj, startPoint, constraints = constraints)
r = p.solve('glpk')
print('Solution: e1 = %f  e2 = %f abs1 = %f abs2 = %f ' % (e1(r), e2(r), abs1(r), abs2(r)))
# Solution: e1 = 0.075118  e2 = 0.008161 abs1 = 0.000000 abs2 = -0.000000

# 2M
from FuncDesigner import *
from openopt import LP
e1, e2, abs1, abs2 = oovars(4)
f1 = 2.14*e1 + 7.26*e2 - 0.33
f2 = 81.46*e1 + 304.00*e2 - 13.20
obj = abs1+ abs2
constraints = [e1>0, e2>0, abs1>f1, abs1>-f1, 
abs2>f2, abs2>-f2]
startPoint = {e2:0, e1:0, abs1:0, abs2:0}
p = LP(obj, startPoint, constraints = constraints)
r = p.solve('glpk')
print('Solution: e1 = %f  e2 = %f abs1 = %f abs2 = %f ' % (e1(r), e2(r), abs1(r), abs2(r)))
# Solution: e1 = 0.075862  e2 = 0.023093 abs1 = 0.000000 abs2 = 0.000000

# 4M
from FuncDesigner import *
from openopt import LP
e1, e2, abs1, abs2 = oovars(4)
f1 = 2.14*e1 + 7.26*e2 - 0.67
f2 = 81.46*e1 + 304.00*e2 - 27.49
obj = abs1+ abs2
constraints = [e1>0, e2>0, abs1>f1, abs1>-f1, 
abs2>f2, abs2>-f2]
startPoint = {e2:0, e1:0, abs1:0, abs2:0}
p = LP(obj, startPoint, constraints = constraints)
r = p.solve('glpk')
print('Solution: e1 = %f  e2 = %f abs1 = %f abs2 = %f ' % (e1(r), e2(r), abs1(r), abs2(r)))
# Solution: e1 = 0.069347  e2 = 0.071845 abs1 = 0.000000 abs2 = 0.000000

# 8M
from FuncDesigner import *
from openopt import LP
e1, e2, abs1, abs2 = oovars(4)
f1 = 2.14*e1 + 7.26*e2 - 1.39
f2 = 81.46*e1 + 304.00*e2 - 58.29
obj = abs1+ abs2
constraints = [e1>0, e2>0, abs1>f1, abs1>-f1, 
abs2>f2, abs2>-f2]
startPoint = {e2:0, e1:0, abs1:0, abs2:0}
p = LP(obj, startPoint, constraints = constraints)
r = p.solve('glpk')
print('Solution: e1 = %f  e2 = %f abs1 = %f abs2 = %f ' % (e1(r), e2(r), abs1(r), abs2(r)))
# Solution: e1 = 0.000000  e2 = 0.191743 abs1 = 0.002057 abs2 = 0.000000

# 16M
from FuncDesigner import *
from openopt import LP
e1, e2, abs1, abs2 = oovars(4)
f1 = 2.14*e1 + 7.26*e2 - 2.85
f2 = 81.46*e1 + 304.00*e2 - 121.79
obj = abs1+ abs2
constraints = [e1>0, e2>0, abs1>f1, abs1>-f1, 
abs2>f2, abs2>-f2]
startPoint = {e2:0, e1:0, abs1:0, abs2:0}
p = LP(obj, startPoint, constraints = constraints)
r = p.solve('glpk')
print('Solution: e1 = %f  e2 = %f abs1 = %f abs2 = %f ' % (e1(r), e2(r), abs1(r), abs2(r)))
# Solution: e1 = 0.000000  e2 = 0.400625 abs1 = 0.058538 abs2 = 0.000000

# 32M
from FuncDesigner import *
from openopt import LP
e1, e2, abs1, abs2 = oovars(4)
f1 = 2.14*e1 + 7.26*e2 - 5.79
f2 = 81.46*e1 + 304.00*e2  - 254.82
obj = abs1+ abs2
constraints = [e1>0, e2>0, abs1>f1, abs1>-f1, 
abs2>f2, abs2>-f2]
startPoint = {e2:0, e1:0, abs1:0, abs2:0}
p = LP(obj, startPoint, constraints = constraints)
r = p.solve('glpk')
print('Solution: e1 = %f  e2 = %f abs1 = %f abs2 = %f ' % (e1(r), e2(r), abs1(r), abs2(r)))
#Solution: e1 = 0.000000  e2 = 0.838224 abs1 = 0.295504 abs2 = 0.000000

\end{comment}



\section{Conclusion}

We have presented a method to decompose arbitrary programs into linear combinations of benchmarks of a test-bed by employing a black box approach, measuring only its resource usage, without the need to analyse a program's source code. Valid metrics of resource usage are both performance counters and energy consumption (or completion time). Performance counters can therefore be used to build a model capable of predicting the energy consumption (or completion time) of the same program on a different computational system. The same method also gives us useful information about what the differences between computational systems are, thereby showing which computational patterns consume more resources.
We intend to apply this method to heterogeneous computing (CPU/GPU), virtual machines and cloud systems to provide realtime analysis and forecasting of energy consumption (as well as completion time) of software, without prior knowledge of its source code. 
%TODO: Automatic segmentation techniques

\bibliography{Energon_bibliography}


\end{document}
