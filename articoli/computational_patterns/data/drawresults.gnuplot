set terminal epslatex size 7.00cm,5.00cm
set output fileout
# x is the x axis
# i1,i2,i3 are the columns used to filter the results
# v1,v2,v3 are the values $i1,$i2 and $i3 must be equal to
plot "<awk '{ if ($".i1." == ".v1." && $".i2." == ".v2." && $".i3." == ".v3.") {print $".x.",$5,$6,$7,$8,$9,$10}}' LS_vs_NNMF.dat" \
	using 1:2 t "ciao"

