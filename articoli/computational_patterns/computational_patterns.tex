%This is a template for producing OASIcs articles.
%See oasics-manual.pdf for further information.

\documentclass{llncs}

\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amssymb}
%\usepackage{amsthm}
\usepackage{amsmath}
%\usepackage{setspace}


\graphicspath{ {./data/} }

\bibliographystyle{plain}

\begin{document}

\title{Computational patterns}
%\subtitle{Do you have a subtitle?\\ If so, write it here}

%\titlerunning{LBM prediction model}        % if too long for running head

\author{Davide Morelli }

%\authorrunning{Short form of author list} % if too long for running head

\maketitle



\begin{abstract}

TODO

\end{abstract}




\section{Computational patterns}

This section explains in details the concept of \emph{computational pattern}, that is the foundation of LBM. 
As stated in Definition \ref{computational_pattern} a \emph{computational pattern} is an ideal concept. Real programs are composed of computational patterns, but in general a computational pattern can't be a real programs on itself. A program can't consist of a single computational pattern, as every program has to perform a large amount of operations such as loading instructions into memory. Therefore computational patterns can't be measured, we can only measure real program, that are composed of several computational patterns.

Examples of computational patterns are: a program made in its entirety by floating point operations; or a program that triggers a cache miss at every instruction. Computational patterns are orthogonal to each other. Computational patterns are usually ideal programs, real programs can not consist only of a single computational pattern. At most synthetic benchmarks can approximate particular computational patterns. Some computational pattern could be reasonably be guessed (in some case even designed), but in general they are unknown, and may arise when new micro-architectures are created: a novel micro-architecture could expose a peculiar resource usage when used by a certain sequence of instructions.

The model assumes that all programs can ideally be decomposed in sequences of computational patterns.
The computational patterns form a basis of the resource consumption space (because they are orthogonal with respect to resource consumption). Any program, including both the benchmark and the target program, can be written as a linear combination of the computational patterns. If every computational pattern used by the target program is contained at least in one of the benchmarks, and if the benchmarks are not linearly dependent, we can operate a change of basis and express the target program as a linear combination of the benchmarks. If the target program contains computational patterns that are not contained in any benchmark, then the change of basis will lose information.

If we could measure computational patterns directly, and because they are orthogonal, we could express every program as a linear combination of computational patterns, or as a point in the \emph{computational patterns space} (where each computational pattern is a dimension).
Because computational patterns can not be measured directly, we express the target program in terms of other programs (benchmarks), relying on the fact that the target program and the benchmarks will share most of the computational patterns, losing little information in the change of basis between the computational pattern space and the benchmarks space.


\subsection{Synthetic example}

In the following example we have 4 programs $p_1$, $p_2$, $p_3$ and $p_4$, composed by a combination of 4 computational patterns $a$, $b$, $c$ and $d$, table \ref{example1_real} shows the composition of each program in terms of how many instances of each compositional pattern.

\begin{table*}
\caption{Composition of programs in terms of computational patterns} 
\label{example1_real}
\begin{center}
\begin{tabular}{ l | c || c | c | c }
program & $a$ & $b$ & $c$ & $d$ \\
\hline
$p_1$ & 1 & 1 & 0 & 0 \\
$p_2$ & 0 & 0 & 1 & 1 \\
$p_3$ & 1 & 0 & 1 & 0 \\
$p_4$ & 0 & 1 & 0 & 1 \\
\end{tabular}
\end{center}
\end{table*}

Let's assume we can measure computational patterns on 4 different counters, where they show 4 different behaviors: on $\mu_1$ they all cost the same, on $\mu_2$, $\mu_3$, $\mu_4$ and $\mu_5$ all patterns cost the same except one, that costs twice as much, as show in table \ref{example1_comp_measures}. 

\begin{table*}
\caption{Measures of the computational patterns} 
\label{example1_comp_measures}
\begin{center}
\begin{tabular}{ l | c || c | c | c }
system & $a$ & $b$ & $c$ & $d$ \\
\hline
$\mu_1$ & 1 & 1 & 1 & 1 \\
$\mu_2$ & 2 & 1 & 1 & 1 \\
$\mu_3$ & 1 & 2 & 1 & 1 \\
$\mu_4$ & 1 & 1 & 2 & 1 \\
$\mu_5$ & 1 & 1 & 1 & 2 \\
\end{tabular}
\end{center}
\end{table*}

The 4 programs will therefore have the costs shown in table \ref{example1_prog_measures}.

\begin{table*}
\caption{Measures of the programs} 
\label{example1_prog_measures}
\begin{center}
\begin{tabular}{ l | c || c | c | c }
system & $p_1$ & $p_2$ & $p_3$ & $p_4$ \\
\hline
$\mu_1$ & 2 & 2 & 2 & 2 \\
$\mu_2$ & 3 & 2 & 3 & 2 \\
$\mu_3$ & 3 & 2 & 2 & 3 \\
$\mu_4$ & 2 & 3 & 3 & 2 \\
$\mu_5$ & 2 & 3 & 2 & 3 \\
\end{tabular}
\end{center}
\end{table*}

\subsection{Cross-architecture}

To have a recognizable effect on the measures of the programs, computational patterns have to show a different behavior on the counters, therefore if we pick similar counters e.g. completion time on system $S_1$ and completion time on system $S_2$ they will show a different behavior only if the systems are different architecture, otherwise the measures will be very similar, the cosine similarity of the vectors in the measure space will be very close to 1, the subspace of the possible measures will be a very narrow cone; if $S_1$ and $S_2$ are different architectures then the behavior of the patterns on the counters will not be the same, the cosine similarity of the measures will be lower, we will be able to distinguish their effect on the measures of the programs. 

It is important to remember that we will never actually be able to measure or recognize computational patterns alone, because they are not programs we can run and measure, we will only see their effect in the measures of concrete programs, that we can run and measure, that are composed of computational patterns.

TODO: programs on measures vs programs on patterns: is it equivalent to a change of basis?

\subsection{Algebraic characterization}

Be $P$ the matrix with the measures of every computational pattern, where each column of $P $ is a computational pattern, and every row is a resource; and $B$ the matrix where each benchmark is expressed in the \emph{computational pattern space}, every column is a benchmark, every row is a computational pattern; then $X$ in \ref{Patterns_measures_to_benchmarks_measures} is the matrix that contains the measures of every benchmark, every column is a benchmark, every row is a resource. 

\begin{equation}
\label{Patterns_measures_to_benchmarks_measures}
P B = X
\end{equation}

We can not directly measure computational patterns, so we can not see $P$ or $B$, but we can measure the benchmarks, so we can see $X$.

Because we assume that both the benchmarks and the target program is a linear combination of computational patterns, there must be a vector $w$ that maps $P$ into the column vector $y$ that contains the measures of the target program. $w$ represents the linear combination of the target program in terms of the computational patterns. 

\begin{equation}
\label{Patterns_measures_to_target_measures}
P w = y
\end{equation}

$B$ and $w$ represent the position of the benchmarks and the target program in the \emph{computational patterns space}.

Using linear regression we express the target program as a linear combination $\beta $ of the benchmarks, therefore \ref{linear_regression} expresses the vector $y$ (the measures of the target program) as a linear combination of $X$ (the measures of the benchmarks)

\begin{equation}
\label{linear_regression}
y = X \beta + \epsilon
\end{equation}




\section{Svalvolamenti}



$P$ has resources rows and patterns columns. It maps linear combinations of patterns into resource usage.

$B$ has patterns rows and benchmarks columns. It maps linear combinations of benchmarks into patterns.

$w$ has patterns rows and 1 column.

$\beta$ has benchmarks rows and 1 column.

$y$ has resources rows and 1 column.

$\epsilon$ has resources rows and 1 column.

$B^{+}$ has benchmarks rows and patterns columns. It maps linear combinations of patterns into benchmarks. 

$ P^{+} $ has patterns rows and resources columns. It maps linear combination of resources into patterns.



$P w = X \beta + \epsilon$

$P w = P B \beta + \epsilon $

$ P (w - B \beta) = \epsilon $


\section{The LBM model}

Benchmarking is currently more an art than a science where the performance of a system $S$ is measured against a particular test
$T$ in order to characterize the performance of S with respect to $T$. If $T$ captures a particular feature of a program $P$ we may infer that if $T$ performs well on $S$ then $P$ will follow the same pattern. Moreover, by varying either $S$ or $T$ it is possible to compare different systems against a benchmark or different benchmark against a given system. The quality of a benchmark $T$ is given by the implicit power of capturing a predictive aspect, though what prediction means is often hinted without formal specification.

As witnessed by \cite{rivoire_models_2008}, benchmarking is largely driven by industry and practitioners rather than a well defined theory, this is due to the fact that it is difficult to relate a particular program $P$ with a particular test $T$.

We propose LBM (Linear Benchmarking Model), a model designed to describe the relation between a set of benchmarks and a program.

LBM is a generalization  of \cite{tiwari_power_1994,hoste_performance_2006,snavely_modeling_2001,sharkawi_performance_2009}, designed to be resource agnostic, it can be used to characterize both hardware and software as shown in \cite{morelli_compositional_2012}, and can predict the completion time as well as the energy consumption, the allocated memory, the number of cache misses, etc. Resource consumption of programs is a known non linear phenomenon, but linear regression using non linear regressors can be used to estimate it.

\subsection{Definitions}

In this section we provide the definition for terms used in the rest of the paper for presenting the case study.

%\newtheorem*{definition}{Definition}

\begin{definition}[program]
A program is a particular and defined sequence of instructions.  
\end{definition}
The same program can be run on different micro architectures, even if will generate different low level sequence of processor instructions, it will still be considered the same program. When called to process different input sizes, because the sequence of high level instructions will considerably change, it will be considered a different program. The target program is the program whose resource consumption we are interested in predicting.

\begin{definition}[computational environment]
A computational environment is any computational system that can execute programs. 
\end{definition}
Examples of computational environments are embedded computers, smartphones, PCs with different micro-architectures, clusters. We consider part of the computational environment the hardware as well as the operative system and all the software running on the machine at the same time as the program being measured.


\begin{definition}[counter]
A counter is a resource of a particular computational environment, used to execute the program, whose usage can be measured.
\end{definition}

The energy used by a computer, or the time used to complete a program are examples of metrics. The same resource on different computational environments are considered different counters: e.g. completion time on computer A and completion time on computer B are different counters. A counter is always in a one to one relationship with a computational environment, a counter can never refer to multiple computational environments.

Not every counter can be used in our model, it needs to provide measures that have the following properties: non negativity ($\forall x$ $\mu(x) \geq 0 $), null empty set ($\mu(\varnothing)=0$) and countable additivity ($\mu(\cup x_i) = \sum \mu(x_i)$). Examples of valid counters are processor time, completion time, cache misses, energy. Examples of invalid resources are \% processor time (it may decrease), active memory (memory could be deallocated), power (instant power could decrease). Usually invalid counters can be made valid combining them with time. 

An interesting example is power that is not a valid counter. Let's consider a program $X$ that is composed of 2 programs $A$ and $B$ executed sequentially: 

\begin{equation*}
X = \{ A ; B \}
\end{equation*}

if $P_a$ the average power during the execution of $A$ is higher than $P_b$ the average power during the execution of $B$, then $P_x$ the average power during the execution of $X$ will be $P_b \leq P_x \leq P_a $. This violates the required countable additivity property (the power used by a part of a program is higher than the power used by the whole program). On the other hand, because both average power and completion time are always positive quantities, countable additivity holds for energy: $E_x = E_a + E_b = P_a T_a + P_b T_b $, $E_x \geq E_a$, $E_x \geq E_b$ (where $T_a$ and $T_b$ are completion times for $A$ and $B$).

\begin{definition}[target counter]
The target counter is the counter that we want to predict for the target program.
\end{definition}

\begin{definition}[measure]
A measure is a positive real number that describes the quantity of resource used by a certain program to run on a certain computational environment, as reported by the corresponding counter.
\end{definition}

A measure always refers to both a program and a counter (therefore a computational environment), i.e. a measure quantifies the usage of a particular resource on a particular computational environment by a program, as reported by the counter.

\begin{definition}[target measure]
The target measure is the measure of the target counter and the target program that we want to predict.
\end{definition}


\begin{definition}[benchmark]
A benchmark is a program used to predict the measure of the target counter and the target program. 
\end{definition}

\begin{definition}[surrogate]
A surrogate is a representation of the target program in terms of the benchmark, used as a model to predict the resource consumption of the target program. 
\end{definition}

\begin{definition}[solver]
A solver is an algorithm that, given a set of measures of the benchmarks and the target program, creates a surrogate for it.
\end{definition}

\begin{definition}[computational pattern]
\label{computational_pattern}
A computational pattern is an ideal program the exhibits a peculiar resource consumption.
\end{definition}
Examples of computational patterns are: a program made in its entirety by floating point operations; or a program that triggers a cache miss at every instruction. Computational patterns are orthogonal to each other. Computational patterns are usually ideal programs, real programs can not consist only of a single computational pattern. At most synthetic benchmarks can approximate particular computational patterns. Some computational pattern could be reasonably be guessed (in some case even designed), but in general they are unknown, and may arise when new micro-architectures are created: a novel micro-architecture could expose a peculiar resource usage when used by a certain sequence of instructions.

The model assumes that all programs can ideally be decomposed in sequences of computational patterns.
The computational patterns form a basis of the resource consumption space (because they are orthogonal with respect to resource consumption). Any program, including both the benchmark and the target program, can be written as a linear combination of the computational patterns. If every computational pattern used by the target program is contained at least in one of the benchmarks, and if the benchmarks are not linearly dependent, we can operate a change of basis and express the target program as a linear combination of the benchmarks. If the target program contains computational patterns that are not contained in any benchmark, then the change of basis will lose information.

\section{Model definition}

TODO: describe the model at a higher level, common to both linear regression and NNMF

TODO: programs are linear combinations of computational patterns.

TODO: the surrogate ?



\subsection{LBM using linear regression}

Given a set of benchmarks and counters $\mathbf{A}$, the LBM model defines the relation between a program $p_t$ and $\mathbf{A}$. Every benchmark is characterized by a set of measurements, each relating with one of the model's counters. A combination of benchmarks and counters defines an LBM model.

More precisely measures, counters, benchmarks, target program, target counter and target measure are organized as follows:

\begin{itemize}
\item $\mathbf{A}$ is a matrix that contains the measures of the counters used by the benchmarks, this matrix does not contain the target counter. Each row of $\mathbf{A}$ contains the measures relative to a resource, each column contains the measures relative to a benchmark. 
\item $\mathbf{b}$ is a vector that contains the measures of the resources used by the target program. This vector contains measures relative to the same resources used to build $\mathbf{A}$: $\mathbf{b_i}$ is the measure of the target program usage of the same resource measured by the values contained in the $i^{th}$ row $\mathbf{A}$.
\item $\mathbf{c}$ is the vector that contains measures of the resource that we want to predict used by the benchmarks. This vector contains measures relative to the same benchmarks used to build $\mathbf{A}$: $\mathbf{c_i}$ is the measure of the target resource used by the benchmark whose values are contained in the $i^{th}$ column of $\mathbf{A}$.
\item $\mathbf{x}$ is the surrogate of the target program with respect to the benchmarks.
\item $\mu_t$ is the target measure.
\end{itemize}

$\mathbf{A}$, $\mathbf{b}$, $\mathbf{c}$, $\mathbf{x}$ and $\mu_t$ are linked by the following equations:

\begin{equation}
\mathbf{Ax}=\mathbf{b}
\label{find_surrogate}
\end{equation}

\begin{equation}
\mathbf{cx}=\mu_t
\label{prediction}
\end{equation}

LBM models can be used to predict target counters: given a set of measures of counters for a set of benchmarks and a target program $p_t$, we can express $p_t$ as a linear combination of the benchmarks: using equation \ref{find_surrogate} we can find $x$. We can add a counter (called target counter) for which we have measures for the benchmarks but not for the target program, LBM can predict the measure of the target counter for the target program (called target measure), using equation \ref{prediction}.  

Consider the following example: we have 3 different subset of nodes of a uniform cluster $S_1$, $S_2$ and $S_3$, we have measured the energy consumption of a CPU bound program $b_1$ and a communication bound program $b_2$ on all the subsets, we have also measured a program $p_t$ on $S_1$ and $S_2$, and we want to predict the energy consumption of $p_t$ on $S_3$. Energy consumption on $S_1$, $S_2$ and $S_3$ are counters, with $S_3$ being the target counter; $p_t$ is the target program, the measure of the energy consumption of $p_t$ on $S_3$ is the target measure. The measures ($\mu$) of $b_1$ and $b_2$ on $S_1$ and $S_2$ will form $A$:
\[
\mathbf{A} = 
\begin{pmatrix}
\mu(b_1,S_1) & \mu(b_2,S_1) \\
\mu(b_1,S_2) & \mu(b_2,S_2) \\
\end{pmatrix}
\]

The measures we have for the target program $p_t$ will form vector $\mathbf{b}$, with the same counters and in the same order as in $\mathbf{A}$:
\[
\mathbf{b} = 
\begin{pmatrix}
\mu(p,S_1) & \mu(p,S_2) \\
\end{pmatrix}
\]

The measures for the target counter for the benchmarks ($b_1$ and $b_2$) will form vector $\mathbf{c}$, with the same programs and in the same order as in $\mathbf{A}$:
\[
\mathbf{c} = 
\begin{pmatrix}
\mu(b_1,S_3) & \mu(b_2,S_3) \\
\end{pmatrix}
\]

$\mu_t$ is the target measure: the energy consumption of the target program $p_t$ on $S_3$. 

The surrogate $x$ not only is a tool to predict the target measure $\mu_t$, also provides information about the target program, with respect of the benchmarks. In this example it will estimate the composition of the target program $p_t$ in terms of \emph{CPU bound} vs \emph{communication bound}.

TODO: interpretation of the surrogate


\section{Non Negative Matrix factorization}

TODO: describe what NNMF is, where it comes from

TODO: describe the algorithm used to get W and H, w and h, how to use them to predict resource usage

TODO: what is the surrogate?

TODO: interpretation of H W and the surrogate

TODO: overcomes least squares limitations

\section{Factor analysis}

\section{SVD}

err(SVD)<err(NMF)

\section{Experimental validation}

\subsection{Synthetic experiment}

TODO: create a set of computational patterns, create a set of programs, try to predict the programs using linear regression, using NNMF.

TODO: what happens changing the number of patterns? measures? programs?

TODO: what happens changing the similarity of counters? (create patterns that are similar to each other)

TODO: test similar programs (that uses similar combinations of comp patterns) vs non similar. Does LS work better?

\subsection{CPU SPEC 2006}

\subsection{OpenFOAM}


%%
%% Bibliography
%%

%% Either use bibtex (recommended), but commented out in this sample

\bibliography{Energon_bibliography}

%% .. or use bibitems explicitely



\end{document}
