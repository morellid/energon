\section{Energy and experimental computational complexity: sorting algorithms}

In this section we explore the use of energy consumption as the only measured \emph{resource}, trying to characterize the experimental complexity of algorithms only looking at their energy consumption. The goal of this experiment is to show that energy is a good metric, capable of capturing the complexity of algorithms.


\subsection{Experiment setting}

We created a simple micro-benchmark (called \emph{CPU}), to be used as basis, that performs $2^{20}$ integer sums. We measured the power consumption using an ammeter at the power plug, of a computer with an AMD Opteron SledgeHamer 1.4 GHz, Socket 940, 2 GB RAM, running Windows Vista Home Edition. The declared CPU power consumption is 89 W. The energy used by the \emph{CPU} micro-benchmark is 217.54 J. We wrote an measured 3 sorting algorithms, \emph{mergesort}, \emph{heapsort}, and \emph{quicksort}, written in C, using vectors of dimension from 1M to 41M elements. 

Because we are measuring a single \emph{resource} and the basis is composed of a single \emph{benchmark}, the equation \ref{eq:regression} simplifies from matrix to scalar operations only, as shown in equations \ref{eq:sorting} and \ref{eq:sorting_beta}, where $\mu_\text{CPU}$ is the measured energy consumption of \emph{CPU}, and $\mu_{p(i)}$ is the energy consumption of the \emph{target program}. 

\begin{align}
\mathbf{y} &= \mathbf{X} \mathbf{\beta} + \mathbf{\epsilon} \nonumber \\ 
\mu_{p(n)} &= \mu_\text{CPU} \beta_n  \label{eq:sorting}  \\
\beta_n &= \dfrac{\mu_{p(n)}}{\mu_\text{CPU}} \label{eq:sorting_beta} 
\end{align}

For every input size $n$ we found the corresponding $\beta_n$, simply dividing the energy consumption of the algorithm by the energy consumption of \emph{CPU}.

To find the experimental complexity $\xi$ we performed curve fitting, with linear regression, using the same curves described in section \ref{sec:compositional} (linear, quadratic, cubic, logarithmic, and log-linear).

\subsection{Experimental results}

Figure \ref{fig:complexity_Heapsort} shows the single component of $\beta_n$ (simply the ration between \emph{heapsort} and the \emph{CPU} benchmark) as the input size changes, and the $\xi$. The best fitting curve was \emph{log-linear}, with $R^2=0.9999$ and multiplicative factor $4.74 \times 10^{-09}$. The complete formula of $\xi$ is shown in equation \ref{eq:xi_heapsort}.

\begin{align}
\label{eq:xi_heapsort}
\xi(n) &= 4.74 \times 10^{-09}  n \log_2n \\ \tag*{$\xi$ of heapsort}
\end{align}

\begin{figure*}[h]
\input{R/complexity_Heapsort}
\caption{$\beta$ and $\xi$ for \emph{heapsort}}
\label{fig:complexity_Heapsort}
\end{figure*}

Figure \ref{fig:complexity_Mergesort} shows the single component of $\beta_n$ (simply the ration between \emph{mergesort} and the \emph{CPU} benchmark) as the input size changes, and the $\xi$. Like for \emph{heapsort}, the best fitting curve was \emph{log-linear}, with $R^2=0.9998$ and multiplicative factor $1.08 \times 10^{-08}$. The complete formula of $\xi$ is shown in equation \ref{eq:xi_mergesort}.

\begin{align}
\label{eq:xi_mergesort}
\xi(n) &= 1.08 \times 10^{-08}  n \log_2n \\ \tag*{$\xi$ of mergesort}
\end{align}


\begin{figure*}[h]
\input{R/complexity_Mergesort}
\caption{$\beta$ and $\xi$ for \emph{mergesort}}
\label{fig:complexity_Mergesort}
\end{figure*}


Figure \ref{fig:complexity_Quicksort} shows the single component of $\beta_n$ (simply the ration between \emph{quicksort} and the \emph{CPU} benchmark) as the input size changes, and the $\xi$. The best fitting curve was \emph{quadratic}, with $R^2=0.9934$ and multiplicative factor $2.38 \times 10^{-14}$. The complete formula of $\xi$ is shown in equation \ref{eq:xi_quicksort}.

\begin{align}
\label{eq:xi_quicksort}
\xi(n) &= 2.38 \times 10^{-14} n^2 \\ \tag*{$\xi$ of quicksort}
\end{align}

\begin{figure*}[h]
\input{R/complexity_Quicksort}
\caption{$\beta$ and $\xi$ for \emph{quicksort}}
\label{fig:complexity_Quicksort}
\end{figure*}

For all sorting algorithms the experimental computational complexity $\xi$ was the same as the theoretical time complexity $O$. Interestingly, the $\xi$ of \emph{quicksort} was the same curve as the theoretical worst case $O(n^2)$ instead of the average case $O(n\log n)$. However, as can be seen in figure \ref{fig:complexity_Quicksort} the growth of the \emph{surrogate} is not as steep as $n^2$. It lies between $n \log n$ and $n^2$, a curve not modelled by $\xi$. Allowing linear combinations of curves, the best fit is the sum of quadratic and log-linear. 
Figure \ref{fig:complexity_Quicksort_combined} shows the fitting of the \emph{surrogate} and the $\xi$ described in formula \ref{eq:xi_quicksort_combined}.

\begin{align}
\label{eq:xi_quicksort_combined}
\xi(n) &= 1.54 \times 10^{-14} n^2 + 3.17 \times 10^{-9} n \log_2 n \\ \tag*{$\xi$ of quicksort, with quadratic and log-linear combined}
\end{align}

\begin{figure*}[h]
\input{R/complexity_Quicksort_combined}
\caption{$\beta$ and $\xi$ for \emph{quicksort}, allowing linear combination of curves}
\label{fig:complexity_Quicksort_combined}
\end{figure*}


\section{Micro-architecture independence: limits of a single dimension}
\label{sec:single_dimension}

To verify the limits of using a single value as \emph{surrogate} presented in chapter \ref{chapter:single_number}, we compared the results for \emph{mergesort} characterized using \emph{CPU} only presented in the previous section, with the same \emph{programs} (the \emph{CPU} benchmark and \emph{mergesort}) on different machines. We expect the \emph{surrogate} to show small differences for similar architectures, and large differences for different architectures. In the first experiment we use a machine of the same family, but with a slightly newer processor. In the second experiment we use a completely different architecture. We present the results of characterizing using measures from each architecture separately, as well as in the same model. We conclude this section showing that using the \emph{target program} to characterize itself, the model error disappears, and that using multiple \emph{programs} as basis (instead of just a single program), the model error becomes much smaller.

\subsection{Different architectures}

In the first experiment we used the same micro-architecture as in the previous section, but with a slightly newer processor: AMD Athlon 64 X2 Toledo 4200++, 2.20 GHz, Socket 939, 2 GB RAM, running Windows Vista Home Edition. 
The differences between the Opteron machine (presented in the previous section) and the Athlon machine are:
\begin{itemize}
\item the socket 939 is similar to socket 940, but is revised removing the need for buffered memory, doubling peak memory bandwidth;
\item the clock rate of the Athlon machine is faster than the Opteron (2.2 GHZ versus 1.4GHz);
\item the Opteron is single core, the Athlon is dual core;
\end{itemize}
We measured the energy consumption of the \emph{CPU} micro-benchmark (177.25 J), and of \emph{mergesort}.
Because the micro-architecture is similar, the \emph{computational patterns} will have a similar \emph{resource} consumption on the Athlon and Opteron machines. However, because the CPU is not the same, the \emph{computational patterns} \emph{resource} consumption not be exactly the same. In particular we expect the CPU to run faster, while the cost of accessing memory will be similar, thus violating the conditions expressed in equation \ref{eq:condition9}. We expect therefore the model created using a single \emph{program} in basis to produce inconsistent \emph{surrogates}.

\begin{align}
\label{eq:xi_mergesort.CASA}
\xi(n) &= 5.44 \times 10^{-9}  n \log_2n \\ \tag*{$\xi$ of mergesort on Athlon}
\end{align}

\begin{figure*}[h]
\input{R/complexity_Mergesort.CASA}
\caption{$\beta$ and $\xi$ for \emph{mergesort} on Athlon}
\label{fig:complexity_Mergesort.CASA}
\end{figure*}

In the second experiment we used a different micro-architecture: an Intel ATOM N230, 512KB L2, 4MB RAM. Measured the energy consumption of the \emph{CPU} micro-benchmark (96.62 J), and of \emph{mergesort}. 
Because the architectures of the Opteron and ATOM machines are substantially different, the \emph{computational patterns} will also have a largely different \emph{resource} consumption, violating the conditions expressed in equation \ref{eq:condition9}. We expect therefore the model to produce inconsistent \emph{surrogates}.


Figure \ref{fig:complexity_Mergesort.ATOM} shows the single component of $\beta_n$ (simply the ration between \emph{mergesort} and the \emph{CPU} benchmark) as the input size changes, and the $\xi$. Like for \emph{mergesort} on the AMD machine, the best fitting curve was \emph{log-linear}, with $R^2=0.9998$ and multiplicative factor $1.79 \times 10^{-9}$. The complete formula of $\xi$ is shown in equation \ref{eq:xi_mergesort.ATOM}.

\begin{align}
\label{eq:xi_mergesort.ATOM}
\xi(n) &= 1.79 \times 10^{-9}  n \log_2n \\ \tag*{$\xi$ of mergesort on ATOM}
\end{align}


\begin{figure*}[h]
\input{R/complexity_Mergesort.ATOM}
\caption{$\beta$ and $\xi$ for \emph{mergesort} on ATOM}
\label{fig:complexity_Mergesort.ATOM}
\end{figure*}


The $\xi$ of \emph{mergesort} found using the measures on both the Athlon, Opteron, and ATOM follows the same curve $n \log n$. However, the multiplicative factors are different. In particular, on the ATOM is an order of magnitude smaller than on the Opteron. Predictions made with the $\beta$ found using measures on Opteron alone, would produce a large error on the ATOM machine.

Figure \ref{fig:complexity_architectures_energon} shows the \emph{surrogates} calculated independently on the 3 machines. We can verify that the \emph{surrogate} calculated on Opteron is close but not equal to the \emph{surrogate} calculated on the Athlon. The distance increases as the difference between architectures becomes significant: the \emph{surrogates} on Opteron and ATOM are radically different.


\begin{figure*}[h]
\input{R/complexity_architectures_energon}
\caption{Discrepancy in \emph{surrogates} between Athlon, Opteron and ATOM}
\label{fig:complexity_architectures_energon}
\end{figure*}



\subsection{Using all the measures in the same model, only 1 \emph{program} as basis}

As expected from the theoretical results in chapter \ref{chapter:single_number}, combining the measures from different architectures in the same model, but using a single \emph{program} as basis, with a different structure than the \emph{target program}, will lead to an imprecise model. We combined the measures from the previous experiments, using \emph{CPU} as basis, and \emph{mergesort} as the \emph{target program}, on Athlon, Opteron and ATOM. 

For every input size we build a \emph{surrogate}, using the measures of the \emph{CPU} micro-benchmark on the 3 architectures as matrix $\mathbf{X}$ (single column), and the measures of \emph{mergesort} on all architectures as $\mathbf{y}$. In this experiment the \emph{surrogate} is the same for all the architectures.

To test the goodness of the model, we plotted the actual values versus the fitted values, as shown in figure \ref{fig:complexity_architectures_fitted}. The fitted values show the representation that the model makes of the actual data. The characterization error is large, as evident by the difference between the actual values and the fitted values, for all the architectures.

\begin{figure*}[h]
\input{R/complexity_architectures_fitted}
\caption{Measured versus fitted values using a single \emph{program} as basis on 3 different architectures}
\label{fig:complexity_architectures_fitted}
\end{figure*}

\subsection{Using all the measures in the same model, with a larger basis}

To further verify the theoretical model, we added one program to the basis. We measured \emph{quicksort} on a single input size $n=1M$, on all the architectures. We used \emph{quicksort} as an example of \emph{program} different than the \emph{target program} (they have different computational complexity), composed of different \emph{computational patterns} than the \emph{CPU} micro-benchmark (\emph{quicksort} uses memory, while \emph{CPU} only performs arithmetic operations).

For every input size we build a \emph{surrogate}, using the measures of the \emph{CPU} micro-benchmark and \emph{quicksort(1M)} on the 3 architectures as matrix $\mathbf{X}$ (2 columns), and the measures of \emph{mergesort} on all architectures as $\mathbf{y}$. In this experiment the \emph{surrogate} is the same for all the architectures. 

As expected, the characterization using a basis with more than a single \emph{program} is more accurate. The differences between the Opteron, Athlon, and ATOM architectures are better captured than in the previous experiment, even using a small basis (2 \emph{programs}).

\begin{figure*}[h]
\input{R/complexity_architectures_fitted2}
\caption{Measured versus fitted values using 2 \emph{programs} as basis on 3 different architectures}
\label{fig:complexity_architectures_fitted2}
\end{figure*}

\subsection{Using the \emph{target program} as basis}

To further confirm the theoretical results in chapter \ref{chapter:single_number}, we verified that using the \emph{target program} as the only \emph{program} in the basis, the \emph{surrogates} found independently on the 3 architectures will be the same.

The \emph{surrogates} as the input size grows are shown in figure \ref{fig:complexity_architectures_mergesort}. 

\begin{figure*}[h]
\input{R/complexity_architectures_mergesort}
\caption{Concordance in \emph{surrogates} between Athlon, Opteron and ATOM, using the \emph{target program} as basis}
\label{fig:complexity_architectures_mergesort}
\end{figure*}

\subsection{Discussion}

In this section we have verified that using a single \emph{program} as basis can only lead to precise model in 2 cases:
\begin{itemize}
\item the architecture is substantially the same
\item the \emph{target program} is the same as the \emph{program} in the basis
\end{itemize}

In chapter \ref{chapter:CPUSPEC} we will show that using a sufficiently large number of \emph{benchmarks} and \emph{resources} from different \emph{computational environments} the model becomes accurate, and the prediction has a small error.

Most of the approaches to benchmarking used nowadays attempt to use a single number to characterize computer systems. This experiment shows that this approach inherently leads to inaccurate models. In chapter \ref{chapter:FSCL} we will show that using more than a single dimension to characterize hardware (the dimensionality of $\beta$), we can build expressive models, and the predictions can be accurate enough to chose the best device for the \emph{target program}. The dimensionality of $\beta$ does not need to be large, in fact it should be as small as possible to ensure readability of the \emph{surrogate}, and avoid over-fitting.

This verifies the the theoretical result described in chapter \ref{chapter:single_number}: the model created using a single metric can not be accurate when the underlying \emph{computational environment} changes significantly. 
