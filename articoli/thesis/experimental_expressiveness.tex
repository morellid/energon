%!TEX root = ./master.tex

\subsection{Using the simplex solver}
\label{sec:expressiveness_simplex}

\subsubsection{Experimental setup}

In \citet{morelli_compositional_2012}, we showed how to use our model to characterize \emph{mergesort}, using two micro-benchmarks as basis, measuring only completion time and consumed energy as \emph{resources}.

Here we present an extended version of the same experiments. We measured the completion time and the energy consumption of a small set of programs running on a desktop computer equipped with a CoreDuo processor with 2MB L2 cache and 1 GB RAM. To measure the energy consumption we attached a simple ammeter to the computer power plug (therefore measuring the whole machine power absorption), calculated the average instant power during the execution of each program, then multiplied by the completion time.
We prepared two synthetic benchmarks:
\begin{description}
\item[cpu] is a simple \emph{add} assembler instruction executed $2^{20}$ times
\item[mem] is a program that sums a fixed number of random locations from a large array
\end{description}
We used \emph{cpu} and \emph{mem} as our \emph{benchmarks} and measured \emph{mergesort} sorting arrays of different sizes (1M, 2M, 4M, 8M, 16M, 32M).

Table \ref{tab:ICCSW_basis} shows the measured completion time and energy consumption for the program used as the basis, table \ref{tab:ICCSW_mergesort} shows the measures relative to the \emph{target program} \emph{mergesort}, changing the input size from 1M to 32M.

\begin{table}[h]
\begin{tabular}{l*{2}{c}}
 & cpu     & mem \\
\hline
time      & 2.14 s  & 7.26 s    \\
energy  & 81.46 J & 304.00 J \\
\end{tabular}
\label{tab:ICCSW_basis}
\caption{Completion time and energy consumption for \emph{cpu} and \emph{mem}}
\end{table}

\begin{table}[h]
\begin{tabular}{l*{6}{c}}
 &  p(1M)  & p(2M)   & p(4M)  & p(8M)   & p(16M)   & p(32M)\\
\hline
time      & 0.22 s & 0.33 s  & 0.67 s & 1.39 s   & 2.85 s    & 5.79 s\\
energy   & 8.60 J & 13.20 J & 27.49  & 58.29 J & 121.79 J & 254.82 J\\
\end{tabular}
\label{tab:ICCSW_mergesort}
\caption{Completion time and energy consumption for \emph{mergesort}}
\end{table}

Referring to the high level model defined in section \ref{sec:general_model}, we defined the $\mathbf{X}$ matrix using the first two columns of the above table, and we used the measurement vectors for \emph{mergesort} at various input sizes as $\mathbf{y}$ vectors.

Using the \emph{simplex} solver presented in section \ref{sec:simplex} we created a \emph{surrogate} $\beta$ for every input size, such that $\mathbf{y_i} = \mathbf{X} \beta_i $. We decided to use the \emph{simplex} solver because we were confident that the program in the basis can be representative of actual \emph{computational patterns}.

\[ 
\mathbf{X}= \left( \begin{array}{cc}
2.14 & 7.26\\
81.46 & 304.00 \end{array} \right) 
\]


\[ 
\begin{array}{ccc}
\mathbf{y_{1M}}= \left( \begin{array}{c}
0.22  \\
8.60 \end{array} \right) & 
\mathbf{y_{2M}}= \left( \begin{array}{c}
0.33  \\
13.20 \end{array} \right) & 
\mathbf{y_{4M}}= \left( \begin{array}{c}
0.67  \\
27.49 \end{array} \right) \\
\mathbf{y_{8M}}= \left( \begin{array}{c}
1.39  \\
58.29 \end{array} \right) &
\mathbf{y_{16M}}= \left( \begin{array}{c}
2.85  \\
121.79 \end{array} \right) &
\mathbf{y_{32M}}= \left( \begin{array}{c}
5.79  \\
254.82 \end{array} \right) 
\end{array}
\]

\subsubsection{Expressiveness of $\beta$}

The resulting $\beta$ vectors (calculated using the \emph{simplex solver}) are:
\[  \begin{array}{ccc}
\beta_{1M}= \left( \begin{array}{c}
0.075118  \\
0.008161 \end{array} \right) &
\beta_{2M}= \left( \begin{array}{c}
0.075862  \\
0.023093 \end{array} \right) &
\beta_{4M}= \left( \begin{array}{c}
0.069347  \\
0.071845 \end{array} \right) \\
\beta_{8M}= \left( \begin{array}{c}
0  \\
0.248125 \end{array} \right) &
\beta_{16M}= \left( \begin{array}{c}
0 \\
0.528191 \end{array} \right) &
\beta_{32M}= \left( \begin{array}{c}
0 \\
0.780954 \end{array} \right)
\end{array} \] 


The evolution of the \emph{surrogate} $\beta$ is also shown in figure \ref{fig:expressiveness_beta_mergesort}, where it can be seen that for small input sizes the computation is dominated by CPU, and for large input sizes it quickly gets dominated by memory usage. This is expected because as the array grows it will not fit into cache and a lot of cache miss will occur, therefore most of the time and energy will be spent accessing memory.

\begin{figure*}
\input{R/expressiveness_beta_mergesort}
\caption{$\beta$ for \emph{mergesort}}
\label{fig:expressiveness_beta_mergesort}
\end{figure*}

It is worth noticing that only black box measures were taken on both the \emph{programs} used in the basis and the \emph{target program}, in particular only completion time and energy consumption. Nonetheless the model was able to reveal the increasing memory usage of the sorting algorithm as the input size grows. This result indicates that, given a representative set of programs, non obvious behaviour of programs can be extracted from high level measures, such as energy consumption, not necessarily directly correlated with the described phenomena.


\subsection{Using the linear regression and NMF solvers}
\label{sec:expressiveness_linear_NMF}

In this section we characterize the memory access pattern, using two different solvers: \emph{linear regression} and \emph{NonNegative Matrix Factorization (NMF)}. 

\subsubsection{Experiment setup}

We wrote two simple micro-benchmarks:
\begin{description}
\item[cpu] sums $2^{20}$ random numbers between 0 and 1024, extracted using the ``rand()'' C function, and forced between 0 and 1024 using the modulo ``\%'' operator. The \emph{cpu} program uses only simple CPU instructions, it does not allocate, read, or write memory. We used \emph{cpu} as one of the programs in the basis, meant to represent CPU-only computations.
\item[mem(n)] allocates a buffer of $2^{24}$ integers and, for $\forall i \in [0, 2^{24}]$ assigns $i$ to the $(i \times n) \% 2^{24}$ element of the buffer, where $n$ is of the form $n=2^k+1$ (a power of 2, plus 1). Because the buffer size is a power of 2 and $n$ is a power of 2 + 1, all the items of the buffer are accessed by the algorithm, if $n=1$ the buffer is accessed sequentially, if $n=3$ the algorithm skips 2 locations between each access, and so on. The \emph{mem} program can be used to test the impact of sequential versus non-sequential memory access. We tested \emph{mem} for values of $n$ from $2^1+1$ to $2^{20}+1$. We selected \emph{mem(33)} as one of the programs in the basis, $n=33$ ensures a large amount of cache-misses. This makes it a good representative of memory intensive computations.
\end{description}


We ran the benchmarks on a machine with an Intel(R) Core(TM)2 Duo CPU E6550, 32K L1 cache, 4MB L2 Cache, 8GB RAM, running Linux Fedora 22, kernel 4.0.2-300.

We measured the \emph{cpu-cycles}, \emph{instructions}, \emph{cache-misses}, and \emph{cache-references} performance counters using the ``perf'' kernel tool. We repeated each run 30 times, reporting here only the mean value.

We used \emph{cpu} and \emph{mem(33)} (from now on simply called \emph{mem}) as basis for the model. All the other programs (and cases for \emph{mem(n)}) are expressed in terms of the basis. In other terms, $\beta_i$ describes the program $i$ as a linear combination of \emph{cpu} and \emph{mem}.

\input{R/complexity_basis_resource_consumption}

Table \ref{complexity_basis_measures} shows the \emph{resource} usage of the basis. The two \emph{programs} have very different behaviour with respect to the measured \emph{resources}: the number of cache-misses and cache-references in \emph{mem} is higher by several order of magnitudes than in \emph{cpu}; the ratio between cache-misses and cache-references in \emph{mem} is approximately 1, meaning that almost every access in cache resulted in a miss, whereas for \emph{cpu} the ratio is 1 miss every 25 references; the ratio between cpu-cycles and instructions (CPI) is almost 0.5 for \emph{cpu}, meaning that the super-scalar pipeline is efficiently used, whereas CPI drops at almost 3 for \emph{mem}, because most of the cpu-cycles are spent waiting for cache-misses.

We measured \emph{mem(n)} for values of $n=2^k+1$, for $k$ going from 1 to 20, on the same computational environment as in the previous section, using the same performance counters (cpu-cycle, instructions, cache-misses, cache-references).

Figure \ref{fig:complexity_mem_resource_consumption} shows the measured resource consumption of \emph{mem(n)}, as the number of items of the buffer as skipped between accesses. Large $n$ values result in a sparse memory access patterns, small $n$ values result in a more \emph{sequential} access pattern. Large $n$ values result in frequent cache misses. The number of cache-misses is affected in non-obvious ways by the L1 (32KB) and L2 (4MB) cache sizes. For $n=33$ the number of cache misses is already approximately equal to the number of memory accesses, but for $n$ between 2048 and 32768 the number of cache-misses reduces by an order of magnitude, because the iterator cycles so quickly that the blocks in memory are not replaced before they are used again the next time the iterator reaches the same area of the buffer.

\begin{figure*}
\input{R/complexity_mem_resource_consumption}
\caption{Measured resource consumption of \emph{mem}}
\label{fig:complexity_mem_resource_consumption}
\end{figure*}

\subsubsection{Characterization using linear regression}

To characterize the memory access pattern, as $n$ changes from $n=2^1+1$ to $2^{20}+1$, we used the \emph{linear regression} \emph{solver}, with \emph{cpu} and \emph{mem(33)} as the \emph{benchmarks} used for the basis. The $\mathbf{X}$ matrix is therefore equal to the measures reported in table \ref{complexity_basis_measures}, normalized by row, and the $\mathbf{y_i}$ vectors are the measures of \emph{mem(i)}. The \emph{surrogate} $\beta_i$ refers to the $\mathbf{y_i}$ vector. 

Figure \ref{fig:complexity_beta_mem} shows the evolution of $\beta$ as $n$ grows. For small values of $n$, the memory access pattern is mainly sequential, therefore the computation is dominated by CPU usage. For extremely large values of $n$ the computation is dominated by cache-misses, therefore the \emph{surrogate} assigns a large value to the \emph{mem} component of $\beta$, and a small negative value to the \emph{cpu} component. This indicates that for those values of $n$, \emph{mem(n)} is even closer to the \emph{memory computational pattern} than \emph{mem(33)}. A possible interpretation of $\beta_{2^{20}+1}\approx(-0.92,1.18) $ is that \emph{mem($2^{20}+1$)} can be explained as 1.18 instances of \emph{mem(33)} removing the CPU usage of 0.92 instances of \emph{cpu}.


\begin{figure*}
\input{R/complexity_beta_mem}
\caption{$\beta$ for \emph{mem}}
\label{fig:complexity_beta_mem}
\end{figure*}

\subsubsection{Characterization using NMF}

To characterize \emph{mem(n)} we also used the \emph{NonNegative Matrix Factorization (NMF)} \emph{solver}, presented in section \ref{sec:NMF}. We ran \emph{NMF} on the same matrix $\mathbf{X}$ used in the previous section (containing the measures reported in table \ref{complexity_basis_measures}, normalized by row). The \emph{surrogate} found by the \emph{NMF} solver explains the measures of the \emph{programs} in terms of 2 matrices $\mathbf{W}$ (the basis components matrix) and $\mathbf{H}$ (the mixture coefficients matrix). 

We used the \emph{nmf} function from the \emph{NMF} \emph{R} package \citep{gaujoux_flexible_2010}. The following options were used:
\begin{itemize}
\item the positive parts of the output of Independent Component Analysis (ICA) was used as the seeder \citep{marchini_fastica:_2013}
\item ``Nonsmooth NMF'' \citep{pascual-montano_nonsmooth_2006} was used a to produce sparser factors (to better separate the influence of hidden factors in the programs)
\item he number of hidden factors was set to two (assuming that the only interesting \emph{computational patterns} present in the measured \emph{programs} were accessing memory efficiently and randomly)
\end{itemize}

Figure \ref{fig:expressiveness_NMF} shows the \emph{surrogate} found using both the basis (\emph{cpu} and \emph{mem(33)}) and all the cases for the \emph{mem(n)} program.

The columns of the basis components matrix (the $\mathbf{W}$ matrix) report the estimated measures of the two potential \emph{computational patterns} present in the \emph{programs}. The first hidden factor captures the absence of cache-misses, with a large number of cache-references and instructions. The second hidden factor captures the cache-misses. This indicates that the \emph{surrogate} found by \emph{NMF} found hidden factors close to the desired \emph{computational patterns}. Clustering the performance counters with respect to their importance to find the hidden factors, cache-misses is the most important factor.

The columns of the mixture coefficients matrix (the $\mathbf{H}$ matrix) report the estimated presence of hidden factors in each \emph{program}. The first row represents the first hidden factor, the second row represents the second hidden factor. Clustering programs by their hidden factors composition we can see 2 groups. One group contains \emph{cpu} and the instances of \emph{mem(n)} for which the number of cache misses is small, for small values of $n$ (sequential memory access) and for values of $2^{12}+1<n<2^{17}+1$, as previously discussed. The second group contains the \emph{mem} basis program and the instances of \emph{mem(n)} that resulted in a large number of cache-misses. 

\begin{figure*}
\input{R/expressiveness_NMF}
\caption{Characterization of \emph{mem} using \emph{NMF}}
\label{fig:expressiveness_NMF}
\end{figure*}

This confirms that the \emph{surrogate} found by \emph{NMF} can extract useful information regarding the behaviour of \emph{programs}, indicating what combination of \emph{computational resources} constitute the hidden factors that might be close to \emph{computational patterns}.
