%!TEX root = ./master.tex

In this section we validate the expressiveness of the experimental computational complexity $\xi$. We created and measured a few micro-benchmarks designed to stress CPU only, memory only, and both CPU and memory intensive computations. The goal of the test is to demonstrate that the experimental complexity is capable of describing the behaviour of programs with respect to interesting resource usage, and that it is compositional.  

\subsection{The experimental setup}
We wrote a small number of simple micro-benchmarks:
\begin{description}
\item[cpu] the same program presented in section \ref{sec:expressiveness}.
\item[mem(n)] the same program presented in section \ref{sec:expressiveness}.
\item[f(n)] consists of a cycle of length $log_2n$, in each iteration are performed $2^{20}$ integer sums, $2^{20}$ integer multiplications, $2^{21}$ integer modulo operations, and a random number extraction. We tested \emph{f} for values of $n$ from $2^{16}$ to $2^{23}$, with increments of $2^{16}$.
\item[g(n)] allocates a buffer of $n$ integers, initially set to 0, assigns $n$ random numbers to $n$ random locations of the buffer, sums the values of $n$ random locations of the buffer. The \emph{g} program performs $n$ memory reads, $n$ memory writes, $n$ integer sums, $2n$ integer modulo operations, $3n$ random number extractions. We tested \emph{g} for values of $n$ from $2^{16}$ to $2^{23}$, with increments of $2^{16}$.
\item[f+g(n)] a program that sums the output of \emph{f(n)} and \emph{g(n)}. We tested \emph{f+g} for values of $n$ from $2^{16}$ to $2^{23}$, with increments of $2^{16}$.
\item[fg(n)] a program with the same code as \emph{f(n)}, with the addition of a call to \emph{g(n)} in each iteration of the cycle (executed $log_2n$ times). We tested \emph{fg} for values of $n$ from $2^{16}$ to $2^{23}$, with increments of $2^{16}$.
\end{description}

We ran the benchmarks on the same machine as the the \ref{sec:expressiveness} section, we also measured the same performance counters(\emph{cpu-cycles}, \emph{instructions}, \emph{cache-misses}, and \emph{cache-references}), repeating each run 30 times, reporting here only the mean value.

We used \emph{cpu} and \emph{mem(33)} (from now on simply called \emph{mem}) as basis for the model. All the other programs (and cases for \emph{mem(n)}) are expressed in terms of the basis. In other terms, $\beta_i$ describes the program $i$ as a linear combination of \emph{cpu} and \emph{mem}. Table \ref{complexity_basis_measures} reports the measures of the basis, and section \ref{sec:expressiveness_linear_NMF} discusses those measures.

%\lstinputlisting{R/cpu.c}

%\lstinputlisting{R/mem.c}

%\lstinputlisting{R/f.c}

%\lstinputlisting{R/g.c}

 
Figure \ref{complexity_programs_resource_consumption} shows the \emph{resource} consumption of the \emph{f, g, fg} and \emph{f+g} \emph{programs}.

The program \emph{f} is CPU-bound and similar to the \emph{cpu} program. as expected, the number of cpu-cycle and instructions grow logarithmically with the input size. Like \emph{cpu}, the number of cache references and misses is extremely low, because it does not make use of memory. However, the CPI is slightly above 2, whereas the \emph{cpu} CPI is approximately 0.5. This is probably caused by a nested loop in \emph{f} that interferes with the super-scalar pipeline: the outer loop is repeated $log_2n$ times, the inner loop is repeated $2^{20}$ times; \emph{cpu} only has a single loop of $2^{20}$ iterations. This makes \emph{f} non trivially explainable with \emph{cpu} alone. 

The program \emph{g} is memory-bound. The number of cpu-cycles, instructions, and cache-references performance counters grow linearly with the input size. The number of cache-misses has a different behaviour for small values of $n$ (approximately below $2^{20}$), where the buffer is contained in cache, therefore resulting in a limited number of misses, and for large values of $n$, because the buffer is not contained in cache, and because we access random location of the buffer, the larger the buffer the more likely to require data not contained in cache.

As expected, the performance counters of \emph{f+g} program as simply the sum of the performance counters of \emph{f} and \emph{g}. The performance counters of \emph{fg} show the composition of the \emph{f} and \emph{g} functions: the values of the performance counters are the sum of the values for \emph{f} with the multiplication of the times \emph{g} is performed (\emph{g} is called inside the loop cycle of length $log_2n$) and the values of the performance counters of \emph{g}. 

\begin{figure*}
\input{R/complexity_programs_resource_consumption}
\caption{Resource (cpu-cycles, instructions, cache-misses, and cache-references performance counters) usage of the programs}
\label{complexity_programs_resource_consumption}
\end{figure*}

%\subsection{The extracted values of $\beta$ and the experimental complexity $\xi$}
\subsection{$\xi$ is compositional}
\label{sec:compositional}

We used our model to find the experimental complexity $\xi$ of \emph{f} and \emph{g}, and we combined $\xi_f$ and $\xi_g$ to predict the values of the surrogate $\beta$ of \emph{f+g} and \emph{fg}, to show that the experimental complexity is compositional.

We used \emph{cpu} and \emph{mem(33)} as the the basis for the model. To find the experimental complexity $\xi$ we used the same curves used in the previous sections (linear, quadratic, cubic, logarithmic, log-linear)

To find the experimental complexity $\xi$ we performed curve fitting, with linear regression, using the following functions as independent functions:
\begin{description}
\item[linear] $y=k_1x$. This function captures loops of the form ``for(in i=0; i $<$ n; i++)''.
\item[quadratic] $y=k_2x^2$. This function captures two level nested linear loops.
\item[cubic] $y=k_3x^3$. This function captures three level nested linear loops.
\item[logarithmic] $y=k_4\lceil\log_2x\rceil$. This function captures loops of the form ``for (i=n; i$>$1; i = i/2)''.
\item[log-linear] $y=k_4x\lceil\log_2x\rceil$. This function captures linear loops combined with logarithmic loops.
\end{description}

Figure \ref{fig:complexity_beta_phi_f} shows the $\beta$ values for \emph{f} and the relative $\xi$. The values of both the \emph{cpu} and \emph{mem} components of $\beta$ are well described by the logarithmic curve
 with a small linear component. 

\begin{figure*}
\input{R/complexity_beta_phi_f}
\caption{$\beta$ and $\xi$ for \emph{f}}
\label{fig:complexity_beta_phi_f}
\end{figure*}

Figure \ref{fig:complexity_beta_phi_g} shows the $\beta$ values for \emph{g} and the relative $\xi$. The \emph{cpu} component of $\beta$ is well described by the linear curve, the \emph{mem} component by a combination of logarithmic and negative linear curves. 

\begin{figure*}
\input{R/complexity_beta_phi_g}
\caption{$\beta$ and $\xi$ for \emph{g}}
\label{fig:complexity_beta_phi_g}
\end{figure*}

We manually defined $\hat{\xi_{f+g}} = \xi_f+\xi_g$ and $\hat{\xi_{fg}}=\xi_f+\lceil\log_2n\xi_g \rceil$, from the manual analysis of the source code of the \emph{f+g} and \emph{fg} programs. We then predicted the $\beta$ values applying equation \ref{eq:experimental_example_prediction} to $\hat{\xi_{f+g}}$ and $\hat{\xi_{fg}}$. Figures \ref{fig:complexity_beta_predicted_f_plus_g} and \ref{fig:complexity_beta_predicted_f_dot_g} show the predicted and actual $\beta$ values. As can be seen they are very close, confirming the compositionality of $\xi$.

\begin{figure*}
\input{R/complexity_beta_predicted_f_plus_g}
\caption{Predicted and actual $\beta$ values for \emph{f+g}}
\label{fig:complexity_beta_predicted_f_plus_g}
\end{figure*}

\begin{figure*}
\input{R/complexity_beta_predicted_f_dot_g}
\caption{Predicted and actual $\beta$ values for \emph{fg}}
\label{fig:complexity_beta_predicted_f_dot_g}
\end{figure*}


%\input{R/complexity_measures_mem}

%\begin{figure*}
%\input{R/complexity_beta_mem}
%\caption{test}
%\label{test}
%\end{figure*}

%\bibliography{Energon_bibliography}
