%!TEX root = ../master.tex

In the previous chapters we have used our model to characterize and predict the performance of micro-benchmarks and complex programs (CPUSPEC). 
In this chapter we present a set of experiments that we conducted to test the energy model presented in chapter \ref{chapter:energy_model}. Using programs used for Computer Fluid Dynamics running on a small cluster we measured the power and energy consumption. We used our model to predict the peak power of a cluster running parallel tasks.
Moreover, we attempt to model the energy consumption of concurrent parallel tasks running on the same \emph{computational environment}.

\section{Predict peak power}

As discussed in section \ref{sec:energy_model_introduction} the ability to predict the peak power consumption of a complex computational environment such as a cluster, or a subset of enclosures of a cluster, is important to avoid over-utilization of Power Distribution Units (PDU) and to avoid over-heating. More generally, characterization of power absorption is an important task to estimate battery life in mobile devices, such as smartphones or netbooks. Almost the entirety of those devices have multiple processing units. The energy model presented in chapter \ref{chapter:energy_model} can be applied to both those \emph{computational environments}, and could be used by the Operating System to predict the power absorption.

\subsection{Experiment setting}

We designed a series of experiments to check if the
energy model is capable of extracting the power consumption coefficients of our energy model
(\Pinfr, \Pm and \Pc) only using the total energy consumption and the
information available at runtime to the scheduler (\Twall, \Tjob, $n$ and $m$).

We measured a small set of real-world programs running on a 4 nodes
enclosure of an HPC cluster, and used linear regression to extract
\Pinfr \Pm and \Pc. To check the accuracy of the estimation, we
tried to predict peak power absorption.

OpenFOAM is an open source Computational Fluid Dynamics (CFD) and
structural analysis tool, widely used in HPC clusters.  We tested our
model measuring the completion time and energy consumption of 4 cases
of the tutorials included in the OpenFOAM CFD suite, running on an
enclosure in the IT Center data center at the University of Pisa, with
4 compute nodes, each node equipped with 2 Intel(R) Xeon(R) X5670 CPUs
(2.93GHz), hyper threading disabled, each CPU has 6 cores. We measured
the instant power consumption of the whole enclosure (at the power
socket) using a Phidgets 1122 ammeter \footnote{\url{http://www.phidgets.com/}}, that has
a range of 30A and 0.042A of resolution on AC, corresponding to a
measurement error of approximately 9W. One compute node was running
Windows HPC server 2008 with the measurement framework we wrote
\footnote{\url{https://github.com/vslab/Energon}} to control the experiment and measure the
energy consumed by the enclosure. The remaining 3 compute nodes were
installed with CentOS, Kernel 2.6.32, we installed OpenFOAM from the
RHEL RPM package available on the OpenFOAM website \footnote{\url{http://www.openfoam.org}}. We modified 4 of
the tutorials included in the OpenFOAM distribution as follows:
\begin{enumerate}
\item case \emph{cavity} with the \emph{icoFoam} solver, augmenting
  the mesh density 900 times, 100 iterations 
\item case \emph{pitzDaily} with the
  \emph{adjointShapeOptimizationFoam} solver, augmenting the mesh
  density 400 times, 10 iterations 
\item case \emph{squareBump} with the \emph{shallowWaterFoam} solver,
  augmenting the mesh density 6400 times, 90 iterations 
\item case \emph{mixerVesselAMI2D} with the \emph{pimpleDyMFoam}
  solver, augmenting the mesh density 1000 times, 10 iterations
\end{enumerate}

We measured the completion time \Twall (real time elapsed from the
start of the job to its completion on all nodes), time elapsed in
parallel execution \Tjob (real time spent during the parallel phase of
the computation), and energy consumed (the product of average instant
power, measured by the ammeter at power distribution unit level, and
completion time) by the 4 programs running on 1 (12 cores), 2 (24
cores) and 3 nodes (36 cores).

The programs have different behaviour, indicating different underlying computational patterns: 
\begin{itemize}
\item \emph{squareBump} achieves the best performance with 36 cores, indicating a CPU bound computation, that is not penalized by communications
\item \emph{cavity} and \emph{pitzDaily} reach the minimum at 24 cores, and the performance remain the same using the full 26 available cores, showing an I/O bound computation, that is penalized by frequent communications 
\item \emph{mixerVesselAMI2D} achieves the best performance with 12 cores, indicating an even stronger I/O bound computation.
\end{itemize}

Equation \ref{single_job} can be rewritten as equation \ref{single_job2}:

\begin{equation}
\label{single_job2}
\E = \Pinfr \Twall + \Pm m \Twall + \Pc (\Twall + (n
- 1) \Tjob)
\end{equation}

For every program we measured $n$, $m$, $\Twall$, $\Tjob$ and $\E$

We tried to estimate \Pinfr, \Pm and \Pc using linear regression. 

To get an immediate idea of the quality of our estimates we calculated
the reference values of \Pinfr, \Pm and \Pc as follows:
\begin{description}
\item[experiment A]: we measured the measurement system alone, with no
  active compute node. The power consumption was 161.08 W.
\item[experiment B]: we measured the measurement system alone, with 1
  active but idle compute node. The power consumption was 261.85 W.
\item[experiment C]: we measured the measurement system alone, with 1
  active compute node with 1 processor running at 100\%, the remaining
  11 idle. The power consumption was 273.40 W.
\end{description}

We defined the reference $\Pinfr=161.08W$ as the power consumption
measured during experiment A, the reference $\Pm=100.77W$ as the
difference between the power consumption of the experiment B and the
experiment A; the reference $\Pc=11.56W$ as the difference between the
power consumption during experiment C and experiment B.

The experiments in the following two sections have been designed to
study the accuracy of the energy model, that was designed to be as
simple as possible while retaining high accuracy, using formula
\ref{single_job2}. The accuracy is high enough to allow us to avoid
modeling other aspects of the computational system, focusing only on
active cores.

To verify the quality of the model, we tested the difference between
the predicted and the measured peak power in each experiment, using:
\begin{description}
\item[F-test] to test whether two normally distributed populations can
 be considered to have the same variance.
\item[T-test] to test whether the differences between the measured and
 predicted values can be considered purely the effect of the noise 
 present in the measured values. We used the variant known in statistics 
 as \emph{paired t-test}, or \emph{repeated measures t-test}. 
\item[p-value] the probability that the F-test or the T-test results 
happened by chance. The null hypothesis of the F-test is that the two 
populations have the same variance; the null hypothesis of the T-test 
is that the two populations have the same mean. We reject the null hypotheses 
only if their p-values are lower than 0.05, i.e. the probability that the 
results of the experiment happened by chance are less than 5\%.
\item[$R^2$] the coefficient of determination, indicating how well data 
fit a statistical model. $R^2$ indicates the amount of information in the 
data explained by the model, values near to 1 indicate a good fit. 
For example, an $R^2=0.99$ indicates that 99\% of the data is explained 
by the model.
\end{description}

\subsection{Modelling power using $\Pinfrm$ and $\Pc$, without separating $\Pinfr$ and  $\Pm$}

In our first experiment we tried to explain the energetic behaviour of
our computational environment only in terms of infrastructure and
active cores, neglecting the power usage differences related to
turning on the machines.  The goal of this experiment is to show that
separating $\Pinfrm$ into $\Pinfr$ and $\Pm$ is necessary if multiple
machines are used.  We rewrote \ref{single_machine} into
\ref{single_job3}.

\begin{equation}
\label{single_job3}
\E = \Pinfrm \Twall + \Pc (\Twall + (n
- 1) \Tjob)
\end{equation}

We can get an estimate of $\Pinfrm$ and $\Pc$ from the measurements of
OpenFOAM executions, using formula \ref{single_job3}: we build a
matrix $\mathbf{X}$ where every row is a program, the first column
contains $\Twall$, and the second $(\Twall + (n - 1) \Tjob)$.

We also prepare a vector $ \mathbf{y} $ with the $\E$, the energy
consumption of all the programs, in the same order as the rows of the
matrix $\mathbf{X}$.

The coefficient of the regression, the values of $\mathbf{\beta}$, are
our estimates for $\Pinfrm$ and $\Pc$.

Regression assigns $\Pinfrm = 247.03$, and $\Pc = 21.28$. $R^2 =
0.8209$, showing that the fitting is not very accurate.

\begin{figure*}
\input{energy_model/data/extracted_Pi_Pc_by_LBM}
\caption{Measured power versus estimated power with fitted $\Pinfrm$ and $\Pc$}
\label{extracted_Pi_Pc_by_LBM}
\end{figure*}

Figure \ref{extracted_Pi_Pc_by_LBM} shows the estimated peak power using
formula \ref{single_job2} with $\Pinfrm$ and $\Pc$ fitted with the
regression (the continuous line), as well as the measured values (the
points) sampled in the middle of the execution, when all the cores are
active (peak power consumption).

It is evident how inaccurate peak power absorption is modeled by
formula \ref{single_job3}, in a computational environment where the
number of active machines can change.

F-test for the estimated and the measured peak power consumption
reports a ratio of variances of 1.54 and a $p$-value of 0.01, showing
that the difference in the variances of the measured and predicted
peak power was relevant. We also ran a T-test to check if the mean of
the absolute values of the differences between predicted and measures
peak powers was less than 9W (the expected power measurement error).
The T-test reported a $p$-value below 0.01, forcing us to reject the
null hypothesis. This indicates that the probability that the
difference of the means of the measured and predicted peak powers was
within measurement error was very low.  Both the F-test and the T-test
found statistically relevant differences in the two datasets, showing
that the predicted peak powers were not accurate.

\subsection{Modelling power using $\Pinfr$, $\Pc$, and  $\Pm$}

To verify that \ref{single_job2} can describe accurately the energy
consumption of a parallel task, we added an independent variable to
the regression, whose coefficient represents \Pm.

Similarly to what we did in the previous experiment, we build a matrix
$\mathbf{X}$ where every row is a row is a program, the first column
contains $\Twall$, the second contains $m \Twall$ and the third
$(\Twall + (n - 1) \Tjob)$. This is the same matrix as the one built
for the previous experiment, with the addition of a column.

The same vector $ \mathbf{y} $ is the same as the previous experiment.

The coefficient of the regression, values of $\mathbf{b}$, are our
estimates for $\Pinfr$, $\Pm$ and $\Pc$. This time $R^2=0.9993$,
showing that the regression could fit the data with a much higher
confidence than it was possible in the previous experiment.

\input{energy_model/data/PiPmPc}

Table \ref{PiPmPc} reports the values of the estimated \Pinfr, \Pm and
\Pc, as well as the reference values, and the relative errors.

\begin{figure*}
\input{energy_model/data/extracted_Pi_Pm_Pc_by_LBM}
\caption{Measured power versus estimated power with fitted $\Pinfr$,
  $\Pm$ and $\Pc$}
\label{extracted_Pi_Pm_Pc_by_LBM}
\end{figure*}

Figure \ref{extracted_Pi_Pm_Pc_by_LBM} shows the estimated power using
formula \ref{single_job2} with $\Pinfr$, $\Pm$ and $\Pc$ fitted with
the regression, as well as the measured values. The estimated values
are extremely close to the measured values.

F-test for the estimated and the measured peak power consumption
reports a ratio of variances of 0.96 and a $p$-value of 0.80. We ran a
T-test, to test the null hypothesis that the mean of the absolute
values of the differences between predicted and measured peak power
was less than 9W (the measurement error). The T-test reported a
$p$-value close to 1, allowing us to accept the null hypothesis.  This
shows that the model accurately predicted peak powers, as both the
F-test and T-test could not find statistically relevant differences in
the mean and the variances of the predicted and measured peak power.

Figures \ref{instant_power_1}, \ref{instant_power_2}, \ref{instant_power_3}, and \ref{instant_power_4} show the measured instant power for most of the experiments. The green and red lines indicate the predicted power consumption during the serial and parallel phases, the dotted lines indicate the uncertainty introduced by the ammeter (9W). This shows how precisely the power absorption can be predicted only using values known to the scheduler ($m$ and $n$) and the architecture characterization, i.e. the coefficients extracted by our model (\Pinfr, \Pm, and \Pc ).

\begin{figure*}
\input{energy_model/data/instant_power_1}
\caption{Predicted and measured instant power during serial (green) and parallel (red) phases: cavity}
\label{instant_power_1}
\end{figure*}

\begin{figure*}
\input{energy_model/data/instant_power_2}
\caption{Predicted and measured instant power during serial (green) and parallel (red) phases: pitzDaily}
\label{instant_power_2}
\end{figure*}

\begin{figure*}
\input{energy_model/data/instant_power_3}
\caption{Predicted and measured instant power during serial (green) and parallel (red) phases: squareBump}
\label{instant_power_3}
\end{figure*}

\begin{figure*}
\input{energy_model/data/instant_power_4}
\caption{Predicted and measured instant power during serial (green) and parallel (red) phases: mixerVesselAMI2D}
\label{instant_power_4}
\end{figure*}


\section{Modelling energy consumption of concurrent programs}

Energy is one of the main expenses in a datacenter. Most of the cloud infrastructure offer a pay-per-use cost model. Therefore, is important for cloud providers to be able to account for the energy consumption of each task \citep{kim_energy-based_2011}. However, in a virtualized environment, several tasks run on the same machine at the same time.

Several energy model have been proposed to characterize the energy consumption of tasks \citep{goiri_energy-aware_2010,kim_energy-based_2011}, also parallel tasks have been modelled \citep{garg_energy-efficient_2009,li_energy_2012,wang_towards_2010}. Most of the research focuses on Dynamic Voltage Scaling (DVS) of the CPU. However, not many models attempt to describe the power and energy consumption of concurrent parallel tasks, running on the same \emph{computational environment}. Our energy model can be used to model concurrent execution of tasks. In this section we validate the characterization and prediction of energy consumption in the case of several tasks running on at the same time. This information could be used for accounting in datacenters and cloud environments.

We designed an experiment to check if our model can accurately
describe the energy consumption of two concurrent parallel programs,
running simultaneously on the same machine. We tested the model
assigning 1, 3 or 6 cores to \emph{cavity}, \emph{mixerVesselAMI2D},
\emph{squareBumpFine} and \emph{pitzDaily} on the same machine used in
the previous section, testing all programs pairs and combinations of
number of cores (1, 3 or 6). We ran a total of 90 experiments.


Under the assumption that the
machine will not thrash, equation \ref{limited_resources} can be
refined into equation \ref{multi} as follows:
\begin{itemize}
\item setting $m = 1$ because we use only one machine
\item we measure \Twall and \Tjobi{1} and \Tjobi{2}, information
  available to the scheduler at runtime
\item $\Pc \sum_{i \in \mathrm{cores}}  \Tres{i}$ can be refined as
  the sum of the number of cores used by each program multiplied by
  their $\Tjob$
\end{itemize}

\begin{equation}
\label{multi}
\E = (\Pinfr + \Pm + 2 \Pc) \max(\Twalli{1}, \Twalli{2}) + \Pc ((n_1 -
1) \Tjobi{1} + (n_2 - 1) \Tjobi{2})
\end{equation}


Figure \ref{details_multi_knownT} shows the energy prediction error
distribution for all tests (90), and filtering by program (36
each). Table \ref{accuracy_measures} reports a statistical analysis of
the prediction accuracy and relative errors. 99\% of the predictions
have an error below 0.04. 

The F-test on the predicted and measured values report a $p$-value of
0.86 and a ratio of variances equal to 0.96. 
We also ran the T-test, with the null hypothesis that the absolute values of the
differences of the predicted and measured values were less than 9W
(the measurement error). The T-test reported a $p$-value of
0.18, allowing us to accept the null hypothesis. This shows that the differences in the predictions and
the measures are not statistically relevant, and they should be
assumed to have same variance and difference of mean within measurement error. 
This confirms the accuracy of the model.

\begin{figure*}
\input{energy_model/data/details_multi_knownT}
\caption{Distribution of Energy prediction error running two tasks
in parallel}
\label{details_multi_knownT}
\end{figure*}

\input{energy_model/data/accuracy_measures}

\section{Conclusions}

In this chapter we have tested the accuracy of our energy model. We could characterize the power coefficients $\Pinfr$, $\Pm$, and $\Pc$, using regression on a set of measures of a real world program (OpenFOAM) running in a small cluster, only using information available to the scheduler: completion time, number of machines, the number of core used, and readings from an ammeter at the power plug level (information available on most PDU units). Using the power coefficients we could predict the peak power absorption of the cluster reliably. 

Moreover, to test the ability of the model to describe the energy consumption of concurrent parallel programs running at the same time on the same cluster, we predicted the energy consumption of 90 different combinations of programs and number of user cores. The predictions had a small error. The model is therefore precise, and could be used to build an energy-based accounting system for a datacenter, without requiring any substantial modification to the Operating System or the scheduler, simply processing the scheduler logs.

