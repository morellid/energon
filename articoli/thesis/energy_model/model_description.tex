%!TEX root = ../test.tex

As shown in the previous chapters, our benchmarking model can characterize and predict the consumption of generic \emph{resources}. In our work we are particularly interested in two specific \emph{resources}: completion time and energy consumption. In this chapter we present a simple and high level energy model that further describes the relationship between time and energy. 

% motivation
The ability to predict the energy needed by a system to perform a
task, or several concurrent parallel tasks, allows the scheduler to
enforce energy-aware policies, while providing acceptable
performance.

% state art
The approaches in literature to model energy consumption of tasks
usually focus on low-level descriptors and require invasive
instrumentation of the computational environment.

% model
We developed an energy model and a methodology to automatically
extract features that characterize the computational environment
relying only on a single power meter that measures the energy
consumption of the whole system. In chapter \ref{chapter:OpenFOAM} we show that 
§once the model has been built, the
energy consumption of concurrent parallel tasks can be calculated, with a
statistically insignificant error, even without any power meter.

% conclusion
We show that our model can predict with high accuracy, even only using
the utilization time of the cores in an HPC enclosure, without using
performance counters. Hence, the model could be easily applicable to
heterogeneous systems, where collecting representative performance
counters can be problematic.



%other stuff......
%http://www.cs.york.ac.uk/rts/docs/DAC-1964-2006/PAPERS/1995/DAC95_042.PDF

%Amdahl's law for energy
%http://www.xcg.cs.pitt.edu/papers/cho-cal07.pdf

%http://www.cs.pitt.edu/PARTS/papers/IEEE_TC04_aydin.pdf
%http://www.sigops.org/sosp/sosp09/papers/hotpower_6_ma.pdf

%\subsection{Contributions}
%fine grain is not needed, coarse grain is still accurate.
%linear is enough
%concurrent tasks

\section{The energy model}

In this section we will present the energy model, starting with the
most general form, the we will refine and simplify the model assuming
property of the computational power and execution setting. 

\subsection{General form}

Similarly to \citet{feng_power_2005} and \cite{wang_span:_2011}, in
its most abstract form, the energy consumed by a computational
environment to complete a set of tasks can be written as equation
\ref{general_E}:

\begin{equation}
\label{general_E}
\mathrm{E} = \int_{\Tstart}^{\Tend} \Pt\,\mathrm{d}t
\end{equation}

where $\Tstart$ and $\Tend$ are starting and ending time of the set of
tasks, $\Pt$ is the function of the instant power with respect to
time, as written in equation \ref{general_P}:

\begin{equation}
\label{general_P}
\Pt = \sum_{i \in \mathrm{res}} \Pres{i} \alpha_{i}(t)
\end{equation}

Equation \ref{general_P} represents instant power in its most general
form, where the instant power consumption is just the sum of the
instant power consumption of each computational resource (e.g. cores,
memory, network). $\alpha_{i}(t)$ is the function of the utilization
factor for the $i^{th}$ resource at time $t$, and \Pres{i} the peak
power consumption of the $i^{th}$ resource.

We define \Tres{i} as the integral of $\alpha_{i}(t)$, as shown in
equation \ref{Ti}.
\begin{equation}
\label{Ti}
\Tres{i} = \int_{\Tstart}^{\Tend} \alpha_{i}(t)\,\mathrm{d}t
\end{equation}
Therefore, \Tres{i} can also be defined as the average value of
$\alpha_{i}(t)$ in the timespan from \Tstart to \Tend, multiplied by
$\Twall = \Tend - \Tstart$.

Starting from the general energy definition \ref{general_E} it is
possible to replace equation \ref{general_P} to separate the
time-dependent components from the peak power as in
\ref{general_E_sum_2}. The sum and integral can be swapped by
linearity to get \ref{general_E_sum_3}. Finally the equation can be
simplified replacing the definition \ref{Ti}.

\begin{align}
\label{general_E_sum_1}
\mathrm{E} &= \int_{\Tstart}^{\Tend} \Pt\,\mathrm{d}t =\\
\label{general_E_sum_2}
&= \int_{\Tstart}^{\Tend} \sum_{i \in \mathrm{res}} \Pres{i} \alpha_{i}(t)\,\mathrm{d}t =\\
\label{general_E_sum_3}
&= \sum_{i \in \mathrm{res}} \Pres{i} \int_{\Tstart}^{\Tend} \alpha_{i}(t)\,\mathrm{d}t =\\
\label{general_E_sum}
&= \sum_{i \in \mathrm{res}} \Pres{i} \Tres{i}
\end{align}

Equation \ref{general_E} can therefore be simplified to equation
\ref{general_E_sum}, where only the peak power and average utilization
factor of the resources are needed to calculate the energy
consumption and no integral is explicitly needed.

However, equation \ref{general_P} can be further refined by thinking
about the nature of the various \Pres{i}. In particular we can
identify two distinct elements that contribute in defining the power
consumption:
\begin{description}
\item[Infrastructure]:
that comprises everything that is constantly turned on during all the
computation, therefore not distinguishable  from the fixed cost of
turning on the computational environment. Its power consumption is
written as $\Pinfr$ 
\item[Active machines]: the number of active computers in a
  computational environment, such as a cluster. The power consumption
  of each machine is written as $\Pm$ and refers to the overhead
  needed to power the machine. If the machines are not identical, each
  group of identical machine will have a different \Pm. For simplicity
  we will assume that all machines have identical overhead.
\end{description}

It is worth noticing that when the computational environment consists
of a single machine, $\Pinfr$ will include $\Pm$, as they will not be
distinguishable.

Equation \ref{general_Pi_Pm} shows the refinement of equation
\ref{general_P}, with \Pinfr and \Pm factored out.

\begin{equation}
\label{general_Pi_Pm}
\Pt = \Pinfr + \Pm m(t) + \sum_{i \in \mathrm{res}} \Pres{i} \alpha_{i}(t)
\end{equation}

Machines could be turned on and off during the execution of jobs. In
the previous equation the number of machines that are active at time
$t$ is described by the $ m(t) $ term. 

As an example let us consider the case of an HPC cluster where we
complete a task $A$ with one dual-core machine active, then turn on an
additional dual-core machine (identical to the first machine) to
execute a second task $B$, therefore $m(t)$ will be equal to 1, during
the execution of task $A$ and 2 during the execution of task $B$. \Pt
will change over time, in particular when we turn on the additional
machine $\Pm m(t)$ will double suddenly. If we model the consumption
of cores, $\alpha_{i}(t)$ will also change over time. Let us assume
task $A$ to be strictly serial, it will then use only one core, and
task $B$ to use all the cores available. $\alpha_{i}(t)$ will also
rise suddenly when task $B$ starts.


Now we discuss how the general model can be refined further by adding reasonable
hypothesis on its various terms in order to obtain variants useful
for analyzing the energy consumption and predicting the energy usage of
a given system.

\subsection{Fixed number of active machines and limited considered resources}

Under the assumption that the number of active machines does not
change during the execution of the workload of interest, the function
$m(t)$ becomes a constant $m$.

\begin{equation}
\label{limited_resources}
\E = \Twall (\Pinfr  + m \Pm) + \sum_{i} \Pres{i} \Tres{i}
\end{equation}

Is often unfeasible to attempt to model all the resources in a
computational environment, and typically the desired level of accuracy
allows to restrict our attention on a limited set of resources.

\subsubsection{Focusing on cores of homogeneous machines}

Equation \ref{only_cores} shows the refinement of equation
\ref{limited_resources} focusing only on the power consumed by cores,
where \Pc is the difference between the power consumption when
performing useful job and idle state (idle state includes job
performed by the operating system, i.e. anything not directly related
to the observed tasks) and \Tres{k} is the time that the core $k$ has
been active for the observed tasks.

\begin{equation}
\label{only_cores}
\E = \Twall (\Pinfr  + m \Pm) + \Pc \sum_{k \in \mathrm{cores}}  \Tres{k}
\end{equation}

Assuming that cores can only be in either active or idle state is an
oversimplification, as CPUs have several possible active
states. However, in the experimental section we will show that this
simple model already achieves the desired descriptive and predictive
results, therefore we are not interested in modeling the time spent by
cores in all the possible active state. For these reasons we decided
to opt for the simplest model, restricting the state to either active
or idle. The model can be refined to express the time spent in several
possible states, should this level of detail be considered important.

Equation \ref{only_cores} assumes that all cores have identical power
consumption, i.e. we are modeling an homogeneous system. If we want to
model an heterogeneous system, where different sets of cores have
different power consumption, we simply have to include multiple \Pc in
equation \ref{only_cores}. This is true also for \Pm, that is assumed
to be identical for all machines. If this assumption does not hold, it
is sufficient to add multiple \Pm to equation \ref{only_cores}.

\subsubsection{The case of a single parallel job}

If we consider the execution of a single parallel job running in an
HPC cluster, we can assume that the task will start with a
pre-processing phase running on a single core (e.g. decomposing the
mesh), then execute the parallel job on all the cores for the same
amount of time each, then a post processing phase on a single core
(e.g. recomposing the mesh). If we assume that all the machines are
turned on for the whole task, and that during the parallel phase we
will use $n$ cores, the formula becomes:

\begin{equation}
\label{single_job}
\E = \Twall (\Pinfr  + m \Pm + \Pc) + (n - 1) \Pc \Tjob
\end{equation}

where \Tjob is the time spent in the parallel phase.

\subsubsection{The case of a single machine}

To model the simple case of a job running on a single machine, we can
simplify equation \ref{single_job}, defining $\Pinfrm = \Pinfr + Pm$,
as shown in equation \ref{single_machine}. Merging $\Pinfr$ and $\Pm$
is unavoidable because the inability to turn off the machine makes it
indistinguishable from the infrastructure.

\begin{equation}
\label{single_machine}
\E = \Twall (\Pinfrm + \Pc) + (n - 1) \Pc \Tjob
\end{equation}

\subsubsection{Sequential job on a single machine}

The simplest case to model is that of a sequential job on a single
machine. Defining $\Pinfrmc = \Pinfrm + \Pc$, equation
\ref{single_machine} can be simplified to equation \ref{sequential}.

\begin{equation}
\label{sequential}
\E = \Twall \Pinfrmc
\end{equation}

\subsection{Automatic characterization of hardware}

In this section we will show how refinements of the energy model can
be used to automatically extract a characterization of the
computational environment. 

The refinements presented in formulae
\ref{limited_resources}, \ref{only_cores}, \ref{single_machine}, and
\ref{sequential}, can be generalized to:

\begin{equation}
  \label{eq:general}
  \E = \sum_{i} \Pres{i} \Tres{i}
\end{equation}

where \Pres{i} and \Tres{i} are the power consumption and the
utilization time of the $i^{th}$ resource.

We will assume \Tres{i} to be known, for every considered
resource. The scheduler usually has this information, at least for
relevant computational resources.

We can consider each \Pres{i} as a random variable. We can then run a
set of benchmarks, measuring \Tres{i} and the energy consumption, with a simple
ammeter at the Power Distribution Unit (PDU), for each run. We can
then consider each run as an independent experiment, and use a
statistical approach to estimate \Pres{i}, for every $i$.

Equation \ref{eq:regression_general} shows the general setting of
regression analysis.

\begin{equation}
  \label{eq:regression_general}
  f(\mathbf{X}, \mathbf{\beta}) + \mathbf{\epsilon} = \mathbf{y} 
\end{equation}

where:
\begin{description}
  \item $\mathbf{X}$ is a matrix with the measured \Tres{i}
    of the benchmarks, a row for every experiment, a column for every $i$.
  \item $\mathbf{y}$ is a vector with the measures of the energy
    consumption of the benchmarks, in the same order as the rows of $\mathbf{X}$. 
  \item $\mathbf{\beta}$ is a vector that contains the regression
    coefficients. Every coefficient represents a \Pres{i}. The
    $i^{th}$ coefficient corresponds to the $i^{th}$ column of $\mathbf{X}$. 
  \item $\mathbf{\epsilon}$ contains the fitting residuals, the
    regression error.
  \item $f()$ is a statistical multivariate function that predicts
    $y_i$ using the values of the $i^{th}$ row of $\mathbf{X}$ and
    $\mathbf{\beta}$.
\end{description}

When the underlying model can be assumed to be linear, ordinary least
square (linear regression) can be used as the regression algorithm,
simple vector dot product can be used as $f()$. Linear regression
finds the $\mathbf{\beta}$ that minimizes the norm of the vector
$\mathbf{\epsilon}$ from equation \ref{eq:regression2}.

\begin{equation}
  \label{eq:regression2}
  \mathbf{X \beta + \epsilon} = \mathbf{y} 
\end{equation}

Linear regression is an adequate statistical regression tool only if
the model exposes a linear behaviour. This assumption usually holds for
high-level models, whereas a linear approximation is usually not
feasible on low-level models. For example, performance counters
measure low-level events that interact in non-linear ways. If we build
a linear model using performance counters as \Tres{i}, those
non-linear interactions will not be modelled, and a large error will
arise.

As an example, imagine attempting to measure the completion time of
low level instructions such as \emph{arithmetic sums}, measuring a
benchmark with very few branch mis-prediction on a super-scalar
processor. The super-scalar pipeline will execute several sums in
parallel. If we try to use this model do predict a program that
contains only a few \emph{arithmetic sums} between branch
mis-predictions, the super-scalar pipeline will often be empty, and the
sums will not be executed in parallel, resulting in a very different
completion time.

Measures of energy and completion time are subject to noise, for
example caused by tasks executed in background by the operating
system. For this reason, some outliers are expected to be present in
the measures.  To mitigate their effect on the results of the
regression, we used a robust linear regression algorithm (iterated
re-weighted least squares), as described by
\citet{hoaglin_exploring_2011} and \citet{fox_applied_1997}.

In chapter \ref{chapter:OpenFOAM} we present experimental results of automatic characterization using the method described in this section.

\section{Discussion}
\label{sec:energy_model_discussion}

The power model presented in this chapter is a simple and coarse grain model that describes the energy consumption of \emph{computational environments} at the cluster level.
Notwithstanding the simplicity of the mode, we show in chapter \ref{chapter:OpenFOAM} that it achieves remarkably precise power and energy consumption predictions.
Figure \ref{fig:power_model_example1} shows a realistic example for a cluster where $\Pinfr$ is equal to 100W, $\Pm$ to 90W and $\Pc$ to 10W, on machines with 16 cores. Even using only 1 active core requires consuming $\Pinfr+\Pm+\Pc$ power, then the required power grows linearly with the number of active cores. When we use more cores than available on a single machine, we turn on the second machine. Using 17 cores the power consumption is equal to $\Pinfr+2\Pm+17\Pc$. 

We model the completion time of the program using Amdahl's law \citep{amdahl_validity_1967}, shown in equation \ref{eq:amdahl}: $T(1)$ is the completion time for the program using a single processing unit; $T(n)$ is the completion time using $n$ processing units; $s$ is the portion of the computation inherently serial (a number between 0, completely parallel computation, to 1, completely serial computation); $n$ is the number of used processing units. Asymptotically, the completion time with a very large number of processing units tends to $s T(1)$.

\begin{align}
  T(n) = T(1)(s + \dfrac{1-s}{n}) \label{eq:amdahl} \\
  \tag*{Amdahl's law}
\end{align}

Figure \ref{fig:power_model_example1} also shows the energy consumption as the number of processing units grows. The energy is simply the multiplication of the instant power and the completion time. Because the power overhead of using additional cores grows with the number $n$ of cores, and the completion time decreases proportionally to $\dfrac{1}{n}$, the energy consumption will initially decrease, then when the power increases faster than the time decreases, the energy needed will start to rise. 

\begin{figure*}
\input{energy_model/data/power_model_example1}
\caption{Realistic scenario: power model with $\Pinfr=100$, $\Pm=90$, $\Pc=10$; completion time following Amdahl's law, with 10\% algorithm serial; and power model combined with Amdahl's law}
\label{fig:power_model_example1}
\end{figure*}

Figure \ref{fig:power_model_example2} considers the ideal scenario where it is possible to build a perfect computational environment that does not waste any power, and only uses energy for useful work, we'll have $\Pinfr=\Pm=0$ (because no energy is wasted in overhead), and some non-zero \Pc. Power now grows linearly with the number of active cores. 

Considering also the ideal scenario where we are able to build a perfectly parallel computation, with no inherently serial portion ($s=0$), the completion time will keep decreasing as we add processing units, asymptotically tending to 0.

The resulting energy consumption is constant with the number of processing units used. Adding units saves time, but consumes more power, in the same ratio.

\begin{figure*}
\input{energy_model/data/power_model_example2}
\caption{Idea scenario: power model with $\Pinfr=\Pm=0$, $\Pc=10$; completion time following Amdahl's law, with 0\% algorithm serial; and power model combined with Amdahl's law}
\label{fig:power_model_example2}
\end{figure*}


\section{Conclusions}
We presented a coarse-grain energy model for parallel concurrent tasks, with
several refinements of the general formula to show how it can be
applied to different cases ranging from no restrictions to single machine,
single task, multiple tasks on a single machine, limited considered
resources.

We showed how linear regression can be applied to the formula to
characterize the hardware. The model was accurate enough to allow us
to predict peak power absorption with high statistical confidence.

We also showed how the model and the regression coefficient can
accurately estimate the energy consumption of a system executing
concurrent parallel tasks, only using the information available at
runtime to the scheduler, which could then optimize the energy
consumption while preserving acceptable performance. This approach
could also be used in cloud systems to account for energy usage of
tasks when a single computational environment is used for concurrent
tasks.

We show that the predictions obtained from our model have high accuracy, even only using
the utilization time of the cores in an HPC enclosure, without using
performance counters. Hence, the model could be easily applicable to
heterogeneous systems, where collecting representative performance
counters can be problematic.

Finally, while the estimate of the energy consumption relies on time
measurements, the prediction of the instantaneous power can be
performed using only the linear model coefficients.  This makes it
possible for a cluster management system to decide how many nodes and
cores can be activated while respecting pre-determined power usage
constraints.

As we will show in chapter \ref{chapter:OpenFOAM}, the energy model can be used to model the energy consumption of concurrent and parallel tasks, running at the same time on the same \emph{computational environment}, e.g. a cluster. We show that once the \emph{computational environment} has been profiled and the energy model coefficients ($\Pinfr$, $\Pm$, and $\Pc$) have been extracted, the energy consumed by each individual task can be calculated with a small error. This could be used by a cloud provider to account for energy consumption of the tasks.
