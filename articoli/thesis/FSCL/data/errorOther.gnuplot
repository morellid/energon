set terminal epslatex size 13.0cm,15cm
set output "errorOther.tex"

#set grid 

set multiplot layout 3, 2 title "Distribution of overall relative error"

set xrange [-0.02:1.5]

set key inside left top vertical Right noreverse enhanced autotitles nobox
set xzeroaxis linetype 0 linewidth 1.000
set yzeroaxis linetype 0 linewidth 1.000
set zzeroaxis linetype 0 linewidth 1.000

bin(x, s) = s*int(x/s+0.5)

set xlabel "relative error"
set ylabel "probability"

#set key samplen .2

binsize = 0.1

set boxwidth binsize
set key off

datapoints = "errors_target1_basis2345_featuresAll.dat"
descr = "saxpy"

set title descr 
plot datapoints u 1:(0.25*rand(0)-.35) t '',      "" u (bin($1,binsize)):(1/1000.) s f t 'frequency' w boxes fs solid 0.25,      "" u 1:(1/1000.) s cumul t 'cumulative'

datapoints = "errors_target2_basis1345_featuresAll.dat"
descr = "matmul"

set title descr 
plot datapoints u 1:(0.25*rand(0)-.35) t '',      "" u (bin($1,binsize)):(1/1000.) s f t 'frequency' w boxes fs solid 0.25,      "" u 1:(1/1000.) s cumul t 'cumulative'

datapoints = "errors_target3_basis1245_featuresAll.dat"
descr = "matmul2"

set title descr 
plot datapoints u 1:(0.25*rand(0)-.35) t '',      "" u (bin($1,binsize)):(1/1000.) s f t 'frequency' w boxes fs solid 0.25,      "" u 1:(1/1000.) s cumul t 'cumulative'

datapoints = "errors_target4_basis1235_featuresAll.dat"
descr = "sobel"


# print legend below
set key horiz
set key at 0,-3 bottom left

set title descr 
plot datapoints u 1:(0.25*rand(0)-.35) t '',      "" u (bin($1,binsize)):(1/1000.) s f t 'frequency' w boxes fs solid 0.25,      "" u 1:(1/1000.) s cumul t 'cumulative'

# no legend
set key off
datapoints = "errors_target5_basis1234_featuresAll.dat"
descr = "convolution"

set title descr 
plot datapoints u 1:(0.25*rand(0)-.35) t '',      "" u (bin($1,binsize)):(1/1000.) s f t 'frequency' w boxes fs solid 0.25,      "" u 1:(1/1000.) s cumul t 'cumulative'


unset multiplot
