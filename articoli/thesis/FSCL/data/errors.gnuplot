set terminal epslatex size 7.0cm,5cm
set output fileout

set key inside left top vertical Right noreverse enhanced autotitles nobox
set xzeroaxis linetype 0 linewidth 1.000
set yzeroaxis linetype 0 linewidth 1.000
set zzeroaxis linetype 0 linewidth 1.000
set title descr 

bin(x, s) = s*int(x/s+0.5)

set xlabel "relative error"
set ylabel "probability"

set key samplen .2

stats datapoints using 1 
range = STATS_max - STATS_min

binsize = range / 10.0

set boxwidth binsize


plot datapoints u 1:(0.25*rand(0)-.35) t '',      "" u (bin($1,binsize)):(1/1000.) s f t 'frequency' w boxes fs solid 0.25,      "" u 1:(1/1000.) s cumul t 'cumulative'