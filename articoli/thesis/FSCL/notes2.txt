Guessing the best execution environment

Prediction model
We use linear regression, using benchmarks as regressors, and features computer with FSCL. to predict the completion time of algorithms on different architectures.
Ax=b
where:
each column or A is a benchmark, each row is a feature extracted with FSCL
b contains the features of the target program for which we want to predict the best execution architecture
we solve and find the surrogate x
we then define a matrix A' so that the column are the same benchmarks as A, and the rows are completion time for the different architectures.
we use A' and x to find b'
A'x=b'
b' contains the predictions for the completion time of the target algorithm on each architecture. 
We pick the lower predicted completion time (the lower value of b') as the architecture where it's most convenient to run the target program.

Experiment
We implemented 4 algorithms: simple matrix multiplication, advanced matrix multiplication, sobel filter, convolution filter (TODO also describe the input size).
We executed the algorithms on Tahiti, Devastator and AMD A10-5800K APU with Radeon(tm) HD Graphics, and collected the completion time.

We predicted the best architecture for [TODO: pick the algorithm] and measured the probability of having made a right guess. We measured the accuracy as the number of programs (with different input sizes) changed. In the following tables basis size is the number of programs used to build the basis, extracted randomly, only using the programs different than the target program. The result is the percentage of guessing the right architecture out of the 3 possible. Results larger than 0.333 mean that the prediction algorithm provided better estimation than random guesses. Every case (basis size) was repeated 1000 times to ensure statistical validity.

Guessing program 1:
basis_size accuracy
1 0.142
4 0.561
7 0.706
10 0.763
13 0.839
16 0.891
19 0.893
22 0.922

Guessing program 2:
basis_size accuracy
1 0.42
4 0.53
7 0.55
10 0.58
13 0.651
16 0.689
19 0.739
22 0.789

Guessing program 3:
basis_size accuracy
1 0.636
4 0.704
7 0.795
10 0.822
13 0.766
16 0.734
19 0.701
22 0.646

Guessing program 4:
basis_size accuracy
1 0.707
4 0.895
7 0.885
10 0.911
13 0.911
16 0.899
19 0.883
22 0.863

Guessing program 5:
basis_size accuracy
1 0.613
4 0.368
7 0.508
10 0.505
13 0.495
16 0.495
19 0.516
22 0.513



