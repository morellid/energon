%!TEX root = ../test.tex

% Package to generate and customize Algorithm as per ACM style
% \usepackage[ruled]{algorithm2e}
% \usepackage{amssymb}
% \usepackage{amsmath}
% \usepackage{tikz}
% \usepackage{xfrac}


% Package to generate and customize Algorithm as per ACM style
% \SetAlFnt{\algofont}
% \SetAlCapFnt{\algofont}
% \SetAlCapNameFnt{\algofont}
% \SetAlCapHSkip{0pt}
% \IncMargin{-\parindent}
% \renewcommand{\algorithmcfname}{ALGORITHM}
% \usetikzlibrary{shapes.geometric,arrows}  

% Page heads

% \begin{abstract}
The last few years have seen an increasing number of programming models, languages and frameworks to address the wide heterogeneity and broad availability of parallel computing resources by raising the level of programming abstraction and enhancing portability across different platforms. Despite the ability to run parallel algorithms across different devices, each device is generally characterised by a restricted set of computations for which it outperforms others. On increasingly popular multi-device heterogeneous platforms, the problem is therefore to schedule computations in a way that automatically chooses the best device among the available ones, each time a computation has to be run.

The broad availability and affordability of multi-device systems introduces a dynamism in both the system configuration and in the set of computations to run that make traditional scheduling approaches to be unfit because of restrictions on the computational structure or of the overhead introduced by the scheduling policy.

In this chapter we combine our benchmarking model with runtime code analysis, to efficiently address the problem of dynamically exploiting the computing power of heterogeneous, parallel platforms through a scheduling strategy based on algorithmic feature extraction, completion time prediction and classification. The goal of this experiment is to prove that our benchmarking model can be adapted with sophisticated techniques, in this case runtime analysis of programs, to predict the most performance device for a given instance of a program, at runtime.

In this chapter: 
\begin{itemize}
\item we describe a strategy to efficiently extract programs features at runtime;
\item we combine our benchmarking model, more specifically the \emph{linear regression solver} with code analysis, to estimate the completion time of parallel programs on heterogeneous platforms;
\item we show how to use the \emph{surrogate} to analyse the device characterization;
\item we show how to use the \emph{linear regression solver} to predict the completion time on several different devices;
\item we apply the performance prediction model to dynamically best-schedule algorithms on heterogeneous platforms based on completion time. Since completion time is a metric, we introduce the chance to easily refine the scheduling policy to take into account data-transfer overhead and sub-optimal scheduling.
\end{itemize}
% \end{abstract}


\section{Introduction}
In the last few years computing has become increasingly heterogeneous, offering a broad portfolio of different parallel devices, ranging from CPUs, through GPUs to APUs\footnote{APU is the term used by some processors brands to refer to a CPU and a GPU integrated into the same die} and coprocessors. 
On the laptop and desktop CPUs market, the recent trend sees embedding a multicore CPU and a GPU on the same chipset \citep{ketan_paranjape_heterogeneous_2014} \footnote{\url{http://www.slideshare.net/PankajSingh137/amd-9th-intlsocconf-presentation}} . From the GPUs point of view we have been heading toward system equipped with multiple cards, exploiting proprietary inter-communication technologies, such as SLI, or effectively doubling the entire GPU architecture on a single card.
Intel recently released an hybrid device called``Xeon Phi'', a coprocessor for cluster nodes and desktop systems, which is becoming quite popular and affordable for a broad audience.
Given this, today's desktop computers are effectively equipped with one or more multicore CPUs, multiple GPUs and possibly highly-parallel coprocessors. 


In this chapter we show how our resource usage characterization and prediction model can be used to schedule parallel computation on heterogeneous platforms. Our approach is device-aware and computation-based, which means it focuses on the specific characteristics of each available device and on the structure of each computation.

\section{Code analysis and feature extraction}

Recently OpenCL \footnote{\url{https://www.khronos.org/opencl/}} has become one of the most popular approaches for heterogeneous parallel programming, thanks to which parallel algorithms can run on CPUs, GPUs and other kind of accelerators with nearly hundred-percent portability.
One of the major OpenCL limitations is the lack of any support to exploit the heterogeneity of a system in a device-aware fashion.

F\# to OpenCL (FSCL) \citep{cocco_fscl:_2015} \footnote{\url{http://fscl.github.io/FSCL.Compiler/}} is a framework for high-level programming and execution on heterogeneous platforms that addresses the problem of raising abstraction over both OpenCL programming, making it possible to program OpenCL kernels from within F\#, while helping to dynamically exploit the heterogeneity of a platform through a transparent, device and computation aware scheduling approach.

The main problem in code analysis for feature extraction is that for most computations the value of a feature depends on the input. 

FSCL statically \footnote{With ``statically'' we mean at kernel compilation time, that is the first time a particular kernel is seen} pre-computes features, analysing the AST of the kernel and building a \emph{finalizer} for each feature, which completes the evaluation of the feature value as soon as the input is known. 

Feature finalizers are computed the first time a kernel is seen and stored in a data-structure for future use (fig. \ref{fig:finalizer-construction}).
Once built, a feature finalizer can be applied to multiple, different sets of kernel arguments to retrieve the matching feature value. Thanks to the static analysis of the kernel AST and the construction of a lambda that generally contains very lightweight code\footnote{For features counting particular construct in a kernel, such as the number of memory accesses, the finalizer code contains only few arithmetic operations}, the overhead of completing feature evaluation at kernel execution time, which corresponds to applying the lambda to the kernel arguments, is mostly irrelevant. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.6\textwidth]{figures/FSCLFinalizerConstructionEvaluation.png}
\end{center}
\caption{Finalizer construction and evaluation}
\label{fig:finalizer-construction}
\end{figure}

Building a feature finalizer for a particular kernel consists in mapping the kernel to a lambda function, preserving the set of parameters but replacing the body.
Figure \ref{fig:finalizer-body-mapping} shows this mapping for a sequential matrix multiplication kernel and a feature that counts the number of accesses to the elements of the input arrays. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/FSCLFinalizerBodyMapping.png}
\end{center}
\caption{Kernel to finalizer mapping for a feature that counts memory reads}
\label{fig:finalizer-body-mapping}
\end{figure}

% The finalizer building process is implemented through a single recursive descent traversing of the kernel body AST, which can be obtained unobtrusively thanks to the F\# quotations mechanism.

This approach combines static analysis of code with actual parameters used at runtime.
For example, the finalizer code that counts the number of arithmetic expression with a dependency on the kernel actual arguments, is shown in figure \ref{fig:finalizer-body-mapping}. 

\section{Prediction model}
\label{sec:FSCL_model}

We will use linear regression as the \emph{solver} for this experiment, as presented in section \ref{sec:linear_regression}.

As discussed in section \ref{sec:regression_limits}, ordinary linear regression is sensitive to outliers in the dependent variable. Completion time is likely to contain outliers, mainly caused by sporadic effects that may not be considered in the model and to the instability of the system where the measurement is performed. Outliers can be introduced also by an heavy task in the system that steals computing resources from the running tests or by driver instability. For this reason, our prediction model employs a robust regression method instead of ordinary regression \citep{hoaglin_exploring_2011,fox_applied_1997}.

The linear model is build running the benchmarks on each available OpenCL device in the system. Each model correlates a set of features to the completion time on a specific device. For each device a linear model is separately built starting from the following data:

\begin{itemize}
\item A set of benchmarks: for each benchmark we consider several cases by varying the input size. Each benchmark case is executed on the device to obtain the corresponding completion time. With respect to the concept of \emph{programs}, as defined in section \ref{seq:definitions}, each benchmark case is considered a different \emph{program}.
\item A set of features: each feature captures a certain aspect of a program. The chosen features are extracted from each benchmark case. The features are here considered as \emph{resources}, as defined in section \ref{seq:definitions}. The completion time is the \emph{target resource}.
\item A matrix $\mathbf{A}$ where each column is a feature and each row is a benchmark case.
\item A vector $ \mathbf{t} $ with the completion times of the benchmark cases on the considered device, in the same order of the cases in $\mathbf{A}$
\end{itemize}

After having defined a set of benchmark cases and features, a matrix $\mathbf{A}$ and a vector $\mathbf{t}$, we build the matrix of the explanatory variables $\mathbf{X}$ from $\mathbf{A}$.

Linear regression assumes homoskedasticity (i.e. constant variance) in the error terms. In particular, the error on a feature should not be correlated to the completion time. In our model we instead expect the errors on features to be affected by such a correlation. To deal with this issue, we employ the Weighted Least Squares method, which is a generalization of ordinary least squares that relaxes this assumption by normalizing the equations using the variance of the completion time. To estimate the standard deviation of the completion time of each benchmark case, we repeated every experiment 100 times.
For our model, we set the dependent variable $\mathbf{y}$ as the component-wise normalization of $\mathbf{t}$ by its standard deviation (i.e. $y[i] = t[i] / \sigma(t[i])$, so that the error can be assumed to be identically distributed on all of the samples. Consistently, we also normalize each row of $\mathbf{A}$ in the same way, as shown in equation \ref{eq:FSCL_X}. 

Finally, we add to the resulting matrix an unary column, normalized like $\mathbf{t}$, which constitutes the intercept of the linear regression. Conceptually, the intercept represents the time needed to start any computation, independently from the specific benchmark case being measured.

\begin{align}
\label{eq:FSCL_y}
\mathbf{y} &=
 \begin{pmatrix}
  \sfrac{t_1}{\sigma(t_1)} \\
  \sfrac{t_2}{\sigma(t_2)} \\
  \vdots \\
  \sfrac{t_m}{\sigma(t_m)}
 \end{pmatrix} \\ \tag*{Normalised regressand}
\end{align}

\begin{align}
\label{eq:FSCL_X}
\mathbf{X} &=
 \begin{pmatrix}
  \sfrac{1}{\sigma(t_1)} & \sfrac{a_{1,1}}{\sigma(t_1)} & \sfrac{a_{1,2}}{\sigma(t_1)} & \cdots & \sfrac{a_{1,n}}{\sigma(t_1)} \\
  \sfrac{1}{\sigma(t_2)} & \sfrac{a_{2,1}}{\sigma(t_2)} & \sfrac{a_{2,2}}{\sigma(t_2)} & \cdots & \sfrac{a_{2,n}}{\sigma(t_2)} \\
  \vdots  & \vdots  & \vdots  & \ddots & \vdots  \\
  \sfrac{1}{\sigma(t_m)} & \sfrac{a_{m,1}}{\sigma(t_m)} & \sfrac{a_{m,2}}{\sigma(t_m)} & \cdots & \sfrac{a_{m,n}}{\sigma(t_m)}
 \end{pmatrix} \\ \tag*{Normalised regressors}
\end{align}

\vspace{0.3cm}

We can now apply $\mathbf{y} $ (as defined in the equation \ref{eq:FSCL_y})  and $\mathbf{X}$ (as defined in the equation \ref{eq:FSCL_X}) to the regression equation (equation \ref{eq:regression} presented in section \ref{sec:linear_regression}). 

The resulting \emph{surrogate} $\mathbf{\beta}$ (the set of regression coefficients) describes the completion time on the considered device in terms of a linear combination of the features. 

Completion time is an unreliable measure, subject to unpredictable errors due to the presence of running processes and tasks in the system during the execution of the measured program. Since we expect the presence of outliers in the data, as already said we employ a robust regression method, which uses the Iteratively Re-weighted Least Squares algorithm \citep{holland_robust_1977} to identify and discard the outliers.

Once $\mathbf{\beta}$ has been calculated, we can use formula \ref{eq:predictor_regression} presented in section \ref{sec:linear_regression} to predict the completion time of a target program (case) on the device considered. In this formula, $\mathbf{x}$ is a vector that contains the same features used in $\mathbf{X}$ but calculated on a \emph{target program}. 

The \emph{solver} is performed independently for each OpenCL device available, building $\mathbf{y}$ in equation \ref{eq:FSCL_y} starting from the completion times of the benchmark cases on the device.

In the context of our benchmarking model, the features extracted at runtime on the OpenCL kernels are the measured \emph{computational resources}; the model is used to characterize the completion time \emph{target resource}, the \emph{surrogate} (the regression coefficients) will express the time taken to complete every unit of reported feature.


\section{Experimental validation}

In order to validate the model, we define a set of relevant features to extract and a set of programs that form the training set. 
Then, we extract the chosen features from each training sample case.
Finally, we build a device model (i.e. set of regression coefficients) independently for each device in the running system.
Given a device $d$, we run each sample case on $d$ to obtain the corresponding completion time. 
With the set of feature values and completion times for all the sample cases, we prepare the model described in section \ref{sec:general_model}, consisting of the matrix of the explanatory variables $\mathbf{X}$ and a dependent vector $\mathbf{y}$, which contains the completion time of the cases on the specific device. We finally apply linear regression to $\mathbf{X}$ and $\mathbf{y}$ to obtain the device model. While feature extraction is performed once, linear regression is repeated for each device in the system.

Once the set of device models has been built, we use it to predict the completion time of a set of testing programs. Each test is also executed to measure its actual completion time. Finally, we calculate the accuracy in selecting the best device for each test in terms of the ratio between the completion time of the estimated fastest device and the measured lowest completion time across the set of devices.

\subsection{Experiment setup}
\label{setup}
To validate the prediction model we setup an heterogeneous system equipped with an APU and a discrete GPU. The APU is a chipset that includes a CPU and a GPU on-die. With ``D-GPU'' we indicate the discrete GPU and with ``I-GPU'' the on-die GPU. The system configuration is reported below.

\begin{itemize}
\item AMD Fusion A10-5800K (CPU with an integrated AMD HD 7660D GPU)
\item AMD HD 7970 (discrete GPU) 
\item 4GB DDR3 Ram - 1333 Mhz
\item Windows 7 64 bit
\end{itemize}

In the definition of the training set, we focus on including computations that stress only specific features and device characteristics with a minimal effect on the others. Since this set of samples constitute the ``basis'' used to predict the completion time of other computations, we want to start from a minimal set of samples (i.e. small basis) capable of describing a possibly wide set of other algorithms and progressively refine it by introducing further samples. 

\begin{description}
\item[Vector addition] This kernel performs an element-wise sum of two vectors, where each work-item sums the elements matching its own global index. Given the very short execution time and the extremely lightweight nature of the computation, this sample allows to focus on the data-transfer time and on the contribution of the work-space size to the completion time. We execute the kernel varying the input size from 1MB up to 128MB with 1MB step.
\item[Matrix multiplication naive] A matrix multiplication kernel where each work-item in a 2D work-space multiplies a row of the first matrix by a column of the second. The first matrix is accessed with a cache-friendly pattern, while the second is accessed with a matrix-width stride that may incur many cache misses. For this reason, matrix multiplication allows to capture the impact of cache misses on completion time.
We run this kernel starting from 64x64 elements matrices up to 2048x2048, with a 64-elements step.
\item[Logistic map] A logistic map performed on each element on an input vector filled with random floating point values. Since each work-item performs one only memory access and many arithmetic operations, this sample captures the impact of floating point operations on the completion time. We execute the kernel varying the input size from 1MB up to 128MB with 1MB step and the number of iterations per-work-item from 1000 to 10000. 
\end{description}

For each training sample we produce 30 cases characterized by different input sizes. Each case is then run 100 times to get the average completion time and the standard deviation.

The selection of the features to extract is related to the aspects stressed by the set of training samples. We consider memory accesses and operations (arithmetic, logic and transcendent) two of the most relevant features to use in order to characterize the completion time. While a given operation takes a fixed same amount of time to complete, memory accesses have a different impact depending on whether the data accessed is in cache or not. For this reason, instead of considering the amount of memory accesses we estimate the number of cache misses. Cache misses are estimated by detecting the size of the data cache and cache line on a specific device\footnote{This information can be retrieved through the OpenCL device-query API or running micro-benchmarks} and by computing the strides of memory accesses in the sample.
A first approximation introduced is considering each array separately as like as each array was stored in a separate, private data cache. At kernel-compilation time we pre-compute the access strides to each (global) array and we count the number of accesses with a certain stride by assessing the total trip-count of the loops containing the memory access operations. A second approximation is introduced by considering the largest stride among the memory accesses in a loop, as if the block of memory between the lowest and the highest addresses was entirely used. 
At kernel-execution time we complete the evaluation of the strides and the relative trip count. These information are then coupled with the cache line and the size of the data cache to estimate the number of cache misses.

Given a certain number of operation and memory accesses per-work-item, the completion time should be affected by the total number of work-items launched. Therefore, we add the work-space size to the set of the computed features. 

\begin{description}
\item[Total number of instructions executed] This feature represents the computation as if all of the operations had the same cost and were executed in a sequential order.
\item[Number of instructions executed per thread]  This feature represents the computation as if all of the work-items could perform operations in a pure parallel fashion.
\item[Cache misses] This feature captures the time spent waiting for memory accesses that do not hit the cache.
\item[Global work-items] This feature corresponds to the amount of work-items spawn to execute a computation.
\item[Kernel-launch overhead] The cost of starting a kernel. Since the value of this features is always $1$ (intercept), linear regression charges it with the part of the completion time that it is not able to properly describe in terms of the other features (non-linear terms).
\end{description}

This set of features should be sufficient to investigate the costs of launching a kernel, the amount of time spent performing arithmetic, logic, or transcendent operations and the overhead of memory accesses.
Several other factors may contribute to the completion time, but we want to start from the coarsest model and progressively refine it as like as for the set of training samples.

\begin{figure*}
\begin{center}
\scriptsize{\input{data/ResidualsGabriele}}
\caption{Fitting residuals}	
\label{fig:ResidualsGabriele}
\end{center}
\end{figure*}

Figure \ref{fig:ResidualsGabriele} shows the residuals of the fitting for both CPU, discrete GPU and integrated GPU. Since we normalize $\mathbf{X}$ by $\mathbf{t}$ (section \ref{sec:FSCL_model}), the residuals are shown as relative values.
The analysis of the residuals reveals the following:

\begin{itemize}
\item Most of the residuals lie near 0, which in general indicates a good fit.
\item Residuals are larger for completion times close to zero. This is expected, because cases that have a very short completion time are more likely to be affected by measurement errors induced by the influence of external and unpredictable effects, such as the presence of other processes running in the system. This also suggests that we should expect a relevant prediction error for programs that complete in a short time.
\end{itemize}

\subsection{Predicting the completion time}

To evaluate the error in predicting the completion time of computations using the models built for the devices in the running platform, we define a set of test samples.

\begin{description}
\item[Sum of matrix rows] This kernel sums the rows of a two-dimensional matrix, producing a vector whose size is equal to the matrix width. Each work-item performs a reduction along a column. 
% Since the stride of memory accesses of a single work-item corresponds to the width of the matrix, this sample helps capture the effect of cache misses due to a sub-optimal usage of cache lines and to cache lines eviction. On GPUs we can also capture the effect of uncoalasced memory reads.
As for all the matrix-based samples, we run this kernel starting from 64x64 elements matrices up to 2048x2048, with a 64-elements step.
\item[Sum of matrix columns] This kernel sums the columns of a two-dimensional matrix, which results in a vector matching the matrix height. Each work-item performs a reduction along a row. 
% Since memory accesses are contiguous this sample helps capture the effect of of cache-friendly memory access pattern. 
%\item[Matrix multiplication advanced (tiled)] A more complex matrix multiplication algorithm which employs local memory to prevent multiple accesses to the same elements of the global memory.
% With this sample, especially compared to naive matrix multiplication, we can capture the benefit of fewer global memory accesses while considering the effects of accessing local memory.
\item[Sobel filtering] A Sobel 3x3 filtering algorithm on a 2D matrix.
\item[Convolution filtering] A generalization of Sobel filtering, with a generic input filter varying in size from 3x3 to 19x19 elements.
 % With this kernel we can captures the changes in best-device selection based on the size of the rectangular block of memory accessed by a single thread or work-group.
\item[Matrix transpose naive] Matrix transpose performed by making each work-item to transpose a single element of the matrix. 
% On CPUs, this sample captures the effect of strided writes to cache/memory, while on GPUs it stresses uncoalesced writes to memory.
\item[Matrix transpose advanced] Matrix transpose that exploits local memory to make successive work-items to read and write successive matrix elements, enabling coalescing and reducing channel/bank conflicts. 
% On CPUs, this sample captures the effect of sequential writes to cache/memory, while on GPUs it stresses coalesced writes.
\end{description}

In the set of test samples we also include the training samples used to build the device models. Using a set of samples to predict themselves provides, in addition to the set of residuals, an insight on the quality of the fitting. 

In figures \ref{fig:Predictions1} and \ref{fig:Predictions2} we compare the measured completion time and the estimated completion time for each test sample by varying the input size. The left column reports the measured completion time on CPU, discrete GPU and integrated GPU, while the right column shows the predicted completion time on the same devices. To be easily compared with each other, both the completion times are expressed in the same logarithmic scale.

\begin{figure}
\begin{center}
\scriptsize{\input{data/Predictions1}}
\caption{Measured and predicted completion time}	
\label{fig:Predictions1}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\scriptsize{\input{data/Predictions2}}
\caption{Measured and predicted completion time}	
\label{fig:Predictions2}
\end{center}
\end{figure}

The first three samples illustrated in figures \ref{fig:Predictions1} and \ref{fig:Predictions2} are the training samples used to build the device models. As expected, the predicted completion times of these programs is very close to the measured completion times.

The overall behaviour of most of the samples shows a simple relation between the input size and the completion time. Two noticeable exceptions are \emph{MatMulNaive} and \emph{SumRows}, whose non-monotonicity is well above measurement noise. A possible explanation for the evident spikes in the graphs is the eviction of many cache lines when accessing memory with a stride of 4KB, 6KB and 8KB. These spikes are correctly estimated thanks to the the cache-miss-estimation feature.
The same behaviour is correctly predicted in \emph{SumRows}, even though it does not belong to the set of training samples. This is consistent with our expectation: the aforementioned spikes are indeed caused by a particularly aggressive cache eviction.

The prediction of CPU completion times is generally more accurate than the prediction of GPUs. This is mainly due to the fact that the cache-miss-estimation feature models the cost of accessing memory from the CPU with sufficient accuracy, while it does not properly fit the GPUs, where the cost of memory accesses depends on the access pattern in a different way. To accurately predict the completion time on GPUs, additional information must be retrieved and analysed, such as coalescing in reading and writing memory, ALU fetch ratio\footnote{The occupancy of GPU ALUs during the time a memory request is served} and channel/bank conflicts.
In addition, information about the usage of LDS (Local Data Share) memory are needed to improve the prediction of \emph{TransposeAdvanced}.

\subsection{Best-device prediction}

The quality of completion-time prediction is only partially related to the quality of best-device guessing. A very precise prediction of the measured completion times leads to a reliable best-device guess, but errors that may affect the completion time prediction not necessarily imply a specific error in guessing the most efficient device. It is sufficient to consider a linear model that overestimates the completion time of a constant factor independently from the input size and the device. In such a case, the quality of the completion time estimation is low, but the one of best-device guessing may be instead very high. 
% For example, we may estimate a completion time of 10 and 20 seconds for two different devices, while the respective measured completion time is 20 and 30 seconds. While this implies an error in completion time prediction, the device predicted to be the most efficient is the first one, which corresponds to the real best device between the two considered.

For this reason, we evaluate the accuracy of best-device prediction. Figure \ref{fig:DensityGabriele} shows the frequency of the relative prediction accuracy, measured as the ratio between the completion time of the device predicted by our algorithm and the actual optimal device. We also show the geometric mean of the relative accuracy (red line). A geometric mean equal to $1$ corresponds to situations where the algorithm always predicts the correct device. When the geometric mean is near to $1$ the algorithm does not always predict the best device across the input sizes, but the performance degradation is low, hence the error is small. Higher values of the geometric mean correspond to situations where the difference between the predicted device and the best device is relevant. For example, a geometric mean close to $2$ means that the predicted device usually takes twice the time than the best device. 

\begin{figure}
\begin{center}
\scriptsize{\input{data/DensityGabriele}}
\caption{Best device prediction relative accuracy}	
\label{fig:DensityGabriele}
\end{center}
\end{figure}

The programs in the basis (\emph{VectorAdd}, \emph{MatMulNaive} and \emph{Logistic}) show an ideal behaviour, with a geometric mean very close to $1$, or exactly $1$. \emph{Convolution} also shows an ideal prediction error. \emph{SumRows}, \emph{Sobel} and \emph{TransposeAdvanced} have a very low geometric mean, with the vast majority of the predictions being accurate.  \emph{Transpose} is plotted on a different x scale, because it is the only algorithm for which we obtain prediction errors larger than $2$. The geometric mean for this test sample is therefore high (close to $2$), even though most of the predictions were accurate (the frequency of the bin relative to $2$ is considerably higher than the others combined together). 

\subsection{Interpretation of the regression coefficients}
\label{section:interpretation_regression}
Each linear regression coefficient can be easily interpreted as the time needed to perform one unit of the corresponding feature. As explained in section \ref{seq:definitions} a \emph{valid} feature ``counts'' the occurrences of a certain phenomenon, while the corresponding coefficient quantifies the time needed for each occurrence. The linear regression applied to the training samples creates a linear model for each device, with a coefficient for each feature. In our experiments, the coefficient for the \emph{total number of instructions executed} feature for the CPU is $0.24e-9$. If we consider this coefficient, the value of the corresponding feature and the completion time on the CPU, we can estimate the number of operations per second of the CPU. The particular value of the regression coefficient for the total number of instructions leads to an estimated $4.2e9$ operations per second, which means $2.1e9$ operations per second on each of the two CPU cores. This number is very close to the declared operating frequency of the CPU, between 3.8 and 4.2 GHz. A similar evaluation can be done for the GPUs.
% , where the regression coefficient for aformentioned features is $6.1e-12$. The discrete GPU is equipped with 32 SIMD compute units, each of which can perform operations on 16 values at once. The $163e9$ operations per second obtained from the regression coefficient, the feature value and the completion time implies about $320e6$ values processed per second on each SIMD lane. This is quite similar to the actual GPU clock of 925 MHz.

Given that we count high-level instructions that may not have a one-to-one match with low level executable code, we expected a larger estimation error. Moreover, not all of the low-level instructions require exactly a single clock cycle and many other relevant behaviours are not modelled, such as super-scalarity and the effect of branches. Nonetheless, the coefficients and their corresponding physical values are surprisingly close.
This shows that our method can accomplish a twofold purpose: predict the completion time of computations running on heterogeneous platforms and estimate the characteristics of the available devices.


\subsection{Limits}

As shown in the previous sections, linear regression can successfully describe and predict complex behaviours of hardware and software, but only if the following conditions are met:

\begin{itemize}
\item The features must provide an adequate model of the costs incurred by target programs. The model can be high-level, i.e. it does not need to explicitly account for compiler optimization or runtime details, but it must capture all of the aspects that characterize the computation. For example, in our experiments we can successfully predict the CPU cache miss behaviour of \emph{Sum of matrix rows} because a similar behaviour is shown by \emph{Matrix multiplication naive}, that is present in the basis. Conversely, with our set of features we would not have been able to evaluate I/O costs, as none of the features was related to them.

\item The basis must exercise the features independently, to avoid multicollinearity between the input data. In our experiments, the \emph{Logistic map} kernel was designed so that the number of work items and the amount of arithmetic operations could be chosen independently. In the other kernels we used, both the number of work items and the number of operations to be performed depend on the size of the data.
\end{itemize}

If none of the features captures an important behaviour of the target program, predictions will contain a large error. The opposite is also usually true, that is if predictions have a large error the target program is characterized by a relevant uncaught behaviour. This is interesting because it helps understanding what is worth investigating when trying to describe a target program. 

If two algorithms are similar in every feature we measure but they differ on an important aspect that we do not measure, then we will not be able to capture their difference in the model, and the predictions will not be able to discriminate between them.

The prediction model can only capture linear relationships between completion time and features. Only features that ``count'' events that are relevant for the completion time will affect the prediction.

Static code analysis shows some limitations in case of complex control flows, such as loops where the trip count depends on the content of an array or while-loops conditioned by multiple variables.
Some advanced techniques, like LLVM branch probability estimation and loop-unrolling algorithms, may be employed in the future to widen the range of computations that can be covered. 

\section{Conclusions}

In this chapter we presented a simple but effective completion time prediction approach that uses the \emph{linear regression solver} to create a model for each device exposed by arbitrary heterogeneous platforms. We evaluated the quality of the approach to predict the completion time of an heterogeneous set of devices and the effectiveness in determining the best device where to schedule a kernel.

The prediction accuracy of our model has been tested against more complex models, like the one described by \citet{wen_smart_2014}, based on SVM, and despite its simplicity, it resulted more accurate. Following Occam's Razor, the simpler model should be chosen over a complex one. Moreover, the regression coefficients found by the linear model have an intuitive physical explanation, as shown in section \ref{section:interpretation_regression}.




