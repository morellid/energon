%!TEX root = test.tex

\part{Benchmarking Model}

In chapter \ref{chapter:high_level_model} we define a high level approach to a generic and unified of computational systems characterization model.
Our model creates an analytic representation, called \emph{surrogate}, of the entity we are interested in modelling (a \emph{target program} or a \emph{target resource}), from measurements of \emph{resource} consumption of \emph{benchmarks} running on known hardware. 
The \emph{target program} is expressed in terms of the \emph{benchmarks}, the \emph{target resource} in terms of the other \emph{resources}. In this chapter we introduce the concept of \emph{computational pattern}.

In chapter \ref{chapter:solvers} we discuss the different algorithms
that can be used to extract \emph{surrogates} and to create
predictions. Linear Regression is a simple model that has an intuitive
interpretation of the \emph{surrogate}, yet offering good predictive
capabilities, as show in chapters \ref{chapter:CPUSPEC} and
\ref{chapter:OpenFOAM}. Linear Regression assumes that the
\emph{target program} can be expressed as a linear combination of
\emph{benchmarks} (or the \emph{target resource} in terms of
\emph{resources}). This can be explained using \emph{computational
  patterns}.



In chapter \ref{chapter:experimental_complexity} we introduce the
concept of \emph{experimental complexity} as an empirical method to
describe how the resource consumption of an algorithm changes as the
input size grows. In this chapter we also describe how to use our
model to predict bottlenecks. 
In section \ref{chapter:compositionality} we discuss the
compositionality of the model. We show how it can be explained in
terms of \emph{computational patterns}. 
In section \ref{chapter:single_number} we discuss the algebraic
reasons why a single number can not be a representative
\emph{surrogate} for a \emph{program} or a \emph{computational
  environment} performance. This explains why simple uni-dimensional
metrics like FLOPS fail to describe performances.

In chapter \ref{chapter:energy_model} we introduce an energetic model,
a set of equations that show the relation between completion time,
instant power and energy consumption. This model is capable of
describing both sequential and parallel concurrent computations. 




\subimport{.}{high_level_model}

\subimport{.}{solvers}


\chapter{Experimental complexity of software}
\label{chapter:experimental_complexity}

In the previous chapters we have considered applying different inputs to the same algorithm as different \emph{programs}. In this chapter we extend the model adding the concept of \emph{experimental complexity}, that describes the evolution of the \emph{surrogate} of a program as the input size changes. 

\section{Characterization through the surrogate}
\sectionmark{Characterization}

Studying how the \emph{surrogate} $\beta$ of a \emph{program} changes with the input size can reveal the nature of a computation and can be used to predict what bottleneck the program will suffer as the input becomes too large.

Consider the following example. The basis is composed by two programs:
\begin{itemize}
\item $p_\text{cpu}$ a CPU intensive program, it does not use memory
\item $p_\text{mem}$ a memory intensive program, it does not perform CPU intensive tasks
\end{itemize}
and using them to characterize a \emph{target program} $p$ where the memory usage grows linearly and the CPU usage polynomially. Imagine also using performance counters closely related to CPU and memory usage, like ``instructions'' and ``cache-misses'', as our \emph{resources}. Imagine the \emph{resources} usage of the basis and the target program to be what reported in table \ref{tab:complexity_example}.

\begin{table}[h]
\begin{tabular}{l | l l}
program & cache-misses & instructions  \\
\hline
$p_\text{cpu}$ & 1 & 1000 \\
$p_\text{mem}$ & 100 & 100 \\
\hline
$p(\Vert x \Vert = 10 )$ & 501   & 1000 \\
$p(\Vert x \Vert = 20)$ & 1004   & 4000 \\
$p(\Vert x \Vert = 30)$ & 1509   & 9000 \\
$p(\Vert x \Vert = 40)$ & 2016  & 16000 \\
$p(\Vert x \Vert = 50)$ & 2525  & 25000 \\
$p(\Vert x \Vert = 60)$ & 3036  & 36000 \\
$p(\Vert x \Vert = 70)$ & 3549  & 49000 \\
$p(\Vert x \Vert = 80)$ & 4064  & 64000 \\
$p(\Vert x \Vert = 90)$ & 4581  & 81000 \\
$p(\Vert x \Vert = 100)$ & 5100 & 100000 \\
\hline
\end{tabular}
\label{tab:complexity_example}
\caption{Example of instructions and cache-miss usage for the basis and the target program}
\end{table}

Because $p_\text{cpu}$ and $p_\text{mem}$ are close to what we could call ``CPU usage'' and the ``memory usage'' \emph{computational patterns}, and because the chosen \emph{resources} accurately measure their usage, our model can characterize the evolution of the surrogate in an intuitive fashion. Figure \ref{fig:complexity_example} shows the evolution of the components of $\beta$ extracted by our model from the data in table \ref{tab:complexity_example}, using the performance counters as \emph{resources}, $p_\text{cpu}$ and $p_\text{mem}$ as the basis, and creating a surrogate for every input size of the \emph{target program} $p$. Is immediately visible that $\beta_\text{mem}$ grows linearly with the input size, while $\beta_\text{cpu}$ grows polynomially, revealing the different behaviour of the \emph{target program} with respect to the \emph{computational patterns} of interest. 

\begin{figure*}
\begin{center}
\scriptsize{\input{R/complexity_example}}
\caption{Example of evolution of surrogate components as the input size grows}
\label{fig:complexity_example}
\end{center}
\end{figure*}

This example is an oversimplification of reality. The available data is usually not as auto-explicative as in table \ref{tab:complexity_example} (that would allow us to characterize the \emph{target program}'s behaviour even without looking at the \emph{surrogate}). In section \ref{seq:experiments_complexity} we show experiments with real algorithms, starting from measures that do not reveal the nature of the \emph{target program} until we analyse it through the evolution of the \emph{surrogate}.

%\section{Self similarity of programs}

%In section \ref{sec:similarity} we introduced the concept of \emph{similarity} between programs based on the cosine distance between the measures of two \emph{programs}. We can apply the same similarity distance to the \emph{surrogates} of programs

%TODO demonstrate that programs similarity can be applied to surrogates


\section{Definition of experimental complexity}
\sectionmark{Experimental complexity}


In the previous section we showed how the evolution of the \emph{surrogate} can reveal how the behaviour of a \emph{program} changes as the input size grows. In this section we formalize that intuition, introducing the concept of \emph{experimental complexity of software}, as a function that describes the relation between the \emph{surrogate} and the size of the input to the \emph{target program}.

\begin{definition}[Experimental complexity of a program]
The experimental complexity $\xi$ of a program is a vector valued function that maps the size of the input data $\Vert x \Vert$ of a program into its corresponding surrogate $\beta$.
\label{def:experimental_complexity}
\end{definition}

Equation \ref{eq:experimental_complexity} shows that the vector valued function $\mathbf{\xi}$ is defined as a vector of functions $\xi_i$, each describing how the $i^{\text{th}}$ coefficient of $\beta$ changes as the input of the \emph{program} grows.

% TODO don't use x !

\begin{align}
\mathbf{\xi}(\Vert x \Vert) &= 
 \begin{pmatrix}
  \xi_1(\Vert x \Vert) \\
  \xi_2(\Vert x \Vert) \\
  \vdots  \\
  \xi_n(\Vert x \Vert) \\
 \end{pmatrix} = 
 \begin{pmatrix}
  \beta_{1_x} \\
  \beta_{2_x} \\
  \vdots  \\
  \beta_{n_x} \\
 \end{pmatrix} = \beta_x \label{eq:experimental_complexity} \\ 
 \tag*{Experimental complexity}
\end{align}

$\xi_i$ can be found using curve fitting on a predefined set of functions. Consider the example of the previous section, $\xi_\text{cpu}$ and $\xi_\text{mem}$ need to fit the curves shown in figure \ref{def:experimental_complexity}. We define a set of possible interesting functions:
\begin{itemize}
\item $f_\text{lin} = \Vert x \Vert $
\item $f_\text{log} = \log(\Vert x \Vert )$
\item $f_\text{square} = \Vert x \Vert^2$
\item $f_\text{cubic} = \Vert x \Vert^3$
\item $f_\text{exp} = e^{\Vert x \Vert }$
\item \dots
\end{itemize}
We then generate a vector for every $f_i$, using the input size as the argument of $f_i$. At this point we run linear regression using the generated vectors as independent variables, and the values of $\beta_\text{mem}$ as dependent variable.
The regression coefficient will show what combination of $f_i$ best fits the values of $\beta_\text{mem}$. For example in our case all the coefficients are extremely small except for $f_\text{lin}$, that has a value of 0.5, showing that the \emph{target program} is linear with respect to the \emph{benchmark} that expresses memory access.

We then repeat using the values of $\beta_\text{cpu}$ as dependent variables, and find that all the coefficient are close to zero except for $f_\text{square}$ that has a coefficient of 0.01, showing that the \emph{target program} is quadratic with respect to the \emph{benchmark} that expresses the CPU intensive task.

The experimental complexity can be used to interpolate (or even extrapolate) resource consumption of the \emph{target program} for input sizes for which we have no measures for any \emph{resource}, and we can not therefore apply the \emph{solver} to build a \emph{surrogate}. In the previous example we could be interested in predicting the ``instructions'' and ``cache-misses'' values for an input size of 110. We can calculate $\beta(\Vert x \Vert = 110)$ as shown in equation \ref{eq:experimental_example_beta}, then predict the ``instructions'' and 
``cache-misses'' as shown in equation \ref{eq:experimental_example_prediction}.

\begin{align}
\beta(\Vert x \Vert = 110) &=
\mathbf{\xi}(\Vert x \Vert) = \nonumber \\ &=
 \begin{pmatrix}
  \xi_\text{mem}(110) \\
  \xi_\text{cpu}(110) \\
 \end{pmatrix} = \nonumber \\ &=
 \begin{pmatrix}
  0.5 * 110 \\
  0.01 * 110^2 \\
 \end{pmatrix} = \nonumber \\ &=
 \begin{pmatrix}
  55 \\
  121 \\
 \end{pmatrix} 
\label{eq:experimental_example_beta}
\end{align}

\begin{align}
 \begin{pmatrix}
  \mu_\text{cache-misses}(\Vert x \Vert = 110) \\
  \mu_\text{instructions}(\Vert x \Vert = 110) \\
 \end{pmatrix} &= 
 \begin{pmatrix}
 p_\text{mem}^T & p_\text{cpu}^T\\
 \end{pmatrix} \beta(\Vert x \Vert = 110) = \nonumber \\ &=
 \begin{pmatrix}
  1210 \\
  5621 \\
 \end{pmatrix} 
\label{eq:experimental_example_prediction}
\end{align}

%TODO verifica predizione!

\section{Relation with computational complexity}

Unlike theoretical computational complexity approaches, such as the widely used Big-O notation, \emph{experimental complexity} is an empirical metric, therefore it depends on the measures available upon model creation.
Like other empirical analysis of algorithms performance, \emph{experimental complexity} attempts to describe the behaviour of \emph{programs} with respect to a set \emph{computational resources} of interest. 
The limitation of empirical approaches is typically the inability do abstract the characterization of the software from the hardware where the program is measured. However, as shown in the previous chapters, our approach is capable of combining information coming from different hardware and resource usage measures, isolating the underlying structure of the \emph{target program}. 
The advantage of empirical approaches is the fact that characterizations are directly applicable to real-world scenarios, like scheduling and resource allocation, whereas theoretical approaches are more suitable to the study asymptotic behaviour, the orders of growth, ignoring the constant factors. In practice, however, constant factors are important, because hardware has limited resources.

Our approach describes the \emph{programs} in terms of features, relying on regression to fit the features to actual measures. A similar approach has been recently proposed by \citet{goldsmith_measuring_2007}, based on basic blocks extraction and clustering. Our model is considerably simpler, because it looks at programs as black boxes, which makes it also easily portable.

\section{Bottlenecks}
\label{seq:bottlenecks}

% TODO: definition of thrashing as a general problem, mainly related to page faulting, but in general to bottlenecks

The definition of measure requires countable additivity if the
resource being measured is not shared by two programs, or countable
sub-additivity if they share part of it. It could also happen that the
measure of the sum of two programs is larger than the sum of the
measures of the programs taken individually. This can happen if
running the programs simultaneously results in bottleneck
e.g. thrashing the system.

We can consider the combination of programs as another program. For
example if we want to run program $p_1$ and program $p_2$, we can
define program $p_{1,2}$ as their combination:
$p_{1,2}=\{p_1,p_2\}$. Our model will still apply. The discussion
about bottlenecks caused by the simultaneous execution of programs can
be reduced to the more general discussion of bottleneck caused by a
program on a computational environment.

Bottlenecks happen when the demand for a computational resource is
higher than the computational environment can provide. Usually the
operating system will serialize the requests, and the computation will
have to wait for the resource to become available. E.g. if a program
uses more memory than physically available, the system will start
swapping, and most of the computational time will be spent waiting for
data to be exchanged between RAM and disk. When a bottleneck
occurs the structure of the computation changes, sometimes
dramatically. 
%This change can be analysed using the experimental complexity of software: when the behaviour of the program suddenly changes we are very likely to be in presence of a bottleneck. Once the experimental complexity of a program has been assessed, we can identify bottlenecks looking at anomalies in the measurements (when a measure significantly deviates from the expected behaviour, it could mean that the process is waiting for a resource to become available).

\subsection{Grace area}

The \emph{grace area} of a \emph{computational environment} is the
combination and amount of \emph{resources} that is safe to consume on that
particular environment without occurring in significant
bottlenecks. When a \emph{program} is outside the \emph{grace area} its
\emph{experimental computational complexity} on that \emph{computational
environment} changes, and the sub-additivity property does not hold. 

In the \emph{resource} measurement consumption space, restricted to \emph{resource}
of a particular \emph{computational environment} only, the \emph{grace area} is the
polytope where \emph{programs} do not thrash that machine. 

Every machine will have different bottleneck conditions, therefore
different \emph{grace areas}. 

Let us now consider a set of identical CPU bound
benchmarks. If we execute them simultaneously, sub-additivity will hold
only as long as there are available processors. Imagine a
computational environment with $n$ processors. Because the benchmarks
are CPU bound, then can run each on a separate processor in
parallel. The completion time of the benchmark defined as the
combination of the first $n$ benchmarks $p_{1 \dotso n} =
\bigcup_{i=1}^n p_i $ will be equal to $t(p_{1 \dotso n}) =
\max(t(p_1), \dotsc, t(p_n)) = t(p_1)$. Subadditivity holds, because
$t(p_{1 \dotso n}) \leq \sum_{i=1}^n t(p_i) $. If we now run $p_{1
  \dotso n} $ ns $p_{n+1}$ simultaneously, we will have more active
CPU bound programs than processors, and some time will be spent
switching context. The completion time of $t(p_{1 \dotso n}+p_{n+1})
>t(p_{1 \dotso n})+t(p_{n+1}) $, violating the subadditivity
constrain. A resource is therefore valid only if is not acting as a bottleneck.


% \subsection{Bottleneck computational patterns}

% TODO we're renouncing countable subadditivity, is it legit?

% Bottlenecks can be seen as computational patterns. A program
% specifically designed to thrash a machine could be used as a
% benchmark. 

% The classical thrashing behaviour can be described as the
% computational pattern that continuously triggers page faults.

% Designing benchmarks that will cap specific computational resources,
% we can capture the bottleneck computational patterns. The \emph{grace
%   area} can therefore be described in the \emph{surrogate space}, the
% vectorial space of the surrogate vectors.


% It is interesting to model the grace area of a
% computational environment, and predict if a program will lie inside or
% outside of it. 

% Let's consider the case of a scheduler that can choose
% whether to execute the program $p$ on the computational environment
% $A$ or $B$. If the scheduler predicts $p$ to be outside the \emph{grace
%   area} of $A$ and inside the \emph{grace area} of $B$, it should
% probably schedule $p$ on $A$, even if $B$ has better performances for
% other input sizes.



% TODO experimental validation

% TODO can you predict thrashing? if you know the thrashing points of
% the benchmarks of the basis, can you find the point in the ? space
% that will let us  predict when the target program will thrash? 

\section{Compositionality of \emph{Surrogates} and \emph{Experimental complexity}}
\sectionmark{Compositionality}
\label{chapter:compositionality}

In this section we discuss the compositional properties of the \emph{surrogate} $\beta$ and the \emph{experimental complexity} $\xi$. 

In section \ref{seq:experiments_complexity} we verify the compositionality of $\beta$ and $\xi$ using simple toy-benchmarks and sorting algorithms, to compare the experimental results with theoretical time complexity analysis.

\subsection{Linear composition of \emph{surrogates} and \emph{experimental complexity}}

By definition, any linear composition of the basis $\mathbf{X}$ can be expressed by the \emph{linear regression solver} using a \emph{surrogate} $\beta$. 
Imagine a \emph{program} $c$ being defined as the linear combination of two programs $a$ and $b$: $c$ consists of $k_a$ invocations of the \emph{program} $a$ and $k_b$ invocations of \emph{program} $b$. The \emph{resource} usage of $c$ will also be a linear combination of the \emph{resource} usage of $a$ and $b$, as shown in equation \ref{eq:compositionality_programs}.

\begin{align}
\label{eq:compositionality_programs}
p_{c} &= k_a p_{a} + k_b p_{b} \nonumber \\ 
\mu_{p_c} &= k_a \mu_{p_a} + k_b \mu_{p_b} \\ \tag*{$c$ is a linear combination of $a$ and $b$}
\end{align}

The \emph{surrogate} $\beta_c$ of the \emph{program} $c$ can be written as a linear combination of the \emph{surrogates} $\beta_a$ and $\beta_b$ of \emph{programs} $a$ and $b$, as shown in equation \ref{eq:compositionality_beta_ab3}:
\begin{itemize}
\item equation \ref{eq:compositionality_beta_yc} follows from equation \ref{eq:compositionality_programs}, because $y_c$ is composed of measures of the \emph{program} $c$;
\item equations \ref{eq:compositionality_beta_a}, \ref{eq:compositionality_beta_b}, and \ref{eq:compositionality_beta_c} simply report the formula of the estimation of $\beta_a$, $\beta_b$, and $\beta_c$ using linear regression;
\item equation \ref{eq:compositionality_beta_ab} uses equation \ref{eq:compositionality_beta_yc} to substitute $y_c$ with $y_a+y_b$
\item equation \ref{eq:compositionality_beta_ab2} uses the distributive property of matrices
\item equation \ref{eq:compositionality_beta_ab3} simplifies equation \ref{eq:compositionality_beta_ab2}, using equations \ref{eq:compositionality_beta_a} and \ref{eq:compositionality_beta_b}, showing that the estimation of the \emph{surrogate} of a linear combination of programs is the linear combination of the \emph{surrogates}.
\end{itemize}
 

\begin{align}
\mathbf{y_c} &= k_a \mathbf{y_a} + k_b \mathbf{y_b} \label{eq:compositionality_beta_yc} \\
\hat{\beta_a} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_a} \label{eq:compositionality_beta_a} \\
\hat{\beta_b} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_b} \label{eq:compositionality_beta_b} \\
\hat{\beta_c} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_c} \label{eq:compositionality_beta_c} \\
\hat{\beta_c} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T (k_a \mathbf{y_a} + k_b \mathbf{y_b}) \label{eq:compositionality_beta_ab} \\
\hat{\beta_c} &= k_a (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_a} + k_b (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_b} \label{eq:compositionality_beta_ab2} \\
\hat{\beta_c} &= k_a \hat{\beta_a} + k_b \hat{\beta_b} \label{eq:compositionality_beta_ab3} \\ \tag*{Compositionality of \emph{surrogates}}
\end{align}


The compositional property of $\beta$ does not depend on the input size. \emph{Surrogates} will therefore be compositional as the input size grows. As shown in equations \ref{eq:compositionality_xi_1} to \ref{eq:compositionality_xi_4}, this also makes the \emph{experimental complexity} $\xi$ compositional: equation \ref{eq:compositionality_xi_1} and \ref{eq:compositionality_xi_2} simply state that applying $\Vert x \Vert$ to $\xi$, the corresponding $\beta$ can be found; equation \ref{eq:compositionality_xi_3} states that \emph{surrogates} are compositional, independently of the input size; substituting equations \ref{eq:compositionality_xi_1} and \ref{eq:compositionality_xi_2} into equation \ref{eq:compositionality_xi_3} we can obtain a valid $\xi_c$, independently of the input size.

\begin{align}
\xi_a(\Vert x \Vert) &= \beta_{a(x)} \label{eq:compositionality_xi_1} \\
\xi_b(\Vert x \Vert) &= \beta_{b(x)} \label{eq:compositionality_xi_2} \\
\beta_{c(x)} &= k_a \beta_{a(x)} + k_b \beta_{b(x)} \quad \forall x \label{eq:compositionality_xi_3} \\
\xi_c(\Vert x \Vert) &= \beta_{c(x)} = k_a \xi_a(\Vert x \Vert) + k_b \xi_b(\Vert x \Vert) \quad \forall x \label{eq:compositionality_xi_4} \\ \tag*{Compositionality of \emph{experimental complexity}}
\end{align}

% TODO this is not a demonstration, I should demonstrate the linear regression will find \beta_c = k_a \beta_a + k_b \beta_b

\subsection{Function composition}

In this section we explore how the composition of functions reflects on the \emph{surrogates} and the \emph{experimental complexity}. 
Imagine a \emph{program} $c$ composed of an algorithm $a$ that in turn calls another algorithm $b$, following a certain function $f(x)$. For example the algorithm $a$ could call $b$ on every element of the input, in this case $f(x)=\Vert x \Vert$, or it could call $b$ on every element visited during a binary search, in this case $f(x)=\log_2 \Vert x \Vert$. 

Assuming linearity in the composition of programs, as discussed in the previous section, the resource consumption of $c$ will follow equation \ref{eq:compositionality_functions}.

\begin{align}
\label{eq:compositionality_functions}
p_{c} &= p_{a} + f(x) p_{b} \nonumber \\ 
\mu_{p_c} &=  \mu_{p_a} + f(x) \mu_{p_b} \\ \tag*{$c$ is  $a \circ b$}
\end{align}

Equations \ref{eq:compositionality_function_y_c} to \ref{eq:compositionality_function_c4} show that, like in the previous section, the \emph{surrogates} compose in the same way as the measures of the \emph{programs}. Equation \ref{eq:compositionality_functions_xi} shows that, because \ref{eq:compositionality_function_c4} holds for any input size, \emph{computational complexity} is compositional as well.

\begin{align}
\mathbf{y_c} &= \mathbf{y_a} + f(x) \mathbf{y_b} \label{eq:compositionality_function_y_c} \\
\hat{\beta_c} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_c} \label{eq:compositionality_function_c} \\
\hat{\beta_c} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T (\mathbf{y_a} + f(x) \mathbf{y_b}) \label{eq:compositionality_function_c2} \\
\hat{\beta_c} &= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_a} + f(x) (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y_b} \label{eq:compositionality_function_c3} \\
\hat{\beta_c} &= \hat{\beta_a} + f(x) \hat{\beta_b} \label{eq:compositionality_function_c4} \\ 
\xi_c(\Vert x \Vert) &= \beta_{c(x)} = \beta_{a(x)} + f(x) \beta_{b(x)} = \xi_a(\Vert x \Vert) + f(x) \xi_b(\Vert x \Vert) \quad \forall x \label{eq:compositionality_functions_xi} \\ \tag*{Compositionality of functions}
\end{align}



\section{A single number is not enough}
\label{chapter:single_number}

As already noted by \citet{smith_characterizing_1988}, characterizing performance with a single number is error prone. Nonetheless, most of the benchmarks, including the most used, represent performance with a uni-dimensional metric. This is seen as ``necessary evil'' \citep{smith_characterizing_1988}, to make the characterization easy to interpret by the user. As discussed in section \ref{sec:evolution_benchmarking}, especially referring to the ``war of the benchmark means'' \citep{mashey_war_2004}, the choice of the best single number metric is still an unresolved issue. Over the years the benchmarking community has used several different single number metrics (like MIPS, GFLOPS, etc.), leading to controversial results.

Using a single number is equivalent to either using a single \emph{resource} (without any further characterization), or to using our model with a single \emph{program} as the basis.

In this chapter we discuss why using a single metric to characterize software or hardware is inadequate and inevitably leads to significant error.

Consider figure \ref{fig:complexity_points}, where we show three \emph{programs} in the \emph{resource} space (the axes are the measure of the \emph{resource} consumption).
If only $p_1$ is used as the basis to represent the other \emph{programs}, only the \emph{programs} with a \emph{resource} consumption multiple of $p_1$ will be modelled without error. 
Geometrically it means that, if the basis is only composed of $p_1$, only the points that lie on the line that passes through $p_1$ and the origin (because of the \emph{null empty set} property of \emph{measures}) can be expressed without error. The \emph{program} $p_2$ lies on the line, it can therefore be modelled with $\beta=1.5$, because $p_1 \beta = p_2$. However, \emph{program} $p_3$ does not lie on the same line as $p_1$, therefore it can not be modelled by any value of $\beta$ without significant error.

This condition similarly applies to \emph{resource spaces} of higher dimensions: only the points that lies on the line that passes through the origin and the single \emph{program} in the basis can be modelled without significant error. 

Using a single dimensional metric different than a program's resource consumption is still equivalent to trying to find a line in the resource consumption space to express all possible programs.

\begin{figure*}
\input{R/complexity_points}
\caption{Example of points in the resource space}
\label{fig:complexity_points}
\end{figure*}

In the rest of the chapter we analyse under which conditions this will happen, in terms of \emph{computational patterns}. For simplicity, we will limit the discussion to 2 \emph{resources}, and to metrics that use the resource consumption of programs.

Equations \ref{eq:measures_compt_patter_a} and \ref{eq:measures_compt_patter_b} show the generic \emph{resource} consumption of 2 \emph{computational patterns} $a$ and $b$, for 2 \emph{resources}. For example, the \emph{resources} could be completion time on two different machines, \emph{computational pattern} $a$ could be pure CPU arithmetic instructions, and $b$ memory usage.

\begin{align}
a &= \left( 
\begin{array}{c}
\mu_\text{a1} \\
\mu_\text{a2} \end{array} \right) \label{eq:measures_compt_patter_a} \\
b &= \left( 
\begin{array}{c}
\mu_\text{b1} \\
\mu_\text{b2} \end{array} \right) \label{eq:measures_compt_patter_b} \\ 
\tag*{\emph{Resource} consumption of \emph{computational patterns}}
\end{align}

Equations \ref{eq:measures_prog_comp_1} and \ref{eq:measures_prog_comp_2} show the \emph{programs} $p_1$ and $p_2$ are linear combination of the \emph{computational patterns} $a$ and $b$.

\begin{align}
p_1 &= k_\text{a1} a + k_\text{b1} b = 
k_\text{a1} \left( 
\begin{array}{c}
\mu_\text{a1} \\
\mu_\text{a2} \end{array} \right) 
+ k_\text{b1} \left( 
\begin{array}{c}
\mu_\text{b1} \\
\mu_\text{b2} \end{array} \right) =  \left( 
\begin{array}{c}
k_\text{a1} \mu_\text{a1} + k_\text{b1} \mu_\text{b1} \\
k_\text{a1} \mu_\text{a2} + k_\text{b1} \mu_\text{b2} \end{array} \right)
\label{eq:measures_prog_comp_1} \\
p_2 &= k_\text{a2} a + k_\text{b2} b = 
k_\text{a2} \left( 
\begin{array}{c}
\mu_\text{a1} \\
\mu_\text{a2} \end{array} \right) 
+ k_\text{b2} \left( 
\begin{array}{c}
\mu_\text{b1} \\
\mu_\text{b2} \end{array} \right) = \left( 
\begin{array}{c}
k_\text{a2} \mu_\text{a1} + k_\text{b2} \mu_\text{b1} \\
k_\text{a2} \mu_\text{a2} + k_\text{b2} \mu_\text{b2} \end{array} \right)\label{eq:measures_prog_comp_2} \\
\tag*{\emph{Programs} composition and \emph{resource} consumption}
\end{align}

Imagine using the \emph{program} $p_1$ as the basis for our model, and $p_2$ as the \emph{target program}. Because the basis is composed of only one \emph{program}, the \emph{surrogate} $\beta$ will be a simple scalar. Therefore, for the model to be accurate, $p_2$ will have to lie on the same line of $p_1$ in the plane. This condition is true if the ratio between the consumption of the two \emph{resources} is equal for $p_1$ and $p_2$, as shown in equation \ref{eq:condition1}.
Equations \ref{eq:condition2} to \ref{eq:condition8} are simple arithmetic transformations:  \ref{eq:condition2} multiplies both members of the equation by 
$(k_\text{a2} \mu_\text{a2} + k_\text{b2} \mu_\text{b2}) (k_\text{a1} \mu_\text{a2} + k_\text{b1} \mu_\text{b2} ) $; equation  \ref{eq:condition3} expands the multiplication; equation \ref{eq:condition4} rearranges the terms to make the simplification in equation \ref{eq:condition5} more evident, where we subtract $k_\text{a1} k_\text{a2} \mu_\text{a1} \mu_\text{a2}$ and $k_\text{b1} k_\text{b2} \mu_\text{b1} \mu_\text{b2}$ to both sides; in equation \ref{eq:condition6} we multiply both sides by $\dfrac{1}{k_\text{a2}\mu_\text{a2}k_\text{b2}\mu_\text{b2}}$; in equation \ref{eq:condition8} we factor the 4 terms into the multiplication of 2 differences; in equation \ref{eq:condition9} we show the conditions under which the equation is satisfied.

\begin{align}
\dfrac{ k_\text{a1} \mu_\text{a1} + k_\text{b1} \mu_\text{b1} }{ 
k_\text{a1} \mu_\text{a2} + k_\text{b1} \mu_\text{b2} } = \dfrac{ k_\text{a2} \mu_\text{a1} + k_\text{b2} \mu_\text{b1} }{ k_\text{a2} \mu_\text{a2} + k_\text{b2} \mu_\text{b2} } 
\label{eq:condition1} \\
(k_\text{a1} \mu_\text{a1} + k_\text{b1} \mu_\text{b1}) (k_\text{a2} \mu_\text{a2} + k_\text{b2} \mu_\text{b2}) = (k_\text{a2} \mu_\text{a1} + k_\text{b2} \mu_\text{b1} ) (k_\text{a1} \mu_\text{a2} + k_\text{b1} \mu_\text{b2} ) 
 \label{eq:condition2} \\
k_\text{a1} \mu_\text{a1} k_\text{a2} \mu_\text{a2} +
k_\text{a1} \mu_\text{a1} k_\text{b2} \mu_\text{b2} +
k_\text{b1} \mu_\text{b1} k_\text{a2} \mu_\text{a2} +
k_\text{b1} \mu_\text{b1} k_\text{b2} \mu_\text{b2}
= \nonumber \\ 
= k_\text{a2} \mu_\text{a1} k_\text{a1} \mu_\text{a2} + 
k_\text{a2} \mu_\text{a1} k_\text{b1} \mu_\text{b2} + 
k_\text{b2} \mu_\text{b1} k_\text{a1} \mu_\text{a2} + 
k_\text{b2} \mu_\text{b1} k_\text{b1} \mu_\text{b2} 
 \label{eq:condition3} \\
k_\text{a1} k_\text{a2} \mu_\text{a1} \mu_\text{a2} +
k_\text{a1} k_\text{b2} \mu_\text{a1} \mu_\text{b2} +
k_\text{a2} k_\text{b1} \mu_\text{a2} \mu_\text{b1} +
k_\text{b1} k_\text{b2} \mu_\text{b1} \mu_\text{b2}
= \nonumber \\ 
= k_\text{a1} k_\text{a2} \mu_\text{a1} \mu_\text{a2} + 
k_\text{a2} k_\text{b1}  \mu_\text{a1} \mu_\text{b2} + 
k_\text{a1} k_\text{b2} \mu_\text{a2} \mu_\text{b1} + 
k_\text{b1} k_\text{b2}  \mu_\text{b1} \mu_\text{b2}  
 \label{eq:condition4} \\
k_\text{a1} k_\text{b2} \mu_\text{a1} \mu_\text{b2} +
k_\text{a2} k_\text{b1} \mu_\text{a2} \mu_\text{b1} 
= k_\text{a2} k_\text{b1}  \mu_\text{a1} \mu_\text{b2} + 
k_\text{a1} k_\text{b2} \mu_\text{a2} \mu_\text{b1} 
 \label{eq:condition5} \\
  \dfrac{k_\text{a1} \mu_\text{a1} }{k_\text{a2}\mu_\text{a2}} 
+ \dfrac{k_\text{b1}  \mu_\text{b1}}{k_\text{b2}\mu_\text{b2}} 
= \dfrac{k_\text{b1}  \mu_\text{a1} }{k_\text{b2}\mu_\text{a2}} 
+ \dfrac{k_\text{a1}   \mu_\text{b1} }{k_\text{a2}\mu_\text{b2}}
 \label{eq:condition6} \\
  \dfrac{k_\text{a1} \mu_\text{a1} }{k_\text{a2}\mu_\text{a2}} 
- \dfrac{k_\text{b1}  \mu_\text{a1} }{k_\text{b2}\mu_\text{a2}} 
+ \dfrac{k_\text{b1}  \mu_\text{b1}}{k_\text{b2}\mu_\text{b2}} 
- \dfrac{k_\text{a1}   \mu_\text{b1} }{k_\text{a2}\mu_\text{b2}} 
= 0
 \label{eq:condition7} \\
\left( 
\dfrac{k_\text{a1}}{k_\text{a2}} - \dfrac{k_\text{b1}}{k_\text{b2}}
\right) \left(
\dfrac{\mu_\text{a1}}{\mu_\text{a2}}  - \dfrac{\mu_\text{b1}}{\mu_\text{b2}}
\right)
= 0
 \label{eq:condition8} \\
\dfrac{k_\text{a1}}{k_\text{a2}} = \dfrac{k_\text{b1}}{k_\text{b2}}
\quad \text{or} \quad
\dfrac{\mu_\text{a1}}{\mu_\text{a2}}  = \dfrac{\mu_\text{b1}}{\mu_\text{b2}} 
 \label{eq:condition9} \\
  \tag*{Condition for an accurate model}
\end{align}

This shows that with a single dimension metric, the model can only be accurate in two cases:
\begin{itemize}
  \item $\dfrac{k_\text{a1}}{k_\text{a2}} = \dfrac{k_\text{b1}}{k_\text{b2}}$: if the ratio between the presence of \emph{computational patterns} in $p_1$ and $p_2$ is the same, which means that $p_1$ and $p_2$ have the same structure. In other words, they are the same program, only run multiple times or on a larger input size. 
  \item $\dfrac{\mu_\text{a1}}{\mu_\text{a2}}  = \dfrac{\mu_\text{b1}}{\mu_\text{b2}}$: if the ratio of the measures of the \emph{computational patterns} are the same, which means that they have the same behaviour on the \emph{resources}. In the case of the two resources being completion time on two different machines, it can only happen if the machines have the same architecture, simply one machine being slower than the other in every aspect of the architecture. In the section \ref{sec:single_dimension} we show an experiment where the same algorithm is measured on different architectures (AMD vs ATOM), leading to different \emph{surrogates} $\beta$.
\end{itemize}

The previous demonstration naturally extends to higher dimension \emph{resource spaces}. With $n$ \emph{resources}, the ratio between the consumption of all the \emph{resources} would have to be the same for basis and the \emph{target program}, as shown in equation \ref{eq:condition10}. In general, with n-dimensional \emph{resource spaces} the model can only describe the programs that lie in the hyperplane identified by the basis span. If the basis contain only 1 \emph{program}, the hyperplane is 1-dimensional (a line).

\begin{align}
\dfrac{\mu_{r_i}(p_1)}{\mu_{r_j}(p_1)} 
&= 
\dfrac{\mu_{r_i}(p_2)}{\mu_{r_j}(p_2)} \quad \forall i,j
\label{eq:condition10} \\ \tag*{Condition for accurate model in n-dimensions}
\end{align}


\section{Conclusions}

In this chapter we have extended our model to consider the relationship between \emph{surrogates} of the same program with a different input size. We have introduced the notion of \emph{experimental computational complexity}. We have discussed its compositional properties, and we have shown why a performance metric based on a single dimension can not be a representative characterization of the performance of software.

\emph{Experimental computational complexity} is an empirical performance analysis metric, an attempt to bridge the gap between theoretical time complexity (Big-O) and the black-box measurement-based approaches to software performance analysis.
 
In chapter \ref{chapter:toy} we will validate its expressiveness with experiments based on sorting algorithms.

\chapter{Computational energy model}
\chaptermark{Energy model}
\label{chapter:energy_model}

\subimport{.}{energy_model/model_description}


\chapter{Conclusions}
\label{chapter:model_conclusions}

In this part of the thesis we have presented our benchmarking model. 

In chapter \ref{chapter:high_level_model} we have introduced the general approach. The model is resource agnostic, in the sense that different and heterogeneous \emph{resources} can be used to build the model, and can be the target of \emph{resource} usage prediction. The model can therefore be used to characterize completion time, as well as energy consumption, and other interesting aspects of \emph{programs}.
The model can be used to characterize both hardware and software. In section \ref{sec:dual} we discuss how the characterization of software is the dual of characterization of hardware. Our model exploit the inherent relationship and interdependency of hardware and software to characterize \emph{program}'s and \emph{computational environments} behaviour. 
Our model is black-box, because it does not require access to neither the source code of the analysed \emph{program}, or the specifications of the \emph{computational environment} used to execute the \emph{program}. It only relies on measures of consumption of \emph{resources}.


In chapter \ref{chapter:solvers} we have presented three solvers: algorithms that can be used to characterize the \emph{target program} or the \emph{target resource} using measures of \emph{benchmarks}. The characterization can be used to understand the nature of the \emph{target program}, explained in terms of linear combination of \emph{benchmarks}; or to understand the nature of the \emph{target resource}, explained in terms of linear combination of the other \emph{resources}.
The model can the use the characterization to create predictions of the consumption of the \emph{target resource} by the \emph{target program}. For example, our benchmarking model could be used to predict the completion time of a \emph{program} on a new architecture.

In this part we have also introduced the concept of \emph{computational patterns} to explain and discuss the properties of our model, and justify why a simple linear approach can be used to build analytical models of complex non-linear phenomena such as the \emph{resource} consumption of \emph{programs} on different architectures.

In chapter \ref{chapter:experimental_complexity} we have also introduced the concept of \emph{experimental computational complexity}, an empirical metric of software complexity, that can be used to describe how the \emph{resource} consumption changes as the input size grows. With respect to other empirical metrics, \emph{experimental computational complexity} is resource agnostic, and can be used to describe the consumption of arbitrary \emph{resources}. We use it to demonstrate why a single dimensional metric for performance analysis can only be accurate if the architecture being analysed does not change, or if the \emph{benchmark} used to analyse the machine is the same \emph{program} whose performances we are interested in.

In our work we support the characterization and prediction of generic \emph{resources}, but we focus on completion time and energy consumption in particular. In chapter \ref{chapter:energy_model} we introduced our energy model, a simple high level approach that is capable of describing the energetic behaviour of concurrent parallel \emph{programs}. We show how the energy model can be refined to describe different kind of \emph{computational environments}, from single core machines to HPC clusters. The energy model is an attempt to describe the relationship between the two \emph{resources} of particular interest for our work: completion time and energy.


%%% Local Variables:
%%% TeX-master: "master"
%%% End:
