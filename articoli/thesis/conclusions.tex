%!TEX root = test.tex

\part{Conclusions}
\label{chapter:conclusions}

%In this chapter we summarize the contributions of our work, and discuss the future work.

As discussed in chapters \ref{sec:evolution_benchmarking} and \ref{chapter:need_unified_model}, the field of software and hardware performance analysis and energy characterization is fragmented and lacks of a systematic approach with the properties of \emph{accuracy}, \emph{consistency}, \emph{broad scope}, \emph{simplicity}, and \emph{fruitfulness}, listed by \citet{kuhn_objetivity_1977} as requisites for a good scientific model. In this work we attempt to provide a simple, generic, and abstract model that adheres to the aforementioned characteristics. 

\chapter{Contributions}

In this work we have introduced the concept of \emph{computational pattern}, a theoretical basic unit of computation. We postulate the existence of a large number of \emph{computational patterns}, one for each computational behaviour with respect to \emph{computational resource} consumption. We also postulate that real \emph{programs} are composed of combinations of \emph{computational patterns}, and that the different \emph{resource consumption} of the \emph{patterns} on different machines can explain the different behaviour of \emph{programs} on different machines. \emph{Computational patterns} are not directly measurable, because real \emph{programs} are composed of several different \emph{patterns}.

We have presented our benchmarking model, an attempt to formally define a well founded, black-box, unified hardware and software, based upon generic resources model, capable of both characterizing and predicting \emph{target program's} and \emph{target resource's} consumption. 

The model is black-box both from the point of view of the hardware and the software:
\begin{description}
  \item[hardware] : it is sufficient to observe the consumption of resources from outside the \emph{computational environment}, i.e. energy consumption alone is enough to characterize, as shown in chapter \ref{chapter:toy}. 
  \item[software] : \emph{programs} can be characterized without analysing their source code, simply observing and performing statistical analysis of their resource usage. 
\end{description}

Our benchmarking model offers a unified view on hardware and software. In literature, hardware and software have traditionally been modelled with different approaches and methodologies. We argue that, due to their intrinsic relationship (one depends on the other), they should be modelled by a unified approach. Our benchmarking model can be used to characterize a \emph{target program}, or a \emph{target resource}, with simple algebraic transformations (a matrix transpose on the matrix $\mathbf{X}$).  

Our work also attempts to offer a simple approach, valid for the broadest possible set of \emph{computational resources}. Traditionally, profoundly different models have been proposed for different \emph{computational resources}. Our model treats all \emph{resources} in the same way, assuming a few properties are respected (the mathematical properties of measures). Examples of \emph{resources} are completion time, energy consumption, and performance counters.

We have described how different \emph{solvers} can be used to characterize and to predict the \emph{resource consumption} of the \emph{target program}:
\begin{description}
  \item[Simplex] is straightforward under the assumption that the \emph{benchmarks} used to build the matrix $\mathbf{X}$ embody \emph{computational patterns}, and that the \emph{target program} is composed of \emph{computational patterns} present in the used \emph{benchmarks};
  \item[Linear Regression] is the simplest and more generic solver, that does not assume the \emph{benchmarks} to embody \emph{computational patterns}, therefore more suited for real world \emph{programs} and \emph{computational environments}. The only assumption used in this solver is that the underlying \emph{computational patterns} compose linearly into \emph{programs}. We argue that non-linear independent variables can model complex behaviour of dependent variables. In the experimental validation we report several experiments where the \emph{linear solver} was able to capture elusive behaviours, such as the effect of cache-eviction on completion time;
  \item[NonNegative Matrix Factorization] is a natural choice, having postulated that \emph{programs} are linear combinations of \emph{computational patterns}. NMF offers a possible characterization of the \emph{benchmarks} in terms of hidden factors, possibly \emph{computational patterns}, identifying their \emph{resource consumption}.
\end{description}
Despite the simplicity of the \emph{solvers} presented, in the experimental section we show that we were able to predict the performance of \emph{programs}, with an accuracy equal if not superior to other approaches in literature, that make use of complex models. As stated by Newton at the beginning of the 3rd book of the ``Principia'': ``We are to admit no more causes of natural things than such as are both true and sufficient to explain their appearances. Therefore, to the same natural effects we must, as far as possible, assign the same causes'' \citep{newton_philosophiae_2011}. 

We introduce the notion of \emph{experimental computational complexity} $\xi$, as a curve fitting process on the \emph{surrogates} of \emph{programs}, as the input size grows. We show the relationship with traditional time complexity. We also show some of the properties of compositionality of $\xi$. It can be used to characterize a \emph{target program}, but also to predict the \emph{surrogate} of a \emph{program} for which we have no measures (by interpolation, or extrapolation).

Most of the benchmarking approaches in literature attempt to describe systems and programs using a single metric. In section \ref{chapter:single_number} we discussed why this approach inevitably leads to a large characterization (therefore prediction) error. We show that such approach can only lead to small error if either the model is used on a single architecture, or the model is normalized using the same program that we want to characterize 

The natural tendency in industry to offer a single number to characterize the performance of systems and program could be a reason why prediction models in literature are usually designed to describe a single architecture, with little portability. Our approach not only can be ported from an architecture to another, but is more precise if the matrix $\mathbf{X}$ contains measures coming from different architectures.

Lastly, we introduced a simple energy model, capable of accurately characterize the power and energy consumption of \emph{computational environments} from the \emph{resource} to the cluster level. We also showed how to automatically characterize the power consumption of the factors in the model. In chapter \ref{chapter:OpenFOAM} we presented an experiment that makes use of the energy model. The energy model is capable of describing the energy consumption of concurrent parallel programs.

We have validated our contributions with extensive testing on several different machines, using both micro-benchmarks and real-world programs, as well as widely used benchmarking suites.

% TODO our model could be used to ...



\chapter{Future work}

As discussed in section \ref{sec:solvers_future_work}, more solvers could be explored. We think that a Bayesian approach to regression would be beneficial, because it makes the assumptions behind the model explicit, and offers a characterization in terms of random distribution, more expressive than single numbers in presence of noise. 
The model could also be used without assuming linearity in the combination of \emph{computational patterns} into \emph{programs}, using non-linear regression methods, such as Support Vector Regression.

Our benchmarking model analyses \emph{programs} only considering their whole execution. It would be interesting to apply the model to program phases. Programs behaviour characterization has been explored by \citet{duesterwald_characterizing_2003}, \citet{sherwood_automatically_2002}, \citet{wunderlich_smarts:_2003}, and \citet{perelman_picking_2003}. An elementary first attempt could be to split the \emph{program}'s execution in time windows, and use apply our model to the measured \emph{resource} consumption in every window independently. The \emph{surrogate} would then become the evolution of the \emph{surrogates} of each time window.
The model could then be enhanced allowing uneven window sizes, detecting a substantial change in the \emph{surrogate} as the trigger to declare the beginning of a new program's phase.
However, this approach treats every window independently, neglecting the underlying structure between program's phases. To overcome this limitation, an Hidden Markov Model (HMM) could be used to model the evolution of the program, assuming the phase as the hidden state. The \emph{surrogate} would then not only identify the program's phases, but also characterize each phase, and describe the transition probability between phases. To pursue this approach, we would need to redefine the \emph{resource consumption} to allow partial usage (in a time window, as opposed to the whole program's execution); then using the cosine similarity between defined in section \ref{sec:similarity} we could separate program's phases; then encode the transitions with an HMM.

We tested our model on several types of devices, including netbooks, workstations, HPC cluster enclosures, heterogeneous platforms equipped with CPU, GPU, and APU. However, our model should also apply to embedded devices and FPGAs. It would be interesting to run experiments on a broader range of devices to verify if the model is still capable of accurate predictions.
% TODO Also, in our experiments we have used completion time, energy, and performance counters. It would be useful to test the model with other resources?

Our benchmarking model is abstract enough to be applied to other fields. Potentially it could be used to characterize and predict the behaviour of systems where the interaction between the components can be approximated with linear interactions of smaller, simpler elements. 

For example, it could be applied to the field of business benchmarking. Modelling the use of \emph{resources} by explanatory business processes to complete tasks, our model could be used to characterize a particular business process in terms of other processes (the rows of the matrix $\mathbf{X}$ being \emph{resources} and the columns business processes, the vector $\mathbf{y}$ as the target business process). Also, it would be interesting to model a performance metrics in terms of consumption resources by business processes (the rows of the matrix $\mathbf{X}$ being the processes, the columns the resources, the vector $\mathbf{y}$ being the analysed performance metric). 

% Another field where it would be interesting to consider applying our model would be the energy expenditure of biological systems. Self-organization and similar patterns can be noticed in systems such as cellular systems \citep{camazine_self-organization_2003}. Moreover, similarly to computer systems, biological systems can be seen as carrying information \citep{collier_information_2008}. In section \ref{sec:energy_model_discussion} we have discussed the implications of imagining an ideal \emph{computational environment} on parallelism. 




%%% Local Variables:
%%% TeX-master: "master"
%%% End:
