set terminal epslatex size 12.00cm,20.00cm
#fileout = "tex/program_error_distribution.tex"
#name = "test"
set output fileout

set grid 

set multiplot layout 8, 2 title "Distribution of relative error changing number of benchmarks (1 to 28), model size 40"

set border 3 front linetype -1 linewidth 1.000
set boxwidth 0.75 absolute
set style fill   solid 1.00 border lt -1
set grid nopolar
set grid noxtics nomxtics ytics nomytics noztics nomztics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000
set style data histograms
set style histogram rowstacked
#set style histogram rowstacked title  offset character 0, 0, 0
#set xtics border in scale 0,0 nomirror rotate by -45  offset character 0, 0, 0 autojustify
#set xtics border in scale 0,0 nomirror  offset character 0, 0, 0 autojustify
#set xtics norangelimit font ",8"
#set xtics (1,"","","",5,"","","","",10,"","","","",15,"","","","",20,"","","","","","","",28) 
#set xtics ("1" 0, "10" 9, "20" 19, "28" 27)
#set x2tics
#set xtics 0,10,27
set noxtics
set noytics
#set title "Distribution of relative errors for ".name 
set ylabel "\\% of total" 
set yrange [ 0.00000 : 1.000 ] noreverse nowriteback
set xlabel "Number of benchmarks"
#i = 3
#n = 29

names = "483.xalancbmk 473.astar 471.omnetpp 464.h264ref 462.libquantum 458.sjeng 456.hmmer 445.gobmk 429.mcf 403.gcc 401.bzip2 400.perlbench 482.sphinx3 481.wrf 470.lbm 465.tonto 459.GemsFDTD 454.calculix 453.povray 450.soplex 447.dealII 444.namd 437.leslie3d 436.cactusADM 435.gromacs 434.zeusmp 433.milc 416.gamess 410.bwaves"

indices = '7 5 21 18 3 1 9 17 27 10 20 11 0 19 8 6 26 2 12 15 13 25 24 4 22 16 14 28 23 99'

do for [j=1:13] {
	set key off
	thisindex = 1 + word(indices, i+j)
	thisindexoffset = 3 + thisindex
	set title word(names, thisindex)	
	plot "<awk '{ if ($1 == 40) {print $0}}' dat/RMSE_vector_space_All_summary_sampleSize30.dat" \
		using (column(thisindexoffset+29)):xtic(2)  t "lower than 0.05" lt rgb "#00FF00", \
		'' using (column(thisindexoffset+29*2)):xtic(2) t "between 0.05 and 0.1" lt rgb "#44CC00", \
		'' using (column(thisindexoffset+29*3)):xtic(2) t "between 0.1 and 0.2" lt rgb "#888800", \
		'' using (column(thisindexoffset+29*4)):xtic(2) t "between 0.2 and 0.5" lt rgb "#CC4400", \
		'' using (column(thisindexoffset+29*5)):xtic(2) t "larger than 0.5 " lt rgb "#FF0000"
}


set key samplen .2
set key horiz
set key at 0,-5 bottom left
thisindex = 1 + word(indices, i+14)
thisindexoffset = 3 + thisindex
set title word(names, thisindex)	
plot "<awk '{ if ($1 == 40) {print $0}}' dat/RMSE_vector_space_All_summary_sampleSize30.dat" \
	using (column(thisindexoffset+29)):xtic(2)  t "lower than 0.05" lt rgb "#00FF00", \
	'' using (column(thisindexoffset+29*2)):xtic(2) t "between 0.05 and 0.1" lt rgb "#44CC00", \
	'' using (column(thisindexoffset+29*3)):xtic(2) t "between 0.1 and 0.2" lt rgb "#888800", \
	'' using (column(thisindexoffset+29*4)):xtic(2) t "between 0.2 and 0.5" lt rgb "#CC4400", \
	'' using (column(thisindexoffset+29*5)):xtic(2) t "larger than 0.5 " lt rgb "#FF0000"

thisindex = 1 + word(indices, i+15)
if (thisindex<29) {
	set key off
	thisindexoffset = 3 + thisindex
	set title word(names, thisindex)	
	plot "<awk '{ if ($1 == 40) {print $0}}' dat/RMSE_vector_space_All_summary_sampleSize30.dat" \
		using (column(thisindexoffset+29)):xtic(2)  t "lower than 0.05" lt rgb "#00FF00", \
		'' using (column(thisindexoffset+29*2)):xtic(2) t "between 0.05 and 0.1" lt rgb "#44CC00", \
		'' using (column(thisindexoffset+29*3)):xtic(2) t "between 0.1 and 0.2" lt rgb "#888800", \
		'' using (column(thisindexoffset+29*4)):xtic(2) t "between 0.2 and 0.5" lt rgb "#CC4400", \
		'' using (column(thisindexoffset+29*5)):xtic(2) t "larger than 0.5 " lt rgb "#FF0000"
}

unset multiplot

