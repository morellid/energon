set terminal epslatex size 9.0cm,6.35cm
f = "RMSE_".t."_".modelsize."_summary"
titolo = "Average relative error and RMSE, for model size=".modelsize.". 100 iterations"
print titolo
set output "tex/".f.".tex"
set zeroaxis
#set title titolo
set key top right
set style fill solid
set xlabel "number of benchmarks ($n$)"
set ylabel "relative error"
datfile = "dat/".f.".dat"
stats datfile using 1 

plot [3:16][:] datfile using 1:2 t "Average relative error" with lines, \
	"" u 1:3 t "RMSE" with lines