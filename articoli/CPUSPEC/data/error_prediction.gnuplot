set terminal epslatex size 6.0cm,5.00cm
#modelsize=80
#basissize=24
titolo = "$m$=".modelsize.", $n$=".basissize
print titolo
set output "tex/error_prediction_".modelsize."_".basissize.".tex"
bin(x, s) = s*int(x/s)
set zeroaxis
set title titolo
set key top right
set style fill solid
set ylabel "predicted RMSE"
set xlabel "RMSE"
set logscale x
set logscale y

plot [0.02:1][0.02:1] "dat/error_prediction_".modelsize."_".basissize.".dat" u 2:1 t "" w points, \
	x with lines