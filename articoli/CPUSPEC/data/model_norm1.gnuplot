set terminal latex
set output "tex/model_normalised.tex"
set xrange [ 0.0 : 1.0 ]
set yrange [ 0.0 : 1.0 ]
plot "dat/PointsAll.dat" with points
unset output