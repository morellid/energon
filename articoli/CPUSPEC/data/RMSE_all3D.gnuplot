set terminal epslatex size 9.0cm,6.35cm
set isosamples 100
set samples 100
set view 45,120

unset key

set style data lines
set parametric

f = "RMSE_".t."_All_summary_sampleSize30"
titolo = "RMSE changing model size and number of benchmarks"
print titolo
print f
set output "tex/".f.".tex"
datfile = "dat/".f.".dat"

#set title titolo
set xlabel "\\rotatebox{0}{$m$}"
set ylabel "\\rotatebox{0}{$n$}"
set zlabel "\\rotatebox{0}{RMSE}"

set zrange [0.0:2.0]
set xrange [10:210]
set yrange [2:28]

# set logscale x
# set logscale y

set hidden3d
#set dgrid3d 32 25

set grid

splot datfile using 1:2:3 title 'RMSE'