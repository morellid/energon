# input: titolo, metric, fileout, columnindex, zmin, zmax, zticz
set terminal epslatex size 9.0cm,5.35cm
set isosamples 100
set samples 100
set view 45,120

unset key

set style data lines
set parametric

set output fileout
datfile = "dat/statistics.dat"

set title titolo
set xlabel "\\rotatebox{0}{$m$}"
set ylabel "\\rotatebox{0}{$n$}"
set zlabel "\\rotatebox{0}{".metric."}"

set zrange [zmin:zmax]
set xrange [40:210]
set yrange [2:28]

set xtics 50
set ztics zticz

# set logscale x
# set logscale y

set hidden3d
#set dgrid3d 32 25

set grid

splot datfile using 1:2:columnindex title 'RMSE'