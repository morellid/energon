set terminal epslatex size 12.0cm,15.00cm

set output "tex/pcaRMSE.tex"

set grid

set multiplot layout 3, 2 title "RMSE vs position of the program after PCA"

bin(x, s) = s*int(x/s)
set zeroaxis
set title "Bias error"
set key top right
set style fill solid
set xlabel "principal component 1"
set ylabel "principal component 2"
unset xtics
unset ytics
#set logscale x
#set logscale y

set xlabel "principal component 1"
set ylabel "principal component 2"
rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 2:3:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle


set xlabel "principal component 1"
set ylabel "principal component 3"
rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 2:4:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

set xlabel "principal component 1"
set ylabel "principal component 4"
rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 2:5:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

set xlabel "principal component 2"
set ylabel "principal component 3"
rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 3:4:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

set xlabel "principal component 2"
set ylabel "principal component 4"
rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 3:5:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

set xlabel "principal component 3"
set ylabel "principal component 4"
rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 4:5:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

unset multiplot

