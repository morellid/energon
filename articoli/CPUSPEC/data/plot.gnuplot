set terminal latex
set output "tex/plot.tex"
set datafile separator ";"
set xlabel "number of benchmarks"
plot '<(sed "s/,/./g" dat/cycle.csv)' using 1:2 every ::1 with lines title 'Average error', '<(sed "s/,/./g" dat/cycle.csv)' using 1:3 every ::1 with lines title 'Root mean square'
#unset output