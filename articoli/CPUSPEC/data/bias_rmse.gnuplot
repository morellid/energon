set terminal epslatex size 8.0cm,5.00cm
modelsize=80
basissize=23
titolo = "Bias erorr and RMSE, $m$=".modelsize.", $n$=".basissize
print titolo
set output "tex/bias_rmse_".modelsize."_".basisize.".tex"
bin(x, s) = s*int(x/s)
set zeroaxis
set title titolo
set key top right
set style fill solid
set xlabel "bias error"
set ylabel "rmse"
#set logscale x
#set logscale y

plot [:][:] "dat/bias_rmse_".modelsize."_".basisize.".dat" u 2:3 t "" w points