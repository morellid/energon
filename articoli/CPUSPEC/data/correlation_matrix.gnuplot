set terminal pngcairo  transparent enhanced font "arial,10" 
set output 'correlation.png'
# set terminal epslatex color colortext standalone
# set output 'tex/correlation.tex'
unset key
set view map
set xtics border in scale 0,0 mirror norotate  offset character 0, 0, 0
set ytics border in scale 0,0 mirror norotate  offset character 0, 0, 0
set ztics border in scale 0,0 nomirror norotate  offset character 0, 0, 0
set nocbtics
set title "CPU SPEC programs correlation matrix" 
set xrange [ -0.500000 : 28.50000 ] noreverse nowriteback
set yrange [ -0.500000 : 28.50000 ] noreverse nowriteback
set cblabel "Correlation" 
set cbrange [ 0.0000 : 1.00000 ] noreverse nowriteback
set palette gray
splot "correlation.txt" matrix with image
#unset output