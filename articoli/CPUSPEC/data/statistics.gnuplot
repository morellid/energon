set terminal epslatex size 12.00cm,10.00cm
#fileout = "tex/statistics.tex"

set output fileout

set grid 

set multiplot layout 2, 1 title "Distribution of overall relative error"

#set size 0.3,0.8
#set origin 0,0

set border 3 front linetype -1 linewidth 1.000
set boxwidth 0.75 absolute
set style fill   solid 1.00 border lt -1
set grid nopolar
set grid noxtics nomxtics ytics nomytics noztics nomztics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000
set style data histograms
set style histogram rowstacked
#set style histogram rowstacked title  offset character 0, 0, 0
#set xtics border in scale 0,0 nomirror rotate by -45  offset character 0, 0, 0 autojustify
#set xtics border in scale 0,0 nomirror  offset character 0, 0, 0 autojustify
#set xtics norangelimit font ",8"
#set xtics (1,"","","",5,"","","","",10,"","","","",15,"","","","",20,"","","","","","","",28) 
#set xtics ("1" 0, "10" 9, "20" 19, "28" 27)
#set x2tics
#set xtics 0,10,27
set noxtics
set noytics
#set title "Distribution of relative errors for ".name 
set ylabel "\\% of total" 
set yrange [ 0.00000 : 1.000 ] noreverse nowriteback
set xlabel "Number of benchmarks"

set key out vert
set key samplen .2
#set key horiz
set key right  

set title "Changing number of benchmarks, fixed model size $m=40$ "	
plot "<awk '{ if ($1 == 40) {print $0}}' dat/statistics.dat" \
	using (column(4)):xtic(2)  t "lower than 0.05" lt rgb "#00FF00", \
	'' using (column(5)):xtic(2) t "between 0.05 and 0.1" lt rgb "#44CC00", \
	'' using (column(6)):xtic(2) t "between 0.1 and 0.2" lt rgb "#888800", \
	'' using (column(7)):xtic(2) t "between 0.2 and 0.5" lt rgb "#CC4400", \
	'' using (column(8)):xtic(2) t "larger than 0.5 " lt rgb "#FF0000"

set xlabel "Model size"


#set size 0.8,0.8
#set origin 0.3,0

set title "Changing model size, fixed number of benchmarks $n=10$"

plot "<awk '{ if ($2 == 10) {print $0}}' dat/statistics.dat" \
	using (column(4)):xtic(2)  t "lower than 0.05" lt rgb "#00FF00", \
	'' using (column(5)):xtic(2) t "between 0.05 and 0.1" lt rgb "#44CC00", \
	'' using (column(6)):xtic(2) t "between 0.1 and 0.2" lt rgb "#888800", \
	'' using (column(7)):xtic(2) t "between 0.2 and 0.5" lt rgb "#CC4400", \
	'' using (column(8)):xtic(2) t "larger than 0.5 " lt rgb "#FF0000"


unset multiplot

