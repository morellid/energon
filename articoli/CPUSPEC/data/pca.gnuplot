set terminal epslatex size 12.0cm,5.00cm

set output "tex/pca.tex"

set grid

set multiplot layout 1, 2 title "Error vs position of the program after PCA"

bin(x, s) = s*int(x/s)
set zeroaxis
set title "Bias error"
set key top right
set style fill solid
set xlabel "principal component 1"
set ylabel "principal component 2"
#unset xtics
#unset ytics
#set logscale x
#set logscale y

set title "Bias error"

rgb(bias) = int(bias/0.071*256)*65536+(256-int(bias/0.071*256))*256
plot [:][:] "dat/pca.dat" u 2:3:(rgb($8)) w points pt 7 ps 2 lc rgb variable notitle

set title "RMSE (m=40, n=10)"

rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
plot [:][:] "dat/pca.dat" u 2:3:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle


#set xlabel "principal component 3"
#set ylabel "principal component 4"

#set title "Bias error"

#rgb(bias) = int(bias/0.071*256)*65536+(256-int(bias/0.071*256))*256
#plot [:][:] "dat/pca.dat" u 4:5:(rgb($8)) w points pt 7 ps 2 lc rgb variable notitle

#set title "RMSE (m=40, n=10)"

#rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
#plot [:][:] "dat/pca.dat" u 4:5:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

#set xlabel "principal component 5"
#set ylabel "principal component 6"

#set title "Bias error"

#rgb(bias) = int(bias/0.071*256)*65536+(256-int(bias/0.071*256))*256
#plot [:][:] "dat/pca.dat" u 6:7:(rgb($8)) w points pt 7 ps 2 lc rgb variable notitle

#set title "RMSE (m=40, n=10)"

#rgb(bias) = int(bias/sqrt(2.54)*256)*65536+(256-int(bias/sqrt(2.54)*256))*256
#plot [:][:] "dat/pca.dat" u 6:7:(rgb(sqrt($9))) w points pt 7 ps 2 lc rgb variable notitle

unset multiplot

