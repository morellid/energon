



% motivation

Being able to predict the resource usage of software is important in many fields: an HPC scheduler could allocate the right amount of computational power to finish a job in a timely fashion without wasting resources; a cloud manager could consolidate virtual servers yet preserving the SLA; a process scheduler could prevent system thrashing by predicting the amount of memory required to complete a job. 

% problem statement

Most of the models focus only on one particular kind of resource, creating models that can not be easily ported to different resources (i.e. a model specialized on completion time can not be easily ported to memory allocation), but the innovation rate in computing architectures makes these models obsolete before they can be widely used.
Moreover, most of the models require access to the task source code, usually not possible when dealing with proprietary software.
Most of the methods are specialized on a particular architecture, or more generally don't produce models capable of abstracting from the subtle interaction between software and hardware.

% approach

In this paper we present and evaluate LBM, a linear model for software resource usage characterization and prediction, that requires no access to the target program source code, that works across different architectures. LBM uses a linear combination of benchmarks to predict the resource usage of one target program, only using passive measures, without requiring binary instrumentation or source code access. 
To evaluate LBM we used the CPU SPEC 2006 suite and completion time as the resource, using the benchmark results publicly available on the SPEC website. We tested LBM on more than 700 different machines, including widely different architectures, to our knowledge the most extensive test on performance prediction.

% results

Our experiments show that for most of the programs the prediction error is near the theoretical limit of (the bias error of the program), and that we are able to predict when we can not achieve low prediction error. We verify that as the number of measures used to build the model grows the error lowers, and the same for the number of benchmarks. We show that there is a correlation between the bias error (inherent to the measures) and the prediction error. We also show an algorithm to estimate the prediction error.

% conclusions

%This paper shows that a simple linear model can succesfully predict the resource usage of programs running on arbitrary architectures. This approach can be used to predict the completion time (as tested in this paper), but can be extended to other resources, such as energy, memory, making it a valuable tool for cloud or HPC job scheduler. 




\section{Introduction}

% why resource prediction

Programs resource usage prediction is a sensitive task in many cases: 
a better informed process scheduler could allocate the right amount of resources to processes, optimizing the completion time and resource usage pressure; 
an HPC job scheduler capable of predicting the memory usage of the jobs in the queue and their completion time could allocate the right amount of nodes to jobs, with the appropriate resources, avoiding situations like allocating nodes with large memory for a CPU bound task (wasting resources for a task that could have been run on a node with less memory) or allocating nodes with too little memory, resulting in thrashing the nodes, wasting hours if not days of work;
a cloud manager capable of predicting the resource usage pressure could migrate virtual machines to the appropriate hardware just before the resource usage spike, consolidating them on a single physical machine when the resource usage pressure is low, saving money while preserving the SLA.

As shown by \citet{hoste_performance_2006}, \citet{phansalkar_measuring_2005}, \citet{sharkawi_performance_2009}, and \citet{sankaran_energy_2013}, a resource can be used to predict other resources (i.e. performance counters can be used to predict energy consumption). Our effort is to unify existing approaches dedicated to different resources (in particular, but not limiting to, completion time), creating a more abstract model that can to characterize programs behavior in a more general sense, predicting different resources from the ones used to build a prediction model. Energy characterization and prediction could then take advantage of the vast literature in performance prediction of software.

% why blind predictions

Prediction models usually require instrumentation of the system, and involve simulation and other complex computationally intensive tasks \citep{annavaram_fuzzy_2004,lau_strong_2005}. Resource usage prediction should be performed without knowledge of the program's source code, as in most of the real world scenarios source code is not known. The resource usage prediction model should be able to rely only on data that can be measured running the programs, implementing a \emph{blind} approach. Characterization and prediction should be performed relying only on the informations about running programs that are usually available to the operative system, without the need of additional hardware or manipulations of the binary. 

% unified model

The resource prediction model should also be as abstract as possible, avoiding relying on a specific micro architecture or resource kind (as in \citet{neugebauer_energy_2001}), as the model would be obsolete before it could become widely used. For a program's descriptive and predictive model to be useful, it should be portable on different hardware from the one where the model was built. 

% competitors

The models that achieve a low prediction error are very narrow, focusing on a particular micro architecture \citep{tiwari_power_1994,brooks_wattch:_2000,steinke_accurate_2001,snavely_modeling_2001,russell_software_1998}, resource (i.e. completion time, energy consumption), even programming language \citep{tiwari_power_1994,arnold_online_2002}, they also usually model execution down to the single instruction level of detail \citep{sinha_jouletrack_2001,steinke_accurate_2001,russell_software_1998}, hiding the interaction between instructions that changes from architecture to architecture, making it difficult to abstract the results to a different architecture. 

% the solution

We developed a simple linear model that leverages on resource usage measures to predict the usage of other resources, the model was designed to be as simple as possible, capable of predicting and describing resource consumption of both hardware and software, not focusing on particular architectures or resources. The model is black-box, it does not require the source code of the program being measured. Because it relies on measures of benchmarks (non linear regressors) it can capture non linear phenomena even if based on linear regression.

% model validation using SPEC CPU 2006

We validated our model predicting the completion time of the SPEC CPU 2006 programs using a subset of the same suite as the benchmarks.

% \begin{itemize}
% \item \cite{saavedra_analysis_1996}: linear composition of abstract operations, SPEC CPU completion time prediction average error 0.57\%, root mean square 15.07\%, idea of a machine independent model to characterize machines and program, and estimation.
% \item \cite{sharkawi_performance_2009}: 7.2\% (5.4\% std dev)average error, 10.5\% (8.2\% std dev) when changing micro architecture.
% \item \cite{eeckhout_workload_2002} \cite{hoste_performance_2006}: TODO usa perf counters, PCA, somiglianza tra programmi in CPU SPEC
% \item 
% : prevede SPEC FP con errore 7\% std dev 5\% (in SWAPP 11\% err con 2\% std dev),  comb lineare di surrogati, stessa intuizione ma procedura molto piu' articolata, genetic algo
% \item \cite{steinke_accurate_2001} instruction level, processor specific
% \item \cite{tiwari_power_1994} 94, embedded, instruction level modeling
% \item \cite{snavely_modeling_2001} comb lineare di "basic blocks", non astratto rispetto alla risorsa, convoluzione con modello i/o. 23\% errore su multi processors
% \item \cite{brooks_wattch:_2000} wattch, power dissipation at architecture level, 10\% accuracy, cycle level performance simulator
% \item \cite{phansalkar_measuring_2005} programs similarity, SPEC CPU, use perf counters
% \end{itemize}

\section{The LBM model}

Benchmarking is currently more an art than a science where the performance of a system $S$ is measured against a particular test
$T$ in order to characterize the performance of S with respect to $T$. If $T$ captures a particular feature of a program $P$ we may infer that if $T$ performs well on $S$ then $P$ will follow the same pattern. Moreover, by varying either $S$ or $T$ it is possible to compare different systems against a benchmark or different benchmark against a given system. The quality of a benchmark $T$ is given by the implicit power of capturing a predictive aspect, though what prediction means is often hinted without formal specification.

As witnessed by \cite{rivoire_models_2008}, benchmarking is largely driven by industry and practitioners rather than a well defined theory, this is due to the fact that it is difficult to relate a particular program $P$ with a particular test $T$.

We propose LBM (Linear Benchmarking Model), a model designed to describe the relation between a set of benchmarks and a program.

LBM is a generalization  of \cite{tiwari_power_1994,hoste_performance_2006,snavely_modeling_2001,sharkawi_performance_2009}, designed to be resource agnostic, it can be used to characterize both hardware and software as shown in \cite{morelli_compositional_2012}, and can predict the completion time as well as the energy consumption, the allocated memory, the number of cache misses, etc... Resource consumption of programs is a known non linear phenomenon, but linear regression using non linear regressors can be used to estimate it.

In this work we will apply the model to the SPEC CPU 2006 suite completion time as an example of resource, using the abundant database of results available in the SPEC website \cite{_spec_2014}.

\subsection{Definitions}

In this section we provide the definition for terms used in the rest of the paper for presenting the case study.

\newtheorem*{definition}{Definition}

\begin{definition}[program]
A program is a particular and defined sequence of instructions.  
\end{definition}
The same program can be run on different micro architectures, even if will generate different low level sequence of processor instructions, it will still be considered the same program. When called to process different input sizes, because the sequence of high level instructions will considerably change, it will be considered a different program. The target program is the program whose resource consumption we are interested in predicting.

\begin{definition}[computational environment]
A computational environment is any computational system that can execute programs. 
\end{definition}
Examples of computational environments are embedded computers, smartphones, PCs with different micro-architectures, clusters. We consider part of the computational environment the hardware as well as the operative system and all the software running on the machine at the same time as the program being measured.


\begin{definition}[counter]
A counter is a resource of a particular computational environment, used to execute the program, whose usage can be measured.
\end{definition}

The Energy used by a computer, or the time used to complete a program are examples of metrics. The same resource on different computational environments are considered different counters: e.g. completion time on computer A and completion time on computer B are different counters. A counter is always in a one to one relationship with a computational environment, a counter can never refer to multiple computational environments.

Not every counter can be used in our model, it needs to provide measures that have the following properties: non negativity ($\forall x$ $\mu(x) \geq 0 $), null empty set ($\mu(\varnothing)=0$) and countable additivity ($\mu(\cup x_i) = \sum \mu(x_i)$). Examples of valid counters are processor time, completion time, cache misses, Joule. Examples of invalid resources are \% processor time (it may decrease), active memory (memory could be deallocated), Watt (instant power could decrease). Usually invalid counters can be made valid combining them with time (e.g. $W$ is invalid, but $J=W T$ is valid). 


\begin{definition}[target counter]
The target counter is the counter that we want to predict for the target program.
\end{definition}

\begin{definition}[measure]
A measure is a positive real number that describes the quantity of resource used by a certain program to run on a certain computational environment, as reported by the corresponding counter.
\end{definition}

A measure always refers to both a program and a counter (therefore a computational environment), i.e. a measure quantifies the usage of a particular resource on a particular computational environment by a program, as reported by the counter.

\begin{definition}[target measure]
The target measure is the measure of the target counter and the target program that we want to predict.
\end{definition}


\begin{definition}[benchmark]
A benchmark is a program used to predict the measure of the target counter and the target program. 
\end{definition}

\begin{definition}[surrogate]
A surrogate is a linear combination of benchmarks used as a model to predict the resource consumption of the target program. 
\end{definition}

\begin{definition}[solver]
A solver is an algorithm that, given a set of measures of the benchmarks and the target program, creates a surrogate for it. 
\end{definition}

\begin{definition}[computational pattern]
A computational pattern is an ideal program the exhibits a peculiar resource consumption.
\end{definition}
E.g. a program made in its entirety by floating point operations, or a program that triggers a cache miss at every instruction. Computational patterns are orthogonal to each other. Computational patterns are usually ideal programs, real programs can not consist only of a single computational pattern. At most synthetic benchmarks can approximate particular computational patterns. Some computational pattern could be reasonably be guessed (in some case even designed), but in general they are unknown, and may arise when new micro-architectures are created: a novel micro-architecture could expose a peculiar resource usage when used by a certain sequence of instructions.

The model assumes that all programs can ideally be decomposed in sequences of computational patterns.
The computational patterns form a basis of the resource consumption space (because they are orthogonal with respect to resource consumption). Any program, including both the benchmark and the target program, can be written as a linear combination of the computational patterns. If every computational pattern used by the target program is contained at least in one of the benchmarks, and if the benchmarks are not linearly dependent, we can operate a change of basis and express the target program as a linear combination of the benchmarks. If the target program contains computational patterns that are not contained in any benchmark, then the change of basis will lose information.

\subsection{Model definition}

Given a set of benchmarks and counters $\mathbf{A}$, the LBM model defines the relation between a program $p_t$ and $\mathbf{A}$. Every benchmark is characterized by a set of measurements, each relating with one of the model's counters. A combination of benchmarks and counters defines an LBM model.

More precisely measures, counters, benchmarks, target program, target counter and target measure are organized as follows:

\begin{itemize}
\item $\mathbf{A}$ is a matrix that contains the measures of the counters used by the benchmarks, this matrix does not contain the target counter. Each row of $\mathbf{A}$ contains the measures relative to a resource, each column contains the measures relative to a benchmark. 
\item $\mathbf{b}$ is a vector that contains the measures of the resources used by the target program. This vector contains measures relative to the same resources used to build $\mathbf{A}$: $\mathbf{b_i}$ is the measure of the target program usage of the same resource measured by the values contained in the $i^{th}$ row $\mathbf{A}$.
\item $\mathbf{c}$ is the vector that contains measures of the resource that we want to predict used by the benchmarks. This vector contains measures relative to the same benchmarks used to build $\mathbf{A}$: $\mathbf{c_i}$ is the measure of the target resource used by the benchmark whose values are contained in the $i^{th}$ column of $\mathbf{A}$.
\item $\mathbf{x}$ is the surrogate of the target program with respect to the benchmarks.
\item $\mu_t$ is the target measure.
\end{itemize}

$\mathbf{A}$, $\mathbf{b}$, $\mathbf{c}$, $\mathbf{x}$ and $\mu_t$ are linked by the following equations:

\begin{equation}
\mathbf{Ax}=\mathbf{b}
\label{find_surrogate}
\end{equation}

\begin{equation}
\mathbf{cx}=\mu_t
\label{prediction}
\end{equation}

LBM models can be used to predict target counters: given a set of measures of counters for a set of benchmarks and a target program $p_t$, we can express $p_t$ as a linear combination of the benchmarks: using equation \ref{find_surrogate} we can find $x$. We can add a counter (called target counter) for which we have measures for the benchmarks but not for the target program, LBM can predict the measure of the target counter for the target program (called target measure), using equation \ref{prediction}.  

Consider the following example: we have 3 computers $S_1$, $S_2$ and $S_3$, we have measured the completion time of a CPU bound program $b_1$ and a memory bound program $b_2$ on all the computers, we have also measured a program $p_t$ on $S_1$ and $S_2$, and we want to predict the completion time of $p_t$ on $S_3$. Completion time on $S_1$, $S_2$ and $S_3$ are counters, with $S_3$ being the target counter; $p$ is the target program, the measure of the completion time of $p$ on $S_3$ is the target measure. The measures ($\mu$) of $b_1$ and $b_2$ on $S_1$ and $S_2$ will form $A$:
\[
\mathbf{A} = 
\begin{pmatrix}
\mu(b_1,S_1) & \mu(b_2,S_1) \\
\mu(b_1,S_2) & \mu(b_2,S_2) \\
\end{pmatrix}
\]

It is worth to notice that in general the measure of a counter may require to repeat tests in order to obtain statistical relevance about the collected value. The resulting model in general is not exact and we will discuss how to deal with this uncertainty in our case study.

The measures we have for the target program $b$ will form vector $\mathbf{b}$, with the same counters and in the same order as in $\mathbf{A}$:
\[
\mathbf{b} = 
\begin{pmatrix}
\mu(p,S_1) & \mu(p,S_2) \\
\end{pmatrix}
\]

The measures for the target counter for the benchmarks ($b_1$ and $b_2$) will form vector $$\mathbf{c}$$, with the same programs and in the same order as in $\mathbf{A}$:
\[
\mathbf{c} = 
\begin{pmatrix}
\mu(b_1,S_3) & \mu(b_2,S_3) \\
\end{pmatrix}
\]

$\mu_t$ is the target measure: the completion time of the target program $p_t$ on $S_3$. 

The surrogate $x$ not only is a tool to predict the target measure $\mu_t$, also provides information about the target program, with respect of the benchmarks. In this example it will estimate the composition of the target program $p_t$ in terms of \emph{CPU bound} vs \emph{memory bound}.

Sometimes is useful to consider the transpose of $\mathbf{A}$ to estimate the prediction error, as we will show in the case study. 

\section{Predicting CPU SPEC 2006 completion time}

We tested our model using the SPEC CPU2006 data publicly available on the SPEC website \cite{_spec_2014}, using the completion time on each report as a different computational resource, choosing a subset of the suite as the benchmarks and the rest of the suite as the target programs. 

We decided to use the SPEC CPU suite because it has been proven to be a representative workload for real world applications, it has been used to prove the performance of several other performance predictors, and all the data needed to replicate the experiment is available on the SPEC website \cite{_spec_2014}, allowing repeatability of the results presented in this work.

\section{Bias error}

Performance measures will have bias error, as Mytkowicz \cite{mytkowicz_producing_2009} shown, because of unexpected phenomena. Our dataset consists of 4082 different SPEC INT or SPEC FP reports, of which some from the same machine, some from different. 2634 reports from SPEC INT and 2568 from SPEC FP. In some case we have more than one report from a machine (repeated experiment). We extracted the reports that have been run on machines with the same components and analyzed the difference in the measures to estimate the bias error. We calculated the expected measure for a program as the average value $\bar{x} = \frac{\sum x}{m}$ where $m$ is the number of measure we have for that program, the relative error as $e = \frac{x-\bar{x}}{\bar{x}}$ and the bias error of a program as the Root Mean Squared Error (RMSE) of the relative errors $RMSE=\sqrt{\frac{\sum e}{m}}$.

\begin{table}
\caption{Programs bias error} 
\label{bias_errors}
\begin{center}
\begin{tabular}{ l l r }
type & program & bias error \\
SPEC INT & 445.gobmk & 0.0101 \\
SPEC INT & 458.sjeng & 0.0107 \\
SPEC FP & 444.namd & 0.0108 \\
SPEC FP & 453.povray & 0.0113 \\
SPEC INT & 464.h264ref & 0.0113 \\
SPEC INT & 473.astar & 0.0126 \\
SPEC INT & 403.gcc & 0.0129 \\
SPEC FP & 454.calculix & 0.0129 \\
SPEC FP & 416.gamess & 0.0132 \\
SPEC INT & 401.bzip2 & 0.0133 \\
SPEC FP & 447.dealII & 0.0146 \\
SPEC INT & 400.perlbench & 0.0154 \\
SPEC INT & 483.xalancbmk & 0.0158 \\
SPEC FP & 450.soplex & 0.0164 \\
SPEC INT & 429.mcf & 0.0170 \\
SPEC INT & 456.hmmer & 0.0181 \\
SPEC FP & 433.milc & 0.0207 \\
SPEC INT & 471.omnetpp & 0.0216 \\
SPEC FP & 482.sphinx3 & 0.0218 \\
SPEC FP & 465.tonto & 0.0226 \\
SPEC FP & 481.wrf & 0.0280 \\
SPEC FP & 434.zeusmp & 0.0340 \\
SPEC FP & 435.gromacs & 0.0351 \\
SPEC INT & 462.libquantum & 0.0432 \\
SPEC FP & 437.leslie3d & 0.0535 \\
SPEC FP & 459.GemsFDTD & 0.0593 \\
SPEC FP & 470.lbm & 0.0681 \\
SPEC FP & 410.bwaves & 0.0701 \\
SPEC FP & 436.cactusADM & 0.0768 \\
\end{tabular}
\end{center}
\end{table}


The total bias error (RMSE) of the CPU SPEC is equal to 0.0330, the bias error of the programs varies from 1\% to more than 7\%.
Therefore we expect an error when trying to predict the program's completion time of the same order of magnitude as its bias error. We will study the correlation between the prediction error and the bias error.


\subsection{The test setting}

We downloaded the data from the SPEC website, we kept only the results that had both SPEC INT and SPEC FP, creating one row for each machine and one column for each program of the suite. The resulting matrix contained 730 rows and 29 columns. The software and scripts used to download and aggregate the data, to create the models, to make predictions and to check the prediction errors is available with open source license on \iffalse TODO \cite{_energon_????}\fi [CITATION REMOVED FOR ANONYMITY].

The primary objective of our experiment is to test the prediction accuracy of our prediction model using the data from CPU SPEC 2006. In each experiment we proceeded as follows:

\begin{enumerate}
\item we chose the number $m$ of resources used to build the model, hereto \emph{model size}, and the number $n$ of benchmarks
\item we picked a program form the CPU SPEC suite as the target program
\item we randomly selected $m$ results from the CPU SPEC 2006 database. This represents the knowledge we have when we build the model. We extracted one results from the rest of the CPU SPEC 2006 database to test the model. We created a matrix $\mathbf{M_{model}}$ with the selected $m$ CPU SPEC results, each system is a row of the matrix and each measure of the program is a column
\item we normalized each row of $\mathbf{M_{model}}$ (to make sure each computational environment had the same weigth in the model)
\item we ran Principal Component Analysis (PCA) \cite{johnson_applied_2002} and k-means clustering with $n$ clusters on the $\mathbf{M_{model}}$ matrix (to reduce the number of dimensions from $m$ to 6, usually retaining almost all the information) and selected the $n$ programs that were closest to the cluster centroids as our benchmarks
\item we created the matrix $\mathbf{A}$ keeping only the columns of $\mathbf{M_{model}}$ relative to the $n$ benchmarks
\item we found the surrogate $\mathbf{x}$ of every target program, using Ordinary Least Squares as the solver, such that $ \mathbf{Ax} = \mathbf{b} $, where $ \mathbf{b} $ is the vector of measures of the target program for the systems used to build the model
\item we predicted the resource consumption of the target program on a system from the CPU SPEC database that was not used to build the surrogate by estimating the resource consumption of the target program ($ \mathbf{p} $), multiplying the vector containing the measures of resource consumption of the benchmarks on the new system $ \mathbf{a} $ by the surrogate $ \mathbf{p} = \mathbf{ax} $ 
\item we calculated the relative error between the predicted resource consumption $ \mathbf{p} $ and the real resource consumption $ \mathbf{b} $ as $ | \frac{\mathbf{p}-\mathbf{b}}{\mathbf{b}} | $
\item we repeated this experiment for every program in the CPU SPEC suite, 30 times for each program (to ensure statistical significance)
\end{enumerate}

\subsection{Prediction results}


\begin{figure}
\scriptsize{
\import*{/Users/davidemorelli/Projects/thesis/energon/articoli/CPUSPEC/tex/}{25th}
\import*{/Users/davidemorelli/Projects/thesis/energon/articoli/CPUSPEC/tex/}{50th}
\import*{/Users/davidemorelli/Projects/thesis/energon/articoli/CPUSPEC/tex/}{75th}
}
\caption{25th, 50th and 75th relative error percentiles, changing number of benchmarks ($n$ from 1 to 28), and model size ($m$ from 20 to 210)}
\label{fig:percentiles}
\end{figure}



\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/statistics}}
\caption{Distribution of relative errors, changing number of benchmarks ($n$ from 1 to 28), and model size ($m$ from 20 to 210)}
\label{fig:statistics}
\end{center}
\end{figure}

Figure \ref{fig:percentiles} shows the 25th, 50th and 75th relative error percentiles, changing number of benchmarks ($n$ from 1 to 28), and model size ($m$ from 20 to 210). For $m=210$ and $n=28$ the 25th percentile is as low as 0.0164, 50th is 0.0410 and 75th is 0.1073; Even for much lower parameters, $m=40$ and $n=10$ (an example of realistic setting where only a limited number of measures is available), the 25th percentile is 0.0325, the 50th percentile is 0.0893 and the 75th percentile is 0.2005. 
Figure \ref{fig:statistics} shows the distribution of the relative prediction error, changing the number of benchmarks $n$ from 1 to 28 while keeping the model size fixed $m=40$, and changing the model size $m$ from 20 to 210 (incrementing by 10) while keeping the number of benchmarks fixed $n=10$.
Using a larger number of benchmarks ($n$) leads to a significant reduction of the prediction error, while using a large number of systems to build the model (large values of $m$) increases the precision by a small factor. This means that using a larger number of benchmarks ensures a better surrogate, but a small number of measures is necessary to provide good results.


\begin{table}
\caption{Error distribution} 
\label{error_distribution}
\begin{center}
\begin{tabular}{ c | c || c | c | c | c }
$m$ & $n$ & < 5\% & < 10\% & < 20\% & < 50\% \\ 
\hline
40 & 10 & 36 \% & 53 \% & 74 \% & 91 \% \\
100 & 10 & 41 \% & 62 \% & 80 \% & 93 \% \\
200 & 28 & 55 \% & 73 \% & 86 \% & 95 \% \\
\end{tabular}
\end{center}
\end{table}

%TODO The percentage of prediction with an error lower than 5\% is substantially lower than the one reported by \cite{saavedra_analysis_1996} TODO



\subsection{Prediction confidence}

Figures \ref{fig:program_error_distribution1}, \ref{fig:program_error_distribution2}, \ref{fig:program_error_distribution3} and \ref{fig:program_error_distribution4} show the distribution of the relative prediction error for every program of the CPU SPEC suite, changing the model size $m$ and the number of benchmarks $n$ used to build the surrogate. In most cases (19 out of 29) the prediction error was more likely to be lower than 10\%; in several cases there was a high chance for the error to be lower than 5\% (astar, sjeng, gobmk, bzip2, perlbench, tonto, namd, gamess); in a few cases (libquantum, lbm, cactusADM, bwaves) the error was usually higher than 20\%.

The programs for which the prediction was usually not accurate also have the higher bias error, indicating that the surrogates suffered from the error present in the data used to build the model. Also, the programs with a low bias error (such as gobmk, sjeng, namd, povray, h264ref, aster, etc.) performed very well. Therefore, bias error is highly correlated to prediction error, as visible in Figures \ref{fig:program_error_distribution1}, \ref{fig:program_error_distribution2}, \ref{fig:program_error_distribution3} and \ref{fig:program_error_distribution4}, where programs are ordered by bias error, prediction accuracy is also ordered. The average correlation between a program bias error and its prediction error is 0.53 (standard deviation 0.13), bias error can be used estimate the prediction error.

\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/program_error_distribution1}}
\caption{Distribution of relative errors of programs completion time prediction, changing number of benchmarks $n$, for a fixed model size $m=40$, ordered by bias error}
\label{fig:program_error_distribution1}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/program_error_distribution2}}
\caption{Distribution of relative errors of programs completion time prediction, changing number of benchmarks $n$, for a fixed model size $m=40$, ordered by bias error}
\label{fig:program_error_distribution2}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/program_error_distribution3}}
\caption{Distribution of relative errors of programs completion time prediction, changing model size $m$, for a fixed number of benchmarks $n=10$, ordered by bias error}
\label{fig:program_error_distribution3}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/program_error_distribution4}}
\caption{Distribution of relative errors of programs completion time prediction, changing model size $m$, for a fixed number of benchmarks $n=10$, ordered by bias error}
\label{fig:program_error_distribution4}
\end{center}
\end{figure}

Figure \ref{fig:pca} shows the position of the programs after the change of coordinates induced by PCA, the color of each point corresponds to the bias error and the RMSE (for $m=40$, $n=10)$ relative to that particular program. A mild correlation between the position on the second principal component and the errors is visible.

\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/pca}}
\caption{Bias error and RMSE versus position of the program after PCA (green = low error, red = high error).}
\label{fig:pca}
\end{center}
\end{figure}

We designed and tested the following RMSE prediction algorithm for the target program:
\begin{enumerate}
\item we take $ \mathbf{D} = \mathbf{A}^\intercal $, the transpose of $ \mathbf{A}$  (the matrix built to predict the resource consumption), with the measures of the benchmarks, every system on a column, every benchmark on a row
\item we run PCA on $\mathbf{D}$, to reduce the dimensionality from $m$ to an arbitrarily small number, in this case study we reduced it to 2 principal components, transforming $\mathbf{D}$ into $\mathbf{D'}$ (Figure \ref{fig:pca})
\item we build a vector $\mathbf{e}$ with the RMSE of the benchmarks, using the same order used for the rows of $\mathbf{D}$ 
\item by using ordinary least squares we find $ \mathbf{x'} $ such that $ \mathbf{D'x'}=\mathbf{e} $
\item we build a vector $\mathbf{d}$ with the measures of the target program, using the same order used for the columns of $\mathbf{D}$
\item we apply to $\mathbf{d}$ the same coordinate transformation used to map $\mathbf{D}$ to $\mathbf{D'}$, obtaining $\mathbf{d'}$
\item we estimate the RMSE of the target program with $RMSE=\mathbf{d'x}$
\end{enumerate}

We tested the algorithm on every program of the CPU SPEC suite, on different values of $m$ and $n$. Figure \ref{fig:error_prediction} shows the RMSE prediction accuracy of the algorithm, for a case with a small number of resources $m=40$, $n=10$, and for a case with a larger number of resources $m=80$, $n=24$. In most cases the RMSE prediction accuracy is enough to estimate the quality of the corresponding completion time prediction. Using a larger model size ($m$) reduces the RMSE prediction error (as well as the RMSE).

\begin{figure}
\begin{center}
\scriptsize{\input{data/tex/error_prediction_40_10}\input{data/tex/error_prediction_80_24}}
\caption{RMSE prediction for $m=40$ $n=10$ and for $m=80$ $n=24$}
\label{fig:error_prediction}
\end{center}
\end{figure}




\section{Conclusion and further work}

This paper presents LBM, a model that can predict counters (the resource consumption of programs) using a black box approach, using a different set of counters (not necessarily of the same kind of the one being predicted) and a set of benchmarks (programs) used to build a surrogate of the target program as a linear combination of benchmarks. This model can be applied to different kind of resources, including energy consumption, completion time, memory, etc. 

We tested the model using the data from the SPEC CPU 2006 suite, predicting the completion time of all the programs, using a subset of the suite as benchmarks, with an increasing accuracy as we used more counters to build the model. Using only 10 benchmarks and 40 (our of the 730) computer systems present in the SPEC CPU 2006 database we achieved results comparable to literature. The model has been extensively tested (16800 predictions).
LBM can also be used to characterize the behavior of a program, only using measures of the counters. LBM can also predict the error of the predictions.

% TODO il nostro modello verifica la ridondanza di CPU SPEC 

LBM could be used in an HPC scheduler (where the source code of the tasks is seldom available) to better allocate the nodes (the right number of nodes, with the right amount of memory); or to predict the resources needed by a task in a cloud, to consolidate the virtual machines while keeping the required SLA; or in an operative system scheduler, because once the model has been built, the prediction is computationally not expensive.

We will apply the same prediction model to other benchmarks suites, such as SPEC VIRT and to SPEC POWER. We will also apply different solvers to find the decomposition of the target program into a linear combination of the benchmarks, such as shrinkage, or probabilistic approaches.


%%
%% Bibliography
%%

%% Either use bibtex (recommended), but commented out in this sample


%% .. or use bibitems explicitely



