%!TEX root = ../thesis/master.tex

In this chapter we show the prediction accuracy of our model, using the widely used benchmarking suite SPEC CPU, and predicting the completion time of the suite on a machine not used to train the model. This demonstrates that the model is capable of accurate cross-architecture \emph{resource} consumption predictions.

\section{CPUSPEC}

We tested our model using the SPEC CPU2006 data publicly available on the SPEC website \footnote{\url{https://www.spec.org}}, using the completion time on each report as a different computational resource, choosing a subset of the suite as the benchmarks and the rest of the suite as the target programs. 

We decided to use the SPEC CPU suite because it has been proven to be a representative workload for real world applications, it has been used to prove the performance of several other performance predictors, and all the data needed to replicate the experiment is publicly available, allowing repeatability of the presented results.

We downloaded the data from the SPEC website, we kept only the complete results that had both SPEC INT and SPEC FP, creating one row for each machine and one column for each program of the suite. The resulting matrix contained 1133 rows and 29 columns. The software and scripts used to download and aggregate the data, to create the models, to make predictions and to check the prediction errors is available with open source license \footnote{\url{https://github.com/vslab/Energon}}.


\subsection{Bias error}

Performance measures will have bias error, as shown by \citet{mytkowicz_producing_2009}, because of unexpected phenomena. Our dataset consists of 4082 different SPEC INT or SPEC FP reports, of which some from the same machine, some from different. 2634 reports from SPEC INT and 2568 from SPEC FP. In some case we have more than one report from a machine (repeated experiment). We extracted the reports that have been run on machines with the same components and analyzed the difference in the measures to estimate the bias error. We calculated the expected measure for a program as the average value $\bar{x} = \frac{\sum x}{m}$ where $m$ is the number of measure we have for that program, the relative error as $e = \frac{x-\bar{x}}{\bar{x}}$ and the bias error of a program as the Root Mean Squared Error (RMSE) of the relative errors $RMSE=\sqrt{\frac{\sum e^2}{m}}$.

\begin{table}
\caption{Programs bias error} 
\label{bias_errors}
\begin{center}
\begin{tabular}{ l l r }
type & program & bias error \\
SPEC INT & 445.gobmk & 0.0101 \\
SPEC INT & 458.sjeng & 0.0107 \\
SPEC FP & 444.namd & 0.0108 \\
SPEC FP & 453.povray & 0.0113 \\
SPEC INT & 464.h264ref & 0.0113 \\
SPEC INT & 473.astar & 0.0126 \\
SPEC INT & 403.gcc & 0.0129 \\
SPEC FP & 454.calculix & 0.0129 \\
SPEC FP & 416.gamess & 0.0132 \\
SPEC INT & 401.bzip2 & 0.0133 \\
SPEC FP & 447.dealII & 0.0146 \\
SPEC INT & 400.perlbench & 0.0154 \\
SPEC INT & 483.xalancbmk & 0.0158 \\
SPEC FP & 450.soplex & 0.0164 \\
SPEC INT & 429.mcf & 0.0170 \\
SPEC INT & 456.hmmer & 0.0181 \\
SPEC FP & 433.milc & 0.0207 \\
SPEC INT & 471.omnetpp & 0.0216 \\
SPEC FP & 482.sphinx3 & 0.0218 \\
SPEC FP & 465.tonto & 0.0226 \\
SPEC FP & 481.wrf & 0.0280 \\
SPEC FP & 434.zeusmp & 0.0340 \\
SPEC FP & 435.gromacs & 0.0351 \\
SPEC INT & 462.libquantum & 0.0432 \\
SPEC FP & 437.leslie3d & 0.0535 \\
SPEC FP & 459.GemsFDTD & 0.0593 \\
SPEC FP & 470.lbm & 0.0681 \\
SPEC FP & 410.bwaves & 0.0701 \\
SPEC FP & 436.cactusADM & 0.0768 \\
\end{tabular}
\end{center}
\end{table}

The total bias error (RMSE) of the CPU SPEC is equal to 0.0330, the bias error of the programs varies from 1\% to more than 7\%.
Therefore we expect an error when trying to predict the program's completion time of the same order of magnitude as its bias error. We will study the correlation between the prediction error and the bias error. We expect he prediction error to be positively correlated with the bias error (guessing the correct completion time will be harder for those programs with a large bias error).



\section{Predicting completion time using the linear regression solver}
\sectionmark{Predicting completion time}


The primary objective of our experiment is to test the prediction accuracy of our prediction model using the data from CPU SPEC 2006. In each experiment we proceeded as follows:

\begin{enumerate}
\item we chose the number $m$ of resources used to build the model (\emph{training size}), and the number $n$ of benchmarks (\emph{basis size})
\item we picked a program form the CPU SPEC suite as the target program
\item we randomly selected $m$ results from the CPU SPEC 2006 database. This represents the knowledge we have when we build the model. We extracted one results from the rest of the CPU SPEC 2006 database to test the model. We created a matrix $\mathbf{M_{model}}$ with the selected $m$ CPU SPEC results, each system is a row of the matrix and each measure of the program is a column
\item we normalized each row of $\mathbf{M_{model}}$ (to make sure each computational environment had the same weight in the model). This matrix constitutes the training set of our model.
\item we pick one of the programs of CPUSPEC as the \emph{target program}
\item we selected a random subset of $n$ programs in the CPU SPEC suite to be used as the \emph{benchmarks}, the basis for our model. The probability of choosing a program as basis is proportional to its correlation with the \emph{target program}. Similar programs will therefore be preferred as basis.  
\item we created the matrix $\mathbf{X}$ keeping only the columns of $\mathbf{M_{model}}$ relative to the $n$ \emph{benchmarks}
\item we found the \emph{surrogate} $\mathbf{\beta}$ of the \emph{target program}, using Robust Least Squares as the solver, such that $ \mathbf{X\beta} = \mathbf{y} $, where $ \mathbf{y} $ is the vector of measures of the target program for the systems used to build the model
\item we predicted the resource consumption of the target program on 100 randomly selected systems from the CPU SPEC database that were not used in the training set ($\mathbf{M_{model}}$) by estimating the resource consumption of the target program ($ \mathbf{p} $), multiplying the vector containing the measures of resource consumption of the benchmarks on the new system $ \mathbf{x} $ by the surrogate $ \mathbf{p} = \mathbf{x\beta} $ 
\item we calculated the relative error between the predicted resource consumption $ \mathbf{p} $ and the real resource consumption $ \mathbf{t} $ as $ \frac{\mathbf{p}-\mathbf{t}}{\mathbf{t}} $
\item we also calculated Relative Absolute Error (RAE), defined in equation \ref{eq:RAE} s, where $p_i$ is the predicted value, $\mu_i$ is the measured value and $\hat{\mu}$ is the mean of the measured values.
\item we repeated this experiment for every program in the CPU SPEC suite, 100 times for each program (to ensure statistical significance, selecting random \emph{benchmarks} and random \emph{resources}), for basis sizes varying from 2 to 25 \emph{benchmarks}, and training sizes from 40 to 200. A total of 1670400 predictions have been performed.
\end{enumerate}

\begin{align}
\label{eq:RAE}
E_{\text{RAE}} = \dfrac{\sum \vert p_i - \mu_i \vert}{\sum \vert \mu_i - \hat{\mu} \vert} \\ \tag*{RAE}
\end{align}



\subsection{Completion time prediction accuracy}

In this section we report the prediction accuracy of our model choosing the basis (the programs used as benchmarks). Figure \ref{fig:CPUSPEC_changing_mn} shows the RAE changing the amount of information available when building the model, both in terms of small training set (40 machines) to large training set (200 machines) and small set of benchmarks (10) to a large set of benchmarks (25). RMSE is a good measure of the overall prediction error because it includes both the bias and the variation of the errors. Every combination of basis size and training set shows the RMSE of a large number of experiments: for each program (29) were repeated 100 experiments, each including 100 predictions, for a total of 290 thousands predictions.



\begin{figure}
\begin{center}
\scriptsize{\input{data/RAE_changing_mn}}
\caption{Completion time prediction RAE for different basis sizes and training set sizes}
\label{fig:CPUSPEC_changing_mn}
\end{center}
\end{figure}

As expected, with limited information available when building the model, the predictions contain a large error. 

With a limited number of \emph{benchmarks} in the basis, RAE is consistently high even using a large amount of measures. This can be explained by the fact that the programs in CPU SPEC are very different. Therefore, using a small set of randomly selected \emph{benchmarks} as the basis, is unlikely that they will contain representative aspects of the \emph{target program}. The resulting model will not be able to characterize the important characteristics of the \emph{target program} and the predictions will have a large error.

For small values of training size is noticeable a minimum in RAE for values of basis size approximately a quarter of the training size. This is an expected phenomenon, because a large basis with not enough data points results in a model that over-fits the data, with little residuals, but large prediction error.

The number of \emph{benchmarks} used in the basis should therefore be chosen depending on the amount of measures available for the training set, making sure that the number of rows in the matrix $\mathbf{X}$ is considerably larger than the number of rows.

\subsubsection{Predictions accuracy for each program}

In this section we will explore the detailed predictions in two cases: a limited amount of information available when building the model (using both a small basis size and a small training set), and a large amount of information (using a large basis and a large training set).

Figure \ref{fig:CPUSPEC_m50_n5} shows the distribution of the relative prediction errors and the fitting residuals, using 5 \emph{benchmarks} as basis, and 50 different machines, for each program.

Figure \ref{fig:CPUSPEC_m100_n25} shows the distribution of the relative prediction errors and the fitting residuals, using 25 \emph{benchmarks} as basis, and 100 different machines, for each program.

Programs are ordered by their bias error, to show that there is a negative correlation between the prediction accuracy the bias error. 

\begin{figure}
\begin{center}
\scriptsize{\input{data/error_m50_n5}}
\caption{Completion time prediction accuracy with basis size 5 and training size 50, ordered by bias error}
\label{fig:CPUSPEC_m50_n5}
\end{center}
\end{figure}

\input{data/errors_table_m50_n5}

\begin{figure}
\begin{center}
\scriptsize{\input{data/error_m100_n25}}
\caption{Completion time prediction accuracy with basis size 25 and training size 100, ordered by bias error}
\label{fig:CPUSPEC_m100_n25}
\end{center}
\end{figure}

\input{data/errors_table_m100_n25}

The overall RAE of the case with 5 \emph{benchmarks} and 50 machines is 0.27, whereas with 25 \emph{machines} and 100 machines RAE is only 0.16.

The improvement is noticeable especially in the benchmarks with high bias error, such as 459.GemsFDTD (where the prediction error mean and standard deviation went from -0.05 and 0.32, to -0.01 and 0.16), or 470.lbm (that went from -0.11 and 0.78, to -0.03 and 0.26). This improvement is expected, because with a larger basis more detailed behaviour can be captured, and with a larger training set the risk of over-fitting decreases.

\subsection{Fitting residuals, bias, and prediction error}

The prediction error and the regression residuals are closely related. Their correlation is as high as 0.97. This is also evident from figures \ref{fig:CPUSPEC_m50_n5} and \ref{fig:CPUSPEC_m100_n25} and tables \ref{tab:errors_table_m50_n5.tex} and \ref{tab:errors_table_m100_n25.tex} where, for each program, the distribution of prediction error and fitting residuals are shown. Programs with large residuals have poor performance predictions (e.g. 436.cactusADM or 410.bwaves), programs with small residuals have good predictions (e.g. 445.gpbmk or 458.sjeng). 

Also, as expected, bias error is positively correlated with prediction error (0.89). The programs in figures \ref{fig:CPUSPEC_m50_n5} and \ref{fig:CPUSPEC_m100_n25} are ordered by bias error, and the prediction error is generally very small in the first programs and large in the last programs.

Therefore, fitting residuals, bias error, and prediction error, are closely related. Knowing the bias error of a program gives an immediate idea of the expected quality of the prediction quality. Looking at the distribution of the regression residuals is possible to estimate the prediction uncertainty.

\subsection{Discussion}

Recently an analysis of the redundancy of the CPU SPEC 2006 suite 
\citep{kareem_principal_2015} showed that 429.mcf, 471.omnetpp, 403.gcc, and 462.libquantum exhibit different behaviour with respect to the rest of the suite. The authors used Principal Component Analysis to cluster the programs, similarly to what previously done by \citet{phansalkar_measuring_2005}. 

From the existing literature we might conclude that those programs that do not fit into any cluster found by PCA will have poor performance predictions, because of the reduced similarity with the programs in the basis. However, with our experiment, we
found that this is generally not true. With the exception of 462.libquantum, 
all the programs that are outside the main PCA clusters (429.mcf, 471.omnetpp, and 403.gcc) have accurate performance prediction. 

% \section{Predicting completion time using the bayesian regression solver}

% TODO: bayes

% \section{Finding hidden factors}

% TODO use NMF, match with literature

\section{Conclusions}

The experiment presented in this chapter shows that our model can be used to predict the \emph{resource} consumption (in particular the completion time) of \emph{programs} using a black box approach. We tested the model using the data from the SPEC CPU 2006 suite, using a subset of the suite as \emph{surrogates} and predicting the completion time of the remaining \emph{programs}, with an increasing accuracy as we use more data to build the model. The model has been extensively tested with this data (290000 predictions), making it a reliable measure of accuracy. We have also shown that the fitting residuals can be used as a reliable estimation of the prediction error.

This model can also be used to characterize the behaviour of a program, only using measures of its resource usage. 

Linear regression, despite its simplicity, offers comparable or superior prediction accuracy than more complicated approaches (as described in \citet{sharkawi_performance_2009}), using only completion time instead of a large set of performance counters (as describe in \citet{phansalkar_measuring_2005,sharkawi_performance_2009}). It should therefore be preferred to more complicated models.

This model could be used in an HPC scheduler (where the source code of the tasks is seldom available) to better allocate the nodes (the right number of nodes, with the right amount of memory); or to predict the resources needed by a task in a cloud, to consolidate the virtual machines while keeping the required SLA; or in an operative system scheduler, because once the model has been built, the prediction is computationally not expensive.

