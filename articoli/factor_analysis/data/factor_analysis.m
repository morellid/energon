latent = 2;
observed = 3;
cases = 4;
Costs = rand(cases,latent);
Compositions = rand(latent,observed);
Measures = Costs*Compositions;

% input data
%X = [ 0.0557824798057695, 0.0939622045087131; 0.201746506492678, 0.185880627748671; 0.0368445076087637, 0.0751156293274197; 0.45723331258131, 0.412438496408923;]
X = A

figure 1;
title "original data"
p1 = plot(X(:,1), X(:,2), "ro", "markersize",10, "linewidth", 3);


mu = mean(X)
Xm = bsxfun(@minus, X, mu)
C = cov(Xm)
[V,D] = eig(C)

% sort eigenvectors desc
[D, i] = sort(diag(D), 'descend');
V = V(:,i);

P = X * V

figure 2;
p2 = plot(P(:,1), P(:,2), "ro", "markersize",10, "linewidth", 3);

%source(varimax)
[Vr,rotdata] = varimax(P,1e-4)

figure 3;
p3 = plot(Vr(:,1), Vr(:,2), "ro", "markersize",10, "linewidth", 3);


